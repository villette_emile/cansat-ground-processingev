/*
 * GIS_Dataset.cpp
 */

#include <ImgProcessing/GIS_Dataset.h>
#include "utils/FileMgr.h"
#include <iostream>
#include <sstream>
#include <cassert>
#include "ogrsf_frmts.h"

#include "DebugUtils.h"

using namespace std;
static constexpr bool DBG=false;

bool GIS_Dataset::GDAL_Initialized=false;

GIS_Dataset::GIS_Dataset(	const std::string theFileName,
		bool doReplaceExisting,
		unsigned int theMaxNumberOfLayers,
		const std::string theFormat,
		const char* GDAL_DCAP_capability,
		bool initGDAL) :
			dataset(nullptr),
			sizeX(0), sizeY(0), numBands(0),
			replaceExisting(doReplaceExisting),
			fileName(theFileName),
			maxNumberOfLayers(theMaxNumberOfLayers),
			format(theFormat),
			sequenceNbr(0), dataType(GDT_Unknown)
{
	DBG_FCT
	stringstream msg;

	if (initGDAL && (!GDAL_Initialized)) {
		GDALAllRegister();
		GDAL_Initialized=true;
	}

	gdalDriver = GetGDALDriverManager()->GetDriverByName(format.c_str());
	if( gdalDriver == NULL ) {
		msg <<  "GDAL driver not available for format '" << format  << "'" << endl;
		throw runtime_error(msg.str());
	}

	// Check format supports dataset creation and subclass-specific capability
	checkDriverCapability(GDAL_DCAP_CREATE);
	checkDriverCapability(GDAL_DCAP_capability);

	// Do not open dataset: base class will do it after initialization is complete.
}

GIS_Dataset::~GIS_Dataset() {
	DBG_FCT
	if (dataset) {
		saveAndSync();
		GDALClose(dataset);
	}
}

unsigned int GIS_Dataset::getLayerCount() {
	if (!dataset) {
		return 0;
	}
	else return (unsigned int) dataset->GetLayerCount();
}

void GIS_Dataset::saveAndSync() {
	DBG_FCT
	if (!dataset) return;
	dataset->FlushCache();
}
void GIS_Dataset::checkDriverCapability(const char *GDAL_DCAP_capability) {
	DBG_FCT
	if (strlen(GDAL_DCAP_capability)==0) return;
	char** papszMetadata = gdalDriver->GetMetadata();
	if (!CSLFetchBoolean(papszMetadata, GDAL_DCAP_capability, FALSE)) {
		stringstream msg;
		msg << "GDAL driver '" << format
				<< "' does not support '" << GDAL_DCAP_capability << "'";
		throw runtime_error(msg.str());
	}
	LOG_IF(DBG, DEBUG) << "Validated capability '" << GDAL_DCAP_capability << "'";
}

OGRSpatialReference* GIS_Dataset::getEPSG4326_SRS() {
	OGRSpatialReference *mySRS= new OGRSpatialReference;
	stringstream msg;
	OGRErr error = mySRS->SetWellKnownGeogCS("EPSG:4326");
	if (error != OGRERR_NONE) {
		msg.clear();
		msg << "Error importing Wkt in SRS: " << error;
		throw runtime_error(msg.str());
	}
	error = mySRS->Validate();
	if (error != OGRERR_NONE) {
		msg.clear();
		msg << "Error validating SRS: " << error;
		throw runtime_error(msg.str());
	}
	LOG_IF(DBG,DEBUG) << " SRS validated.";
	return mySRS;
}

void GIS_Dataset::openNewDataset() {
	DBG_FCT
	stringstream sstr;
	saveAndSync();
	if(dataset) {
		LOG_IF(DBG,DEBUG) << "Closing dataset before opening new one";
		GDALClose(dataset);
		dataset=nullptr;
	}

	do {
		string thisFileName = getFileNameWithSequence(sequenceNbr++);
		// check existence
		if (FileMgr::directoryExists(thisFileName) || (FileMgr::fileExists(thisFileName))) {
			LOG_IF(DBG,DEBUG) << "File/folder '" << thisFileName << "' exists";
			if (replaceExisting) {
				LOG_IF(DBG,DEBUG) << "Deleting existing dataset.";
				OGRErr error = gdalDriver->Delete(thisFileName.c_str());
				if (error != OGRERR_NONE) {
					cout << "Error deleting dataset '" << thisFileName << "': " << error
							<< endl;
				}
				dataset = gdalDriver->Create(thisFileName.c_str(), (int) sizeX, (int) sizeY, (int) numBands, dataType, NULL);
				LOG_IF(DBG,DEBUG) << "Created new dataset after deleting '"<< thisFileName << "'";
			} else {
				// Open existing. Do not require GDAL_VECTOR or GDAL_RASTER: it's the caller responsibility.
				// If opening files with wrong type, it will just fail.
				dataset = (GDALDataset*) GDALOpenEx( thisFileName.c_str(), GDAL_OF_UPDATE,
													 NULL, NULL, NULL );
				if (!dataset) {
					cout << "*** Got null dataset pointer opening '" << thisFileName << "'" << endl;
					throw runtime_error("*** Error: dataset is null at opening");
				}
				LOG_IF(DBG,DEBUG) << "Opened existing dataset '"<< thisFileName << "'";
			}
		} else {
			// Open new
			dataset = gdalDriver->Create(thisFileName.c_str(), (int) sizeX, (int) sizeY, (int) numBands, dataType, NULL);
			LOG_IF(DBG,DEBUG) << "Created new dataset '"<< thisFileName << "'";
		}

		if( dataset == NULL )
		{
			sstr.clear();
			sstr << "Creation/Opening of output file failed for '" << thisFileName << "'" << endl;
			throw runtime_error(sstr.str());
		}
	} while ((maxNumberOfLayers >0) && (getLayerCount() >= maxNumberOfLayers));
}

bool GIS_Dataset::deleteFiles(	const std::string theFileName,
										bool includeNextDatasets,
										const std::string theFormat,
										bool initGDAL) {
	DBG_FCT

	unsigned short seq=1;
	bool exists;

	if (initGDAL && (!GDAL_Initialized)) {
		GDALAllRegister();
		GDAL_Initialized=true;
	}

	GDALDriver* gdalDriver = GetGDALDriverManager()->GetDriverByName(theFormat.c_str());
	if( gdalDriver == NULL ) {
		stringstream msg;
		msg <<  "GDAL driver not available for format '" << theFormat  << "'" << endl;
		throw runtime_error(msg.str());
	}

	do {
		string fName=getFileNameWithSequence(theFileName, seq++);
		exists =(FileMgr::directoryExists(fName) || (FileMgr::fileExists(fName)));
		if (exists){
			LOG_IF(DBG,DEBUG) << "Deleting dataset '" << fName << "'";
			OGRErr error = gdalDriver->Delete(fName.c_str());
			if (error != OGRERR_NONE) {
				cout << "Error deleting dataset '" << fName << "': " << error
						<< endl;
				return false;
			}
			else if(FileMgr::directoryExists(fName) || (FileMgr::fileExists(fName))){
				cout << "*** Warning: " << fName << endl;
				cout << "              Dataset deleted but file or directory still present: remove manually" << endl;
			}
		}
	} while (includeNextDatasets && exists);
	return true;
}

string GIS_Dataset::getFileNameWithSequence(string theFileName, unsigned short seq) {
	stringstream sstr;
	sstr<< theFileName;
	if (seq > 1) {
		sstr << setfill('0') << setw(2) << seq;
	}

	LOG_IF(DBG, DEBUG) << "Built file name '" << sstr.str() << "' (from '"
			<< theFileName << "', seq=" << seq << ")";
	return sstr.str();;
}

string GIS_Dataset::getFileNameWithSequence(unsigned short seq) {
	return getFileNameWithSequence(fileName, seq);
}

void GIS_Dataset::checkGDAL_Error(const string &msg, OGRErr error) {
	if ( error != OGRERR_NONE)
	{
		stringstream sstr;
		sstr << msg << ". Error=" << error;
		throw runtime_error(sstr.str());
	}
}
