/*
 * GIS_ContourDataset.cpp
 */

#include <ImgProcessing/GIS_ContourDataset.h>
#include "utils/FileMgr.h"
#include <iostream>
#include <sstream>
#include <cassert>
#include "ogrsf_frmts.h"

#include "DebugUtils.h"
static constexpr bool DBG=false;

using namespace std;

GIS_ContourDataset::GIS_ContourDataset(	const std::string theFileName,
		bool doReplaceExisting,
		unsigned int theMaxNumberOfContours,
		const std::string theFormat,
		bool initGDAL)
: GIS_Dataset(	theFileName, doReplaceExisting,
				theMaxNumberOfContours,
				theFormat, GDAL_DCAP_VECTOR, initGDAL)
{
	openNewDataset(); // Create dataset immediately, we have all required data.
}

GIS_ContourDataset::~GIS_ContourDataset() {}

void GIS_ContourDataset::addViewingLine(	OGRLayer* layer,
											double canLongitude, double canLatitude,
											double cornerLongitude, double cornerLatitude) {
	OGRFeature* feature = OGRFeature::CreateFeature(layer->GetLayerDefn());

	OGRLineString viewingLine;
	viewingLine.addPoint(canLongitude, canLatitude);
	viewingLine.addPoint(cornerLongitude, cornerLatitude);
	feature->SetGeometry(&viewingLine);
	OGRErr error = layer->CreateFeature(feature);
	if (error != OGRERR_NONE) {
		stringstream msg;
		msg.clear();
		msg << "Failed to create viewing line: " << error;
		throw runtime_error(msg.str());
	}
	OGRFeature::DestroyFeature(feature);
}

void GIS_ContourDataset::addContour(double lat1, double long1, double lat2,
		double long2, double lat3, double long3, double lat4, double long4,
		unsigned long timestamp, double altitudeFromGround,
		double canLatitude, double canLongitude) {
	DBG_FCT
	assert (dataset);
	stringstream msg;
	unsigned int numLayers=(unsigned int) getLayerCount();
	unsigned int numContours=(unsigned int) getContourCount();

	LOG_IF(DBG,DEBUG) << "Dataset contains " << numLayers << " layer(s) (max=. "
					  <<getMaxNumberOfLayers() << ")" ;
	LOG_IF(DBG,DEBUG) << "Dataset contains " << numContours << " contours (s). ";
	// Check double constraint: total number of layers not exceeded
	// Total number of contours does not use more than maxLayers-1 layers.
	bool maxReached = (getMaxNumberOfLayers() > 0)
							&& ((numLayers >=  getMaxNumberOfLayers()
								|| numContours >= getMaxNumberOfLayers()-1));
	if (maxReached) {
		LOG_IF(DBG,DEBUG) << "Opening new dataset for contour";
		openNewDataset();
	}

	OGRLayer* newLayer;
	stringstream sstr;
	sstr << setw(9) <<setfill('0') << timestamp;
	string layerName=sstr.str();
	LOG_IF(DBG,DEBUG) << "Creating layer '" << layerName << "'";

	OGRSpatialReference *mySRS=getEPSG4326_SRS();
	newLayer = dataset->CreateLayer(layerName.c_str(), mySRS, wkbLineString, nullptr);
	// Do not free SRS, it is reference counted.
	if (newLayer == NULL) throw runtime_error("Error creating contour layer.");

	LOG_IF(DBG,DEBUG) << "Layer '" << layerName << "' created: '";

	// Create attribute fields before creating any feature
	createField(newLayer,"timestamp", OFTInteger);
	createField(newLayer,"altitude", OFTReal);

	//  Create contour itself
	OGRFeature* feature=OGRFeature::CreateFeature(newLayer->GetLayerDefn());
	feature->SetField("timestamp", (int) timestamp);
	feature->SetField("altitude", altitudeFromGround);

	// Define vertices for contour itself
	OGRLinearRing ring;
	ring.addPoint( long1, lat1);
	ring.addPoint( long2, lat2);
	ring.addPoint( long3, lat3);
	ring.addPoint( long4, lat4);
	ring.addPoint( long1, lat1);
	feature->SetGeometry(&ring);

	OGRErr error=newLayer->CreateFeature(feature);
	if (error!= OGRERR_NONE) {
		msg.clear();
		msg << "Failed to create polygon in shapefile: "  << error;
		throw runtime_error(msg.str());
	}
	OGRFeature::DestroyFeature(feature);

	//  Create viewing lines
	if ((canLongitude <500) && (canLatitude < 500)) {
		addViewingLine(newLayer, canLongitude, canLatitude, long1, lat1);
		addViewingLine(newLayer, canLongitude, canLatitude, long2, lat2);
		addViewingLine(newLayer, canLongitude, canLatitude, long3, lat3);
		addViewingLine(newLayer, canLongitude, canLatitude, long4, lat4);
	}
}

unsigned int GIS_ContourDataset::getContourCount() {
	unsigned int result=0;
	if (dataset) {
		result= (unsigned int) dataset->GetLayerCount();
	}
	return result;
}

bool GIS_ContourDataset::deleteFiles(const std::string theFileName,
		bool includeNextDatasets, const std::string theFormat, bool initGDAL)
{
	return GIS_Dataset::deleteFiles(theFileName, includeNextDatasets, theFormat, initGDAL);
}

void GIS_ContourDataset::createField(
		OGRLayer* layer, const char* fieldName,  OGRFieldType fieldType)
{
	stringstream msg;
	OGRFieldDefn oField(fieldName, fieldType);
	OGRErr error = layer->CreateField(&oField);
	if (error != OGRERR_NONE) {
		msg.clear();
		msg << "Error creating field '"<< fieldName << "': " << error;
		throw runtime_error(msg.str());
	}
}

bool GIS_ContourDataset::checkIntegrity(bool verbose) {
	assert(dataset);
	bool result=true;
	int numLayers=dataset->GetLayerCount();
	cout << "Checking integrity of dataset... (" << numLayers << " layers)" << endl;
	for (unsigned short i=0; i< numLayers;i++) {
		OGRLayer *layer = dataset->GetLayer(i);
		if (!layer) {
			cout << " **** ERROR: Layer is null" << endl;
			return false;
		}
		result &= checkContourLayerIntegrity(layer, verbose);
	}
	return result;
}

bool GIS_ContourDataset::checkContourLayerIntegrity(OGRLayer *layer, bool verbose ) {
	auto count= layer->GetFeatureCount();
	if (verbose) cout << " Layer: " << layer->GetDescription() << ", " << count << " feature(s)" << endl;
	OGRFeature *feature= layer->GetFeature(0);
	if (!feature) {
		cout << " **** ERROR: Feature is null" << endl;
		return false;
	}
	auto featureDef=feature->GetDefnRef();
	if (!featureDef) {
		cout << " **** ERROR: Feature is null" << endl;
		return false;
	}
	int fieldCount = featureDef->GetFieldCount();
	if (verbose) cout << "  Feature->FeatureDef->fieldCount=" << fieldCount << endl;
	for (auto i=0; i< fieldCount ;i++) {
		auto fieldDef= featureDef->GetFieldDefn(i);
		if (!fieldDef) {
			cout << " **** ERROR: Field def is null" << endl;
			return false;
		}
		if (verbose) cout << "    Field " << i << ": " << fieldDef->GetNameRef() << endl;
	}
	if (featureDef->GetGeomType() != wkbLineString)
	{
		cout << "  *** Error: geometry type != wkbLineString: " << featureDef->GetGeomType();
		return false;
	}
	else if (verbose) cout << "  Geometry type = wkbLineString as expected" << endl;
	OGRGeometry * geometry=feature->GetGeometryRef();
	if (!geometry) {
		cout << " **** ERROR: Geometry is null" << endl;
		return false;
	}
	OGRLineString* line= dynamic_cast<OGRLineString*>(geometry);
	if (!line) {
		cout << " **** ERROR: ring is null" << endl;
		cout << " typeid(geometry):" << typeid(*geometry).name();
		return false;
	}
	auto numPoints= line->getNumPoints();
	if (verbose) cout << "  Contour has " << numPoints  << " points" << endl;
	for (int i=0; i<numPoints ; i++)
	{
		OGRPoint point;
		line->getPoint(i, &point);
		if (verbose) cout << "    " << i << ": (" << point.getX() << "," << point.getY() <<")" << endl;
	}
	return true;
}

