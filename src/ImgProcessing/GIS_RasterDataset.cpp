/*
 * GIS_RasterDataset.cpp
 */

#include <ImgProcessing/GIS_RasterDataset.h>
#include <ImgProcessing/JPEG_Image.h>
#include "utils/FileMgr.h"
#include <iostream>
#include <sstream>
#include <cassert>
#include "ogrsf_frmts.h"

#include "DebugUtils.h"
static constexpr bool DBG=false;

using namespace std;
GIS_RasterDataset::GIS_RasterDataset(	const std::string theFileName,
										unsigned char* bitmap,
										unsigned int sizeX,
										unsigned int sizeY,
										unsigned int numChannels,
										bool doReplaceExisting,
										const double &topLeftCornerLong,
										const double &topLeftCornerLat,
										const double &pixelSizeInDegreesLong,
										const double &pixelSizeInDegreesLat,
										const std::string theFormat,
										bool initGDAL) :
	GIS_Dataset(	theFileName, doReplaceExisting, 1,
					theFormat, GDAL_DCAP_RASTER, initGDAL)
{
	loadBitmap(	 bitmap, sizeX, sizeY, numChannels,
			 	 topLeftCornerLong, topLeftCornerLat,
				 pixelSizeInDegreesLong, pixelSizeInDegreesLat);
}

GIS_RasterDataset::GIS_RasterDataset(	const std::string theFileName,
										const std::string theJPEG_FileName,
										bool doReplaceExisting,
										const double &topLeftCornerLong,
										const double &topLeftCornerLat,
										const double &pixelSizeInDegreesLong,
										const double &pixelSizeInDegreesLat,
										const std::string theFormat,
										bool initGDAL) :
	GIS_Dataset(	theFileName, doReplaceExisting, 1,
					theFormat, GDAL_DCAP_RASTER, initGDAL)
{
	DBG_FCT
	JPEG_Image img(theJPEG_FileName);
	loadBitmap(  img.getBitmap(),img.getSizeX(),img.getSizeY(), img.getNumChannels(),
			   	 topLeftCornerLong, topLeftCornerLat,
				 pixelSizeInDegreesLong, pixelSizeInDegreesLat);
}

GIS_RasterDataset::~GIS_RasterDataset() {}

unsigned int GIS_RasterDataset::getImageCount() {
	if (!dataset) {
		return 0;
	}
	else return (unsigned int) dataset->GetLayerCount();
}

bool GIS_RasterDataset::deleteFiles(const std::string theFileName,
		bool includeNextDatasets, const std::string theFormat, bool initGDAL)
{
	return GIS_Dataset::deleteFiles(theFileName, includeNextDatasets, theFormat, initGDAL);
}

bool GIS_RasterDataset::checkIntegrity(bool verbose) {
	assert(dataset);
	bool result=true;
	unsigned int numImages=getImageCount();
	if (verbose) cout << "Checking integrity of dataset... (" << numImages << " contours)" << endl;
	for (unsigned short i=0; i< numImages;i++) {
		OGRLayer *layer = dataset->GetLayer(i);
		if (!layer) {
			cout << " **** ERROR: Layer is null" << endl;
			return false;
		}
#ifdef REVIEW
		auto count= layer->GetFeatureCount();
		if (verbose) cout << " Layer: " << layer->GetDescription() << ", " << count << " feature(s)" << endl;
		OGRFeature *feature= layer->GetFeature(0);
		if (!feature) {
			cout << " **** ERROR: Feature is null" << endl;
			return false;
		}
		auto featureDef=feature->GetDefnRef();
		if (!featureDef) {
					cout << " **** ERROR: Feature is null" << endl;
					return false;
				}
		int fieldCount = featureDef->GetFieldCount();
		if (verbose) cout << "  Feature->FeatureDef->fieldCount=" << fieldCount << endl;
		for (auto i=0; i< fieldCount ;i++) {
			auto fieldDef= featureDef->GetFieldDefn(i);
			if (!fieldDef) {
				cout << " **** ERROR: Field def is null" << endl;
				return false;
			}
			if (verbose) cout << "    Field " << i << ": " << fieldDef->GetNameRef() << endl;
		}
		if (featureDef->GetGeomType() != wkbLineString)
		{
			cout << "  *** Error: geometry type != wkbLineString: " << featureDef->GetGeomType();
			return false;
		}
		else if (verbose) cout << "  Geometry type = wkbLineString as expected" << endl;
		OGRGeometry * geometry=feature->GetGeometryRef();
		if (!geometry) {
			cout << " **** ERROR: Geometry is null" << endl;
			return false;
		}
		OGRLineString* line= dynamic_cast<OGRLineString*>(geometry);
		if (!line) {
			cout << " **** ERROR: ring is null" << endl;
			cout << " typeid(geometry):" << typeid(*geometry).name();
			return false;
		}
		auto numPoints= line->getNumPoints();
		if (verbose) cout << "  Contour has " << numPoints  << " points" << endl;
		for (int i=0; i<numPoints ; i++)
		{
			OGRPoint point;
			line->getPoint(i, &point);
			if (verbose) cout << "    " << i << ": (" << point.getX() << "," << point.getY() <<")" << endl;
		}
#endif
	}
	return result;
}

void GIS_RasterDataset::setMetaData(const string& name, const string& value)
{
	assert(dataset);
	dataset->SetMetadataItem(name.c_str(), value.c_str(), NULL);
}

void GIS_RasterDataset::setCanSatMetaData(	double longitude,
											double latitude,
											double altitude)
{
	// NB: No blanks allowed in names and values
	setMetaData("Origin", "CansatCSPU");
	stringstream sstr;
	sstr << setprecision(10) << altitude;
	setMetaData("Altitude", sstr.str());
	sstr.str("");
	sstr << setprecision(10) << longitude;
	setMetaData("Longitude",sstr.str());
	sstr.str("");
	sstr << setprecision(10) << latitude;
	setMetaData("Latitude",sstr.str());
}

void GIS_RasterDataset::loadBitmap(	unsigned char* bitmap,
									unsigned int sX,
									unsigned int sY,
									unsigned int nChannels,
									const double &topLeftCornerLong,
									const double &topLeftCornerLat,
									const double &pixelSizeInDegreesLong,
									const double &pixelSizeInDegreesLat)
{
	DBG_FCT

	LOG_IF(DBG,DEBUG) << "Size: "<< sX << "x" << sY << ", " << nChannels << " channels. Dataset=" << dataset;
	setDataType(GDT_Byte);
	sizeX=sX;		// Configure size and number of channels before opening file.
	sizeY=sY;
	numBands=nChannels;
	openNewDataset();
	LOG_IF(DBG,DEBUG) << "Dataset open";

	// Fill with a projection, geotransform and raster data
	// Y pixel size must be negative since Y image dimension points towards
	// negative latitudes.
	double adfGeoTransform[6] = { topLeftCornerLong, pixelSizeInDegreesLong, 0,
								  topLeftCornerLat, 0, -pixelSizeInDegreesLat };
	dataset->SetGeoTransform( adfGeoTransform );
	LOG_IF(DBG,DEBUG) << "GeoTransform set";

	//set projection
	OGRSpatialReference mySRS;
	OGRErr error = mySRS.SetWellKnownGeogCS( "EPSG:4326" ); // mySRS.importFromWkt("EPSG:4326");
	checkGDAL_Error("Error importing Wkt in SRS",error);
	error = mySRS.Validate();
	checkGDAL_Error("Error validating SRS",error);
	LOG_IF(DBG, DEBUG) << " SRS validated.";

	//oSRS.SetUTM( 11, TRUE );
	//oSRS.SetWellKnownGeogCS( "NAD27" );
	char *pszSRS_WKT = NULL;
	mySRS.exportToWkt( &pszSRS_WKT );
	dataset->SetProjection( pszSRS_WKT );
	CPLFree( pszSRS_WKT );
	LOG_IF(DBG,DEBUG) << "SRS set to '"<< pszSRS_WKT << "'" ;

	// img.addTestRGB_Blocks(); (uncomment to check pure RGB blocks are correctly displayed).

	// Add image
	GDALRasterBand *band;
	for (unsigned int currentBand = 1; currentBand <= numBands; currentBand++)
	{
		band = dataset->GetRasterBand((int) currentBand); // !! numbered from 1 to GetRasterCount().
		assert(band != nullptr);

		// Bitmap from the JPEG is interleaved (I THINK).
		// so, (1) each byte is numBand-1 bytes away from the corresponding byte for the next pixel.
		//     (2) each first bite is currentBand-1 byte from the start of the image.
		error=band->RasterIO( 	GF_Write,
				0, 0,  // Offset in X-Y: start from top-left corner
				(int) sizeX, (int) sizeY,
				bitmap+currentBand-1,
				(int) sizeX, (int) sizeY,
				GDT_Byte,
				numBands,   // nPixelSpaceX (numBand bytes/ pixel)
				0);			// nPixelSpaceY (lines are not interleaved).
		checkGDAL_Error("Error creating band",error);
		LOG_IF(DBG, DEBUG) << " Band " << currentBand << " ok";
	}
}

