/*
 * GIS_RasterDataset.h
 *
 */

#pragma once
#include <string>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#include <gdal_priv.h>
#pragma GCC diagnostic pop

#include "ImgProcessing/GIS_Dataset.h"

/** @ingroup ImgProcessing
 *  @brief A class to handle the production of a GIS dataset containing an image.
 *  The class encapsulates the creation of the dataset
 *  and the creation of multiple datasets (1 per image)
 *  This class makes use of the GDAL library which is assumed to be installed
 *  (if not install from www.gdal.org, see instructions in folder 7900).
 *
 *  The underlying GDAL library is only initialized once, and several instances of any subclass of
 *  GIS_Dataset can be used simultaneously without problem.
 *
 *  Usage
 *  =====
 *  @code
 *  	(...)
 *  	(repeat any number of times):
 *  	GIS_RasterDataset* ds=new GIS_RasterDataset(fileName, (...other arguments...));
 * 	 	// Optional: will be saved to file by the destructor anyway.
 * 	 	ds.saveAndSync();
 * 	 	delete ds;
 *  @endcode
 *  To delete an existing dataset:
 *  @code
 *  	GIS_RasterDataset::delete(fileName, true);
 *  	// Remark: on Windows deletion sometime appears to succeed while leaving
 *  	//         some files behind anyway....
 *  @endcode
 *
 *  If the GDAL library is used outside this class and already initialized, be sure to set the optional parameters
 *  initGDAL to false in the constructor and the delete() method to avoid re-initialization.
 */
class GIS_RasterDataset : public GIS_Dataset {
public:
	/**
	 *  Create a dataset from in-memory bitmap.
	 *  @param theFileName The name of the dataset (depending on the format, it can be
	 *  				a file or a folder name). Can be a relative or absolute path,
	 *  				but in this case the containing folder is expected to exist.
	 *  @param bitmap   Pointer to the bitmap. Channels are expected to be 8-bits, interleaved.
	 *  				(0,0) is at top-left corner.
	 *  				Size is expected to be sizeX*sizeY*numChannels
	 *  @param sizeX	The number of columns of the bitmap
	 *  @param sizeY	The number of lines of the bitmap.
	 *  @param numChannels The number of bytes per pixel (each channel is one unsigned byte).
	 *  @param topLeftCornerLong Longitude of the top left corner of the image (positive
	 *                  (decimal degrees).
	 *  @param topLeftCornerLat Latitude of the top left corner of the image (positive
	 *                  (decimal degrees).
	 *  @param pixelSizeInDegreesLong Pixel size in degrees in the longitude direction (positive)
	 *  @param pixelSizeInDegreesLat  Pixel size in degrees in the latitude direction (positive)
	 *  @param doReplaceExisting If true, an existing file or directory named "fileName"
	 *  				is deleted (if any). If false, and the file or directory
	 *  				exists, it is open, and appended.
	 *  @param theFormat The format for storing the dataset (chosen among the formats
	 *  			    supported by GDAL for creation of multiple raster layers
	 *  			    (see https://gdal.org/ogr_formats.html).
	 *  @param initGDAL If true, the GDAL library is initialized the first time any subclass of
	 *  				GIS_Dataset is created (default). Set this
	 *  				to false if GDAL was initialized prior to using subclasses of GIS_Dataset.
	 */
	GIS_RasterDataset(	const std::string theFileName,
						unsigned char* bitmap,
						unsigned int sizeX,
						unsigned int sizeY,
						unsigned int numChannels,
						bool doReplaceExisting,
						const double &topLeftCornerLong = 0,
						const double &topLeftCornerLat = 0,
						const double &pixelSizeInDegreesLong = 0.01,
						const double &pixelSizeInDegreesLat = 0.01,
						const std::string theFormat="GTiff",
						bool initGDAL=true);
	/**
	 *  Create a dataset from a JPEG file.
	 *  @param theFileName The name of the dataset (depending on the format, it can be
	 *  				a file or a folder name). Can be a relative or absolute path,
	 *  				but in this case the containing folder is expected to exist.
	 *  @param theJPEG_FileName The name of the file containing the JPEG image containing the
	 *  				raster data.
	 *  @param doReplaceExisting If true, an existing file or directory named "fileName"
	 *  				is deleted (if any). If false, and the file or directory
	 *  				exists, it is open, and appended.
	 *  @param topLeftCornerLong Longitude of the top left corner of the image (positive
	 *                  (decimal degrees).
	 *  @param topLeftCornerLat Latitude of the top left corner of the image (positive
	 *                  (decimal degrees).
	 *  @param pixelSizeInDegreesLong Pixel size in degrees in the longitude direction (positive)
	 *  @param pixelSizeInDegreesLat  Pixel size in degrees in the latitude direction (positive)
	 *  @param theFormat The format for storing the dataset (chosen among the formats
	 *  			    supported by GDAL for creation of multiple raster layers
	 *  			    (see https://gdal.org/ogr_formats.html).
	 *  @param initGDAL If true, the GDAL library is initialized the first time any subclass of
	 *  				GIS_Dataset is created (default). Set this
	 *  				to false if GDAL was initialized prior to using subclasses of GIS_Dataset.
	 */
	GIS_RasterDataset(	const std::string theFileName,
						const std::string theJPEG_FileName,
						bool doReplaceExisting=true,
						const double &topLeftCornerLong = 0,
						const double &topLeftCornerLat = 0,
						const double &pixelSizeInDegreesLong = 0.01,
						const double &pixelSizeInDegreesLat = 0.01,
						const std::string theFormat="GTiff",
						bool initGDAL=true);

	/** Destructor. Dataset is saved to file before the object is deleted */
	virtual ~GIS_RasterDataset();

	/** Obtain the number of images already in dataset
	 *  @return the number of images.
	 */
	virtual unsigned int getImageCount();

	static bool deleteFiles(	const std::string theFileName,
								bool includeNextDatasets=false,
								const std::string theFormat="GTiff",
								bool initGDAL=true);
	/** Check every element expected in the dataset is indeed present.
	 *  Problems are reported on cout.
	 *  @param  verbose If true, diagnostic is printed on cout, even when no problem
	 *  		is detected.
	 *  @return true if everything ok, false otherwise.
	 */
	virtual bool checkIntegrity(bool verbose=false);

	virtual void setMetaData(const std::string& name, const std::string& value);
	virtual void setCanSatMetaData(double longitude, double latitude, double altitude);
protected:
	/**
		 *  Create load the image bitmap.
		 *  @param bitmap   Pointer to the bitmap. Channels are expected to be 8-bits, interleaved.
		 *  				(0,0) is at top-left corner, X positive to the East, Y
		 *  				positive down.
		 *  				Size is expected to be sizeX*sizeY*numChannels
		 *  @param sX	The number of columns of the bitmap
		 *  @param sY	The number of lines of the bitmap.
		 *  @param nChannels The number of bytes per pixel (each channel is one unsigned byte).
		 *  @param topLeftCornerLong Longitude of the top left corner of the image (positive
		 *                           (decimal degrees).
		 *  @param topLeftCornerLat Latitude of the top left corner of the image (positive
		 *                           (decimal degrees).
		 *  @param pixelSizeInDegreesLong Pixel size in degrees in the longitude direction (positive)
		 *  @param pixelSizeInDegreesLat  Pixel size in degrees in the latitude direction (positive)
		 */
		virtual void loadBitmap(	unsigned char* bitmap,
									unsigned int sX,
									unsigned int sY,
									unsigned int nChannels,
									const double &topLeftCornerLong,
									const double &topLeftCornerLat,
									const double &pixelSizeInDegreesLong,
									const double &pixelSizeInDegreesLat);
};

