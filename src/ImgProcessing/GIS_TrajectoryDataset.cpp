/*
 * GIS_TrajectoryDataset.cpp
 *
 */

#include <ImgProcessing/GIS_TrajectoryDataset.h>
#include "DebugUtils.h"
static constexpr bool DBG=true;

#include "ogrsf_frmts.h"
#include <exception>
#include <cassert>

static const char* TrajectoryLayerName="Trajectory";

using namespace std;

GIS_TrajectoryDataset::GIS_TrajectoryDataset(const std::string theFileName,
		bool doReplaceExisting,
		const std::string theFormat,
		bool initGDAL)
:	GIS_Dataset(	theFileName, doReplaceExisting,0,
		theFormat, GDAL_DCAP_VECTOR, initGDAL), trajectory(), creationOver(false)
{
	DBG_FCT
	openNewDataset();
	assert(dataset);

	OGRLayer* layer = dataset->GetLayerByName(TrajectoryLayerName);
	if (!layer) {
		OGRSpatialReference *mySRS = getEPSG4326_SRS();
		layer = dataset->CreateLayer(TrajectoryLayerName, mySRS, wkbLineString, nullptr);
		if (layer == NULL) {
			throw runtime_error("Error creating trajectory layer.");
		}
		LOG_IF(DBG,DEBUG) << "Trajectory layer created.";
	}
}

bool GIS_TrajectoryDataset::deleteFiles(const std::string theFileName,
										const std::string theFormat, bool initGDAL)
{
	return GIS_Dataset::deleteFiles(theFileName, false, theFormat, initGDAL);
}

void GIS_TrajectoryDataset::saveAndSync() {
	DBG_FCT
	if (!dataset) return;
	if (creationOver) return; // polyline was already saved, cannot update it.

	OGRLayer* layer = dataset->GetLayerByName(TrajectoryLayerName);
	//  Create Polyline
	OGRFeature* feature=OGRFeature::CreateFeature(layer->GetLayerDefn());
	//feature->SetField("name", value);

	// Define vertices
	feature->SetGeometry(&trajectory);

	OGRErr error=layer->CreateFeature(feature);
	if (error!= OGRERR_NONE) {
		stringstream msg;
		msg.clear();
		msg << "Failed to create polygon in shapefile: "  << error;
		throw runtime_error(msg.str());
	}
	OGRFeature::DestroyFeature(feature);

	GIS_Dataset::saveAndSync();
	creationOver=true;
}
GIS_TrajectoryDataset::~GIS_TrajectoryDataset() {

}

void GIS_TrajectoryDataset::addCanPosition(double longitude, double latitude) {
	DBG_FCT
	if (creationOver) {
		throw runtime_error("*** Error, addCanPosition() called after dataset was written to file");
	}

	trajectory.addPoint(longitude, latitude);
	LOG_IF(DBG,DEBUG) << "added point (" << longitude << "," << latitude << ")";
}

bool GIS_TrajectoryDataset::checkIntegrity(bool verbose) {
	assert(dataset);
	saveAndSync();
	bool result=true;
	int numLayers=dataset->GetLayerCount();
	cout << "Checking integrity of dataset... (" << numLayers << " layers)" << endl;
	if (numLayers !=1) {
		cout << "*** Expected 1 layer, got " << numLayers << endl;
		result=false;
	}
	OGRLayer *layer = dataset->GetLayer(0);
	if (!layer) {
		cout << " **** ERROR: Layer is null" << endl;
		return false;
	}
	result &= checkTrajectoryLayerIntegrity(layer, verbose);
	return result;
}

bool GIS_TrajectoryDataset::checkTrajectoryLayerIntegrity(OGRLayer *layer, bool verbose ) {
	auto count= layer->GetFeatureCount();
	if (verbose) cout << " Layer: " << layer->GetDescription() << ", " << count << " feature(s)" << endl;
	if (count != 1) {
		cout << "*** Error: expected 1 feature, got " << count << endl;
		return false;
	}
	OGRFeature *feature= layer->GetFeature(0);
	if (!feature) {
		cout << " **** ERROR: Feature is null" << endl;
		return false;
	}
	auto featureDef=feature->GetDefnRef();
	if (!featureDef) {
		cout << " **** ERROR: Feature is null" << endl;
		return false;
	}
	int fieldCount = featureDef->GetFieldCount();
	if (verbose) cout << "  Feature->FeatureDef->fieldCount=" << fieldCount << endl;
	for (auto i=0; i< fieldCount ;i++) {
		auto fieldDef= featureDef->GetFieldDefn(i);
		if (!fieldDef) {
			cout << " **** ERROR: Field def is null" << endl;
			return false;
		}
		if (verbose) cout << "    Field " << i << ": " << fieldDef->GetNameRef() << endl;
	}
	if (featureDef->GetGeomType() != wkbLineString)
	{
		cout << "  *** Error: geometry type != wkbLineString: " << featureDef->GetGeomType();
		return false;
	}
	else if (verbose) cout << "  Geometry type = wkbLineString as expected" << endl;
	OGRGeometry * geometry=feature->GetGeometryRef();
	if (!geometry) {
		cout << " **** ERROR: Geometry is null" << endl;
		return false;
	}
	OGRLineString* line= dynamic_cast<OGRLineString*>(geometry);
	if (!line) {
		cout << " **** ERROR: line string is null" << endl;
		cout << " typeid(geometry):" << typeid(*geometry).name();
		return false;
	}
	auto numPoints= line->getNumPoints();
	if (verbose) cout << "  Contour has " << numPoints  << " points" << endl;
	for (int i=0; i<numPoints ; i++)
	{
		OGRPoint point;
		line->getPoint(i, &point);
		if (verbose) cout << "    " << i << ": (" << point.getX() << "," << point.getY() <<")" << endl;
	}
	return true;
}
