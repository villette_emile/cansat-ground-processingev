/*
 * MovingAverage.cpp
 *
 *  Created on: 22 fevr. 2019
 *      Author: Steven
 */

#include <MovingAverage.h>
#include <stdlib.h>
#include <iostream>
#include "DebugUtils.h"
#include <cassert>
constexpr bool DBG=false; // Set to true to activate debugging.

using namespace std;

// TMP: ajouté l'initialisation de qques données + verification que le malloc fonctionne.
MovingAverage::MovingAverage(const unsigned int theNumSamples)
	:	numSamples(theNumSamples),
		index(0),
		sum(0),
		tableFull(false)
{
	samplesTable = (double *) malloc(numSamples*sizeof(double));
	assert(samplesTable);  // TMP: malloc renvoie null s'il est à court de mémoire.
}

MovingAverage::~MovingAverage() {
	if(samplesTable){
		free(samplesTable);
		samplesTable=NULL;  // bonne pratique, pour éviter de faire un double free.
	}
}

double MovingAverage::getAverage(double sample){
	if  (!tableFull)  {
		// In this case, we don't have enough sample yet to obtain a valid average:
		// Let's accumulate samples and calculate an average on the samples we have.
		samplesTable[index++] = sample;
		sum += sample;
		LOG_IF(DBG, DEBUG) << "Filling: sample=" << sample << ".  numSamplesInTable="
				<< index << ", sum=" << sum;
		if (index == numSamples) {
			// Now it's full!
			tableFull=true;
			index=0; // oldest sample is the first one.
		}
		return sum/index;
	}
	else{
		// The samples table is full :
		// 1. update sum
		sum -= samplesTable[index];
		sum += sample;

		// 2. Replace oldest sample with the new one.
		LOG_IF(DBG, DEBUG) << "Replacing sample #" << index
				<< " (value=" << samplesTable[index]
			  << ") with value=" << sample;
		samplesTable[index]=sample;

		// 3. update index to the next oldest sample.
		index++;
		if (index == numSamples) {
			index=0; // after the last position, return to position 0.
		}
		return sum/numSamples;
	}
}

