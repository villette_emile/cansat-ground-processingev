/*
 * MovingAverage.h
 *
 *  Created on: 22 févr. 2019
 *      Author: Steven
 */

#pragma once

/** @ingroup Utilities
 *  @brief A class performing a moving average.
 */
class MovingAverage {
public:

	/** Constructor
	 *  @param theNumSamples The number of samples to average (must be >=2).
	 */
	MovingAverage(const unsigned int theNumSamples=2);

	/** Destructor*/
	virtual ~MovingAverage();

	/** Perform a moving averaging of samples
	 * @param sample The sample received to do the moving averaging
	 * @return average if the enough samples provided according to the number of samples to average received in the constructor
	 */
	double getAverage(double sample);
private:
	// TMP: attention, quand le commentaire Doxygen est après l'élément
	//      documenté il faut mettre un /**< et non un /**
	//		documenter ce qui ne l'est pas.
	double *samplesTable;			/**< Table of samples received*/
	unsigned int numSamples;		/**< Number of samples to average*/
	unsigned int index;
	double sum;
	bool tableFull;
};
