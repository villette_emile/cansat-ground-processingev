/*
 * CinManip.cpp
 *
 */

#include "CinManip.h"
#include <iostream>
#include <CancellationWatch.h>

void pressAnyKey(const std::string & msg) {
	std::string myMsg(msg);
	if (msg.length()> 0) myMsg+= "\n";
	if (CancellationWatch::cinDisturbed()) {
		myMsg+="[Press Return (possibly twice) to proceed...]";
	} else myMsg+="[Press Return to proceed...]";
	std::cin >> pause(myMsg);
}


