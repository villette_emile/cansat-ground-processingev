/*
 * FileMgr.cpp
 *
 * Source:  http://insanecoding.blogspot.com/2007/11/pathmax-simply-isnt.html
 * 			http://insanecoding.blogspot.com/2007/11/implementing-realpath-in-c.html
 */

#include "FileMgr.h"

#include <utility>
#include <vector>
#include <set>
#include <iostream>
#include <chrono>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>

#include <fcntl.h>
#include <sstream>  // TMP: remove when system not used anymore.
#include <cstdio>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>

#ifdef _WIN32
#  include <shlwapi.h>  // for FileMatchSpecA()
#else
#  include <fnmatch.h>
#endif


#include "CinManip.h" // for pressAnyKey

#include "DebugUtils.h"
constexpr auto DBG=false; // Activate for the function call tracing.
constexpr auto DBG_MSG=false;

using namespace std;

bool FileMgr::getCwd(string& path)
  {
	DBG_FCT
	bool success = false;
#ifdef _WIN32
	path="INVALID-PATH_ImplementFileMgr::getCwd";
	cout <<"ERROR: FileMgr::getCwd not implemented on windows" << endl;
	throw runtime_error("FileMgr function not supported on Windows.");
#else
    typedef pair<dev_t, ino_t> file_id;


    int start_fd = open(".", O_RDONLY); //Keep track of start directory, so can jump back to it later
    if (start_fd != -1)
    {
      struct stat sb;
      if (!fstat(start_fd, &sb))
      {
        file_id current_id(sb.st_dev, sb.st_ino);
        if (!stat("/", &sb)) //Get info for root directory, so we can determine when we hit it
        {
          vector<string> path_components;
          file_id root_id(sb.st_dev, sb.st_ino);

          while (current_id != root_id) //If they're equal, we've obtained enough info to build the path
          {
            bool pushed = false;

            if (!chdir("..")) //Keep recursing towards root each iteration
            {
              DIR *dir = opendir(".");
              if (dir)
              {
                dirent *entry;
                while ((entry = readdir(dir))) //We loop through each entry trying to find where we came from
                {
                  if ((strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..") && !lstat(entry->d_name, &sb)))
                  {
                    file_id child_id(sb.st_dev, sb.st_ino);
                    if (child_id == current_id) //We found where we came from, add its name to the list
                    {
                      path_components.push_back(entry->d_name);
                      pushed = true;
                      break;
                    }
                  }
                }
                closedir(dir);

                if (pushed && !stat(".", &sb)) //If we have a reason to contiue, we update the current dir id
                {
                  current_id = file_id(sb.st_dev, sb.st_ino);
                }
              }//Else, Uh oh, can't read information at this level
            }
            if (!pushed) { break; } //If we didn't obtain any info this pass, no reason to continue
          }

          if (current_id == root_id) //Unless they're equal, we failed above
          {
            //Built the path, will always end with a slash
            path = "/";
            for (vector<string>::reverse_iterator i = path_components.rbegin(); i != path_components.rend(); ++i)
            {
              path += *i+"/";
            }
            success = true;
          }
          fchdir(start_fd);
        }
      }
      close(start_fd);
    }
#endif
    return(success);
  }

bool FileMgr::chdir_getcwd(const string& dir, string& path)
{
	DBG_FCT
  bool success = false;
#ifdef _WIN32
	path="INVALID-PATH_ImplementFileMgr::chdir_getcwd";
	cout <<"ERROR: FileMgr::chdir_getcwd not implemented on windows dir=" << dir <<  endl;
	throw runtime_error("FileMgr function not supported on Windows.");
#else
  int start_fd = open(".", O_RDONLY); //Open current directory so we can save a handle to it
  if (start_fd != -1)
  {
    if (!chdir(dir.c_str())) //Change to directory
    {
      success = getCwd(path); //And get its path
      fchdir(start_fd); //And change back of course
    }
    close(start_fd);
  }
#endif
  return(success);
}

void FileMgr::relativeDirBaseSplit(const string& path, string& dir, string& base)
{
	DBG_FCT
  string::size_type slash_pos = path.rfind("/"); //Find the last slash
  if (slash_pos != string::npos) //If there is a slash
  {
    slash_pos++;
    dir = path.substr(0, slash_pos); //Directory is before slash
    base = path.substr(slash_pos); //And obviously, the file is after
  }
  else //Otherwise, there is no directory present
  {
    dir.clear();
    base = path;
  }
}

bool FileMgr::realpath_file(const string& path, string& resolved_path)
{
	DBG_FCT
  bool success = false;
  string dir;
  string base;
  relativeDirBaseSplit(path, dir, base);

  //If there is a directory, get the realpath() for it, otherwise the current directory
  if (dir.size() ? chdir_getcwd(dir, resolved_path) : getCwd(resolved_path))
  {
    resolved_path += base;
    success = true;
  }
  return(success);
}

bool FileMgr::readlink_internal(const string& path, string& buffer, size_t length)
{
	DBG_FCT
	bool success = false;
#ifdef _WIN32
	buffer="INVALID-BUFFER_ImplementFileMgr::readlink_internal";
	cout <<"ERROR: FileMgr::readlink_internal not implemented on windows path=" << path
			<< ", length=" << length << endl;
	throw runtime_error("FileMgr function not supported on Windows.");
#else
	if (length > 0)
	{
		char *buf = new(nothrow) char[length+1]; //Room for Null
		if (buf)
		{
			long amount = ::readlink(path.c_str(), buf, (size_t) length+1); //Give room for failure
			if ((amount > 0) && ((unsigned long) amount <= length)) //If > length, it was modified mid check
			{
				buf[amount] = 0;
				buffer = buf;
				success = true;
			}
			delete[] buf;
		}
	}
#endif
	return(success);
}

void FileMgr::build_path_base_swap(string &path, const string& newbase)
{
	DBG_FCT
  string dir;
  string base;
  relativeDirBaseSplit(path, dir, base);

  if (dir.size())
  {
    path = dir + newbase;
  }
  else
  {
    path = newbase;
  }
}


bool FileMgr::symlinkResolve(const string& start, string& end)
{
	DBG_FCT
	bool success = false;
#ifdef _WIN32
	end="INVALID-END_ImplementFileMgr::symlinkResolve";
	cout <<"ERROR: FileMgr::symlinkResolve not implemented on windows start=" << start << endl;
	throw runtime_error("FileMgr function not supported on Windows.");
#else
	typedef pair<dev_t, ino_t> file_id;


  if (start.size())
  {
    string path = start; //Need a modifyable copy
    struct stat sb;
    set<file_id> seen_links;

    bool resolved_link;
    do //The symlink resolve loop
    {
      resolved_link = false;
      if (!lstat(path.c_str(), &sb))
      {
        file_id current_id(sb.st_dev, sb.st_ino);
        if (seen_links.find(current_id) == seen_links.end()) //Not a link we've seen
        {
          seen_links.insert(current_id); //Add to our set

          if (S_ISLNK(sb.st_mode)) //Another link
          {
            string newpath;
            if (readlink_internal(path, newpath, (size_t) sb.st_size))
            {
              if (newpath[0] == '/') //Absolute
              {
                path = newpath;
              }
              else //We need to calculate the relative path in relation to the current
              {
                build_path_base_swap(path, newpath);
              }
              resolved_link = true;
            } //Else, Link can't be read, time to quit
          }
          else //Yay, it's not a link! got to the last part finally!
          {
            success = realpath_file(path, end);
          }
        } //Else, Nice try, someone linked a link back into a previous link during the scan to try to trick us into an infinite loop
      } //Else, Dangling link, can't resolve
    } while (resolved_link);
  }
#endif
  return(success);
}

bool FileMgr::realPath(const string& path, string& resolved_path, bool resolve_link)
{
	DBG_FCT
  bool success = false;
  if (path.size())
  {
    struct stat sb;
    if (!stat(path.c_str(), &sb))
    {
      bool (*rp)(const string&, string&) = resolve_link ? symlinkResolve : realpath_file;
      success = S_ISDIR(sb.st_mode) ? chdir_getcwd(path, resolved_path) : rp(path, resolved_path);
    }
  }
  return(success);
}

/** Tested on MacOS+Windows */
bool FileMgr::deleteFile(	const string& pathToFile,
							bool silentIfInexistent)
{
	DBG_FCT
	bool result=false;
	if (!fileExists(pathToFile))
	{
		if (!silentIfInexistent)
		{
			cout << "Cannot delete file '" << pathToFile << "' (no such file)" << endl;
			return false;
		}
		else return true;
	}
	if (directoryExists(pathToFile)) {
		cout << "Cannot delete file '" << pathToFile << "' (is a directory)" << endl;
		return false;
	}

	int err = unlink(pathToFile.c_str());
	LOG_IF(DBG_MSG, DEBUG) << "Unlink of '" << pathToFile << "': return value = " << err << endl;
	if (err != 0) {
		cout << "*** Error: cannot unlink file '"<< pathToFile << "'" << endl;
		cout << "    Error " << errno << ": " << strerror(errno) << endl;
	}
	else result=true;

	return result;
}

/** Tested on MacOS & Windows */
bool FileMgr::moveFile(	const std::string& pathToFile,
							const std::string& destinationDir,
							bool silentlyOverwrite,
							bool interactive) {
    DBG_FCT
	if (!fileExists(pathToFile)) {
		cout << "Cannot move file '" << pathToFile <<"' (inexistent)" << endl;
		if (interactive) pressAnyKey();
		return false;
	}

    if (fileExists(destinationDir))
    {
    	cout << "Cannot move file. Destination directory '"<< destinationDir << "' is a regular file" << endl;
		if (interactive) pressAnyKey();
		return false;
    }

	if (!createDirectory(destinationDir,interactive,true)) { // interactive, silent if exists
		return false;  // Error message and key press is in createDirectory.
	}

	string dirName, baseName;
	relativeDirBaseSplit(pathToFile, dirName, baseName);
	string newPath=destinationDir;
	addFinalSlash(newPath);
	newPath+=baseName;

	if (fileExists(newPath) && !silentlyOverwrite) {
		cout << "File '" << newPath << "' exists." << endl;
		cout << "Aborted." << endl;
		if (interactive) pressAnyKey();
		return false;
	} else {
		// destination file does not exist or can be overwritten (rename does that on MacOS,
		// but fails with "Operation not permitted" on Windows: let's be careful and remove
		// the file that may be overwritten first)
		if (!deleteFile(newPath, true)) {
			cout << "Error attempting to remove file '" << newPath << "'" << endl;
			return false;
		};
	}


	bool result=rename(pathToFile.c_str(), newPath.c_str()); // Portable !
	if (result != 0) {
		// This error we report anyway. NB: It can be because both files are not in the same filesystem!
		// Moving across file systems currently not supported.
		cout << "Error renaming '" << pathToFile << "' to '" << newPath << "'" << endl;
		cout << "rename(2): " << strerror(result) << endl;
		if (interactive) pressAnyKey();
		return false;
	}

    return true;
}

/** Tested on MacOS & Windows */
bool FileMgr::deleteFiles(	const string& pathToDir,
							const string& basenameWithWildcards,
							bool echoOnCout)
{
	DBG_FCT
	bool result;

	if (DBG) echoOnCout=true;

	vector<string> names;
	result= listDirectory(names,pathToDir, "");
	LOG_IF(DBG_MSG, DEBUG) << "  Found " << names.size() << " files in dir '" << pathToDir << endl;
	if (result) {
		for (auto basename : names )
		{
			bool matchFound;
			LOG_IF(DBG_MSG, DEBUG) << "processing '" << basename << endl;
#ifdef _WIN32
			matchFound = PathMatchSpecA(basename.c_str(), basenameWithWildcards.c_str());
#else
			matchFound = fnmatch(basenameWithWildcards.c_str(),basename.c_str(),0) == 0;
#endif
			if (matchFound)
			{
				string path=pathToDir+"/"+basename;
				if (isRegularFile(path)) {
					if (echoOnCout) cout << "  Deleting " << path << endl;
					result=deleteFile(path);
				}
				else {
					if (echoOnCout) cout << "  Skipped " << path << "(not regular)" << endl;
				}
			}
		}
	}

	return result;
}

bool FileMgr::copyFiles(	const string& pathToDir,
							const string& basenameWithWildcards,
							const string& destinationDir,
							bool echoOnCout)
{// TODO: portable file copy functions. This is ugly and Unix only...
	DBG_FCT
#ifdef _WIN32
	cout <<"ERROR: FileMgr::copyFile not implemented on windows" << endl;
	cout << "pathToDir: " << pathToDir
		 << ", basenameWithWildcards: " << basenameWithWildcards
		 << ", destinationDir: " << destinationDir
		 << ", echoOnCout:" << echoOnCout << endl;
	throw runtime_error("FileMgr function not supported on Windows.");
#else
	if (!directoryExists(destinationDir)) {
		cout << "*** Cannot copy files to '"<< destinationDir << "' (directory does not exist)" << endl;
		return false;
	}
	stringstream cmd;
	cmd << "cp -f " << pathToDir <<"/" << basenameWithWildcards << " " << destinationDir;
	if (echoOnCout) cout << cmd.str() << "..." << endl;
	system(cmd.str().c_str());
	return true;
#endif
}

/** Tested on MacOS+Windows */
bool FileMgr::isRegularFile(const string &filePath)
{
	DBG_FCT
    struct stat path_stat;
    int result= stat(filePath.c_str(), &path_stat);
    if (result !=0) {
    	if (errno == ENOENT) return false;
    	else {
    		stringstream msg;
    		msg <<  "FileMgr::isRegularFile: Error calling stat() for '" << filePath << "': " << strerror(errno) << endl;
    		throw runtime_error(msg.str());
    	}
    }
    return S_ISREG(path_stat.st_mode);
}

std::string FileMgr::getExtension(const std::string &path, bool lastExtensionOnly) {
	DBG_FCT
	string extension;
	size_t slashIndex = path.rfind("/");
	string baseName=path.substr(slashIndex+1);
	size_t dotIndex;
    if (lastExtensionOnly) {
    	dotIndex = baseName.rfind(".");
    }
    else {
    	dotIndex = baseName.find(".");
    }
	if (dotIndex != std::string::npos) {
		extension=baseName.substr(dotIndex+1);
	}
	return extension;
}

bool FileMgr::hasExtension(const string &path, const string& extension) {
	DBG_FCT
	assert(!extension.empty());
	string extensionWithDot(".");
	if (extension[0] == '.') {
		extensionWithDot= extension;
	} else extensionWithDot+=extension;
	size_t index = path.rfind(extensionWithDot);
	LOG_IF(DBG_MSG, DEBUG) << "hasExtension: path="<< path << " (len=" << path.length() << ")" ;
	LOG_IF(DBG_MSG, DEBUG) << "hasExtension: ext="<< extensionWithDot << " (len=" << extensionWithDot.length() << ")" ;
	LOG_IF(DBG_MSG, DEBUG) << "hasExtension: index ="<< index;

	// extension is present if found and at the end of the string.
	return ((index != std::string::npos) && (index + extensionWithDot.length() == path.length())) ;
}

bool FileMgr::replaceExtension(	const string &original,
								const string &originalExt,
								const string &newExt, string &result,
								bool interactive)
{
	DBG_FCT
	assert(originalExt.length() == newExt.length());
	size_t index = original.find(originalExt);
    if(index == std::string::npos) {
    	if (interactive) {
    		cout << "Error: file '" << original << "' should have a '" << originalExt << "' extension." << endl;
    		pressAnyKey();
    	}
	   	return false;
	}
    result=original;
	result.replace(index, newExt.length(), newExt);
	return true;
}

void FileMgr::addFinalSlash(std::string &directoryPath){
	DBG_FCT
	if (directoryPath.back() !='/')  directoryPath+='/';
}

void FileMgr::removeFinalSlash(std::string &directoryPath){
	DBG_FCT
	if (directoryPath.back() =='/')  directoryPath.pop_back();
}

/** Tested on MacOS & Windows */
bool FileMgr::listDirectory(	vector<string> &entries,
								const string &srcDirectory,
								const string &extension,
								bool regularFilesOnly,
								bool appendToEntries) {
	DBG_FCT
	return listDirectory(entries, srcDirectory, extension, "",regularFilesOnly,appendToEntries);
}

/** Tested on MacOS & Windows */
bool FileMgr::listDirectory(	vector<string> &entries,
								const string &srcDirectory,
								const string &extension,
								const string &startString,
								bool regularFilesOnly,
								bool appendToEntries) {
	DBG_FCT
	LOG_IF(DBG_MSG,DEBUG) << "Extension='" << extension << "', startString='" << startString << "', regularOnly=" << regularFilesOnly;
	if (srcDirectory.empty()) return false;
	DIR* dirp = opendir(srcDirectory.c_str());
	if (!dirp){
		cout << "Error opening directory '"<< srcDirectory << "': " << endl;
		cout << strerror(errno)<< endl;
		return false;
	}
	string directoryPath(srcDirectory);
	if (!appendToEntries) entries.clear();
	addFinalSlash(directoryPath);
	bool checkExtension=extension.length() > 0;
	bool checkstartString=startString.length() > 0;
	bool extensionOK, startStringOK;
	struct dirent *dp;
	while ((dp = readdir(dirp)) != NULL) {
		string fileName=dp->d_name;
		LOG_IF(DBG_MSG,DEBUG) << "  Processing '" << fileName << "'";
		if (fileName.length() < extension.length()) continue;
		if (regularFilesOnly && !FileMgr::isRegularFile(directoryPath+fileName)) continue;
		LOG_IF(DBG_MSG,DEBUG) << "  Checking extension...";
		extensionOK =  (!checkExtension ||
						fileName.substr(fileName.length()-extension.length(),extension.length()) == extension);
		// Do not evaluate startString if extension is nok
		startStringOK = extensionOK &&
				        (!checkstartString || fileName.substr(0,startString.length()) == startString);
		LOG_IF(DBG_MSG,DEBUG) << "  Extension, starting: " << extensionOK << ", " << startStringOK;
		if (!extensionOK && !startStringOK) continue;
		entries.push_back(fileName);
	}
	closedir(dirp);
	return true;
}

/** Tested on MacOS+Windows */
bool FileMgr::fileExists(	const std::string& filePath) {
	DBG_FCT
	bool result=false;
	mode_t mode;
	if (fileEntryExists(filePath, mode)) {
#ifdef _WIN32
		// OPEN ISSUE: S_ISLINK is not provided by MinGW's stat.h header. How to handle links?
		result = S_ISREG(mode);
#else
		result= (S_ISREG(mode) || S_ISLNK(mode));
#endif
	}
	LOG_IF(DBG_MSG, DEBUG) << "fileExists(" << filePath << "): " << result;
	return result;
}

/** Tested on MacOS+Windows */
bool FileMgr::fileEntryExists(	const std::string& path, mode_t &stmode ) {
	DBG_FCT
	struct stat path_stat;
	int result=stat(path.c_str(), &path_stat);
	if (result !=0) {
		if (errno == ENOENT) return false;
		else {
			stringstream msg;
			msg << "FileMgr::fileEntryExists: Error calling stat() for '" << path << "': " << strerror(errno) << endl;
			throw runtime_error(msg.str());
		}
	}
	stmode=path_stat.st_mode;
	return true;
}

/** Tested on MacOS+Windows */
bool FileMgr::directoryExists(const std::string& dirPath) {
	DBG_FCT
	mode_t mode;
	bool result=false;
	if (fileEntryExists(dirPath, mode)) {
		result=S_ISDIR(mode);
	}
	LOG_IF(DBG_MSG,DEBUG)<<"Directory exists(" << dirPath << ": " << result;
	return result;
}

/** Tested on MacOS+Windows */
bool FileMgr::removeDirectory(	const std::string& dirPath,
									bool interactive,
									bool silentIfInexistent) {
	DBG_FCT
	LOG_IF(DBG_MSG, DEBUG) << "Removing '" << dirPath << "' (silent=" << silentIfInexistent << ")";
	if (!directoryExists(dirPath)) {
		if (silentIfInexistent)  {
			return true;
		}
		else {
			cout << "FileMgr::removeDirectory: Directory '" << dirPath <<"' does not exist." << endl;
			return false;
		}
	}

	int status = rmdir(dirPath.c_str());
	if (interactive && (status!= 0)) {
		cout << "Error removing directory '" << dirPath << endl;
		cout << strerror(errno)<< endl;
	}

	if (directoryExists(dirPath)) {
		cout << "FileMgr::removeDirectory: " << endl;
		cout << "'" << dirPath  << "' does still exists and status=" << status <<endl << flush;
		cout << strerror(errno) << endl;
		status=-1;
	}
	return (status==0);
}

/** Tested on MacOS+Windows */
bool FileMgr::createDirectory(	const string& dirPath,
								bool interactive,
								bool silentIfExists) {
	DBG_FCT
	// Permissions: rwxrwxr-x
    LOG_IF(DBG_MSG, DEBUG) << "Creating '" << dirPath << "'";
    if (directoryExists(dirPath)) {
    		if (silentIfExists)  {
    			LOG_IF(DBG_MSG, DEBUG) << "Directory '" << dirPath << "' exists (silently ignored)." << endl;
    			return true;
    		}
    		else {
    			cout << "FileMgr::createDirectory: Directory '" << dirPath <<"' exists." << endl;
    			return false;
    		}
    	}

	string dir, base;
	relativeDirBaseSplit(dirPath, dir,base);
	removeFinalSlash(dir);
	if (dir.size())
	{
		// Create parent directory first, always be silent if
		// it already exists.
		if (!createDirectory(dir,interactive,true)) return false;
	}
#ifdef _WIN32
	int status = mkdir(dirPath.c_str());
#else
	int status = mkdir(dirPath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
#endif
	if (interactive && (status!= 0)) {
		cout << "Error creating directory '" << dirPath << endl;
		cout << strerror(errno)<< endl;
	}
	if (!directoryExists(dirPath)) {
		cout << "FileMgr::createDirectory: " << endl;
		cout << "'" << dirPath  << "' does not exist and status=" << status <<endl << flush;
		cout << strerror(errno) << endl;
		status=-1;
	}
	return (status==0);
}

std::string FileMgr::getTimestampedFileName(const std::string& label, const std::string& extension)
{
	DBG_FCT
	using namespace std::chrono;

    struct std::tm * dt;
    char buffer [30];

    auto time_point = system_clock::now();
    std::time_t now_c = system_clock::to_time_t(time_point);
    dt = std::localtime(&now_c);
    std::strftime(buffer, sizeof(buffer), "%Y%m%d_%H-%M-%S", dt);
    string fileName=std::string(buffer);
    if (!label.empty()) fileName += (string("_")+label);
    if (!extension.empty()) fileName += (string(".")+extension);
    return fileName;
}

bool FileMgr::copyFile(const std::string& originalPath,
		const std::string& destinationPath) {
	if (!fileExists(originalPath)) {
		cout << "Error: file '" << originalPath << "' does not exist" << endl;
		return false;
	}
	string destinationDir;
	string destinationName;
	relativeDirBaseSplit(destinationPath,destinationDir, destinationName);
	if (!directoryExists(destinationDir)) {
		cout << "Error: directory '" << destinationDir << "' does not exist" << endl;
		return false;
	}
	mode_t mode;
	if (fileEntryExists(destinationPath, mode)) {
		cout << "Error: file entry '" << destinationPath << "' exists" << endl;
		return false;
	}

	return cp(destinationPath.c_str(),originalPath.c_str());
}

bool FileMgr::cp(const char *to, const char *from)
{
  // The original version used goto statements, but I couldn't get it to compile.
  bool error=false;
  int fd_from = open(from, O_RDONLY);
  if(fd_from < 0)
    return false;
  struct stat Stat;
  if(fstat(fd_from, &Stat)<0) {
	  cout << "  cp: fstat failed." << endl;
	  error= true;
  }
  void *mem= MAP_FAILED;
  if (!error) {
	  if (Stat.st_size==0) {
		  cout << "  cp: Error: original file is empty." << endl;
		  error=true;
	  } else {
		  mem = mmap(NULL, Stat.st_size, PROT_READ, MAP_SHARED, fd_from, 0);
		  if(mem == MAP_FAILED){
			  cout << "  cp: mmap failed for size=" << Stat.st_size << "." << endl;
			  error= true;
		  }
	  }
  }
  int fd_to=-1;
  if (!error) {
	  fd_to = creat(to, 0666);
	  if(fd_to < 0){
		  cout << "  cp: file creation failed." << endl;
		  error= true;
	  }
  }
  if (!error) {
	  ssize_t nwritten = write(fd_to, mem, Stat.st_size);
	  if(nwritten < Stat.st_size){
		  cout << "  cp: copy size inconsistent." << endl;
		  error= true;
	  }
  }
  if (!error)  {
	  if(close(fd_to) < 0) {
		  fd_to = -1;
		  cout << "  cp: error closing file descriptor (to)." << endl;
		  error=true;
	  }
  }
  close(fd_from);

  if (!error) {
	  return true;
  }
  else {
	  int saved_errno = errno;
	  close(fd_from);
	  if(fd_to >= 0) close(fd_to);
	  errno = saved_errno;
	  return false;
  }
}
