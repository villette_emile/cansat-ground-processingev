/*
 * SocketStringSender.cpp
 */

//#define ASIO_ENABLE_HANDLER_TRACKING

#include "SocketStringSender.h"
#include <exception>
#include "CancellationWatch.h"

#include "DebugUtils.h"
constexpr auto DBG=false;				// debugging in main thread
constexpr auto DBG_SERVER_THREAD=false; // debugging in the server thread.
constexpr auto CONNECTION_STATUS_ON_COUT=true; // Write on cout when client is connected/disconnected.

using namespace std;

// ------------- Methods executed in the server thread --------------------

void SocketStringSender::ServerThread_Function() {
	try {

	while(listenForConnexion.load()) {

		AcceptClientConnexion();

    	// Process output queue as long as a client is connected.
    	while (clientConnected.load()) {
    		if (!outputQueue.empty())
    		{
    			LOG_IF(DBG_SERVER_THREAD, DEBUG) <<  "Queue not empty";
    			outputQueueMutex.lock();
    			string msg=outputQueue.front() + "\n";
    			outputQueue.pop();
    			outputQueueMutex.unlock();
    			// Use async::write, not async_write_some to make sure the whole buffer is sent.
    			asio::async_write(	*tcpSocket,
    								asio::buffer(msg),
    								[this] (const asio::error_code& ec, size_t bytesTransferred) {
    									RecordWritten_Handler(ec, bytesTransferred);
    								});
    			io_context.run();
    			io_context.restart();
    		}
    		else this_thread::sleep_for(std::chrono::milliseconds(50));
    	}
    }
    LOG_IF(DBG_SERVER_THREAD, DEBUG) << "Server thread exiting.";
	}
	catch (asio::system_error &e) {
		cout << "System error #" << e.code() << " in server thread on port " << portNbr << ":" << endl;
		cout << e.code() << ": " << e.what() << endl;
		cout << "Server thread exiting" << endl;
	}
	catch (exception &e) {
		cout << "Unanticipated exception in server thread: report to development team" << endl;
		cout << e.what()<< endl;
		cout << "Server thread exiting" << endl;
	}
}

void SocketStringSender::AcceptClientConnexion()
{
	using asio::ip::tcp;

	bool done = false;
	unsigned int  portIncrement=0;
	do  {
		// NB: This thread is the only one to modify the socket and socket pointer.
		if (tcpSocket) {
			delete tcpSocket;
		}
		tcpSocket= new tcp::socket(io_context);

		// Add an accept call to the service.
		if (CONNECTION_STATUS_ON_COUT) {
			if (visualFeedback) cout << endl;
			cout << "Waiting for incoming TCP connection on port " << portNbr << "..." << endl;
		}

		try {
			tcp::acceptor acceptor( io_context, tcp::endpoint( tcp::v4(), portNbr ) );
			LOG_IF(DBG, DEBUG) << "Calling async_accept";
			acceptor.async_accept( *tcpSocket, [this] (const asio::error_code& ec) { ConnectionAccepted_Handler(ec); });
			LOG_IF(DBG, DEBUG) << "Back from async accept";
			// Process event loop for accepting.
			// This function will return after the connection is accepted (possibly with error)
			// or when the event-loop is stopped.
			// Every single call to run() must be followed by a restart() before any async request is
			// sent, otherwise the callback handler will not be called.
			io_context.run();
			io_context.restart();
			done=true;
		}
		catch (asio::system_error &e) {
			if ((e.code().value() == 48) && (portIncrement < 10)) {
				// Port already in used, let's try the next one
				cout << "Port " << portNbr << " is in use: switching to the next one." << endl;
				portNbr++;
				portIncrement++;
				// tcpSocket->close(); // close socket: we cannot accept if it is open.
			} else throw;
		}
	} while (!done);
}

void SocketStringSender::DisconnectClient() {
	if (CONNECTION_STATUS_ON_COUT) {
		if (visualFeedback) cout << endl;
		cout << "*** Client disconnected ***" << endl;
	}
	clientConnected = false;
}

void SocketStringSender::ConnectionAccepted_Handler(const asio::error_code& error){
	if (error) {
		cout << "*** Error when connecting client on port " << portNbr << endl;
		cout << "*** " << error << ": " << error.message() << endl;
		DisconnectClient();
	}
	else {
		if (CONNECTION_STATUS_ON_COUT) {
			if (visualFeedback) cout << endl;
			cout << "Client connected on port " << portNbr << endl;
		}
		clientConnected=true;
	}
}

void SocketStringSender::RecordWritten_Handler(const asio::error_code& error,
	      size_t bytesTransferred){
	if (error) {
		cout << endl;
		cout << "*** Error when transmitting record on port " << portNbr << endl;
		cout << "*** " << error << ": " << error.message() << endl;
		DisconnectClient();
	}
	else {
		LOG_IF(DBG_SERVER_THREAD, DEBUG) << bytesTransferred << " bytes transferred";
	}
}

// ------------------ Methods executed in the main thread --------------------------

SocketStringSender::SocketStringSender(	unsigned short thePortNbr,
								ClientDisconnectOption_t option,
								bool visualFeadback)
: portNbr(thePortNbr),
  doSuspendOnClientDisconnection(option==suspendOnClientDisconnection),
  serverThread(nullptr),
  io_context(),
  tcpSocket(nullptr),
  listenForConnexion(true),
  clientConnected(false),
  visualFeedback(visualFeadback)
{
	DBG_FCT
}

void SocketStringSender::stopServerThread() {
	DBG_FCT
	if (serverThread) {
		// Signal thread to stop
		listenForConnexion=false;
		clientConnected=false;
		// stop asynchronous event-loop to cause io_context::run() to return
		io_context.stop();
		// Wait for termination
		serverThread->join();
		delete serverThread;    // This method can be called more than once.
		serverThread=nullptr;
	}
}

SocketStringSender::~SocketStringSender() {
	DBG_FCT
	stopServerThread();
	if (serverThread) delete serverThread;
	if (tcpSocket) delete tcpSocket;
}

void SocketStringSender::SendRecordToOutputQueue(const std::string& record, unsigned int &counter) {
	DBG_FCT
	outputQueueMutex.lock();
	outputQueue.push(record);
	outputQueueMutex.unlock();
	if (DBG) {
		LOG_IF(DBG, DEBUG) << "Sent:   '" << record << "'";
	} else {
		if (visualFeedback) {
			cout << '.' << flush;
			if (!(counter % 50)) {
				cout << endl;
				counter=0;
			}
		}
	}
}

void SocketStringSender::IgnoreRecord(const std::string& record, unsigned int &counter) {
	DBG_FCT
	if (DBG) {
		LOG_IF(DBG, DEBUG) << "Ignored:'" << record << "'";
	} else {
		if (visualFeedback) {
			cout << '!' << flush;
			if (!(counter % 50)) {
				cout << endl;
				counter=0;
			}
		}
	}
}

bool SocketStringSender::checkClientIsConnected()
{
	DBG_FCT
	bool result=false;

	if(clientConnected.load()) {
		result=true;
	}
	else {
		if (doSuspendOnClientDisconnection) {
			// Block until client connects or user cancels
			CancellationWatch watch;
			while (!clientConnected.load() && !watch.cancelled())
			{
				this_thread::sleep_for(std::chrono::milliseconds(50));
			}
			if (!watch.cancelled()) result=true;
		}  // if
	} // else
	return result;
}

bool SocketStringSender::prepare()
{
	DBG_FCT
	if (!serverThread) {
		serverThread=new thread([this] { this->ServerThread_Function(); });
	}
	// Preparation is ok if !doSuspendOnClientDisconnection or if client is connected.
	return (checkClientIsConnected() || !doSuspendOnClientDisconnection);
}

bool SocketStringSender::send(const std::string& str) {
	DBG_FCT
	static unsigned int counter=0;
	counter++;

	if (checkClientIsConnected()) { // This line is blocking if doSuspendOnClientDisconnection
									// is true;
		SendRecordToOutputQueue(str, counter);
	}
	else {
		IgnoreRecord(str, counter);
	}
	return true;
}

void SocketStringSender::terminate() {
	DBG_FCT
	stopServerThread();
};
