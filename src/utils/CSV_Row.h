/*
 * CSV_Row.h
 */

#pragma once

#include <vector>
#include <string>
#include <array>

/** @ingroup Utilities
 *  @brief Utility class to parse strings assuming a comma-separated (CSV) format, and access
 *  individual elements.
 * Source: https://stackoverflow.com/questions/1120140/how-can-i-read-and-parse-csv-files-in-c
 *
 * Usage: @code
 *  std::ifstream file("plop.csv");
 *  CSV_Row row;
 *  while(file >> row)
 *  {
 *  	 std::cout << "4th Element(" << row[3] << ")\n";
 *  }
 * @endcode
 *
 * Improvement, CSPU, 2018-19:
 *   - now skip lines beginning with a '#' (comment lines).
 *   - added line number support.
 */
class CSV_Row
{
    public:
		/** Default constructor. If used, call #setStream before calling next(). */
		CSV_Row();
		/*! @param str The input stream to read from.
		 */
		CSV_Row(std::istream& str) : stream(&str){};
		/*! Method to call  before calling next(), if the object was created using the default constructor.
		 *   @param str The input stream to read from.
		 */
		void setStream(std::istream& str) { stream = &str;} ;
		/** Access a particular element in the row as a string. Use after next() was called at least once
		 *  @param index The index of the element accessed (first element's index is 0.
		 *  @return the string between the (index-1)th and index-th comma.
		 */
        std::string const& operator[](std::size_t index) const ;

        /** Get the number of elements found in the current row.
         * @return The number of elements.
         */
        std::size_t size() const { return m_data.size(); }

        /*! Read new row, skipping comments (lines starting with '#') and lines containing only blank characters..
         *  Expected use: @code while (row.next()) { process row }; @endcode
         *  @param skipHeader If true, the first non empty line which does not start with a '#' character is skipped,
         *  	   assuming it is the header.
         *  @return True if a valid row was found
         */
        bool next(bool skipHeader=false);
        /*! Return the complete row.
         * @return The complete row as a string.  */
        std::string getCompleteRow() const {return currentRow; } ;

        /** Obtain the number of the line currently read from the file. Skipped lines are taken into
         *  account.
         *  @return Current line number.
         */
        unsigned long getCurrentLine() const { return currentLineNumber;};
        /*! Skip 1 non-empty, non comment line in the stream.
         *  @return The number of lines actually skipped in the file (1 + number of comment lines + number of empty lines).
         */
        static unsigned int skipOneRow(std::istream &is);

        /** @name Utility methods to perform numeric parsing. */
        /** @{ */
        float getFloat(std::size_t idx) const {return std::stof(operator[](idx));} ;
        double getDouble(std::size_t idx) const {return std::stod(operator[](idx));} ;
        int getInt(std::size_t idx) const {return std::stoi(operator[](idx));} ;
        unsigned int getUInt(std::size_t idx) const {return (unsigned int) std::stoul(operator[](idx));} ;
        long getLong(std::size_t idx) const {return std::stol(operator[](idx));} ;
        unsigned long getULong(std::size_t idx) const {return std::stoul(operator[](idx));} ;
        bool getBool(std::size_t idx) const {return (std::stoi(operator[](idx)) != 0);} ;
        /** @} */
        /*! Read next line and return double from first column */
        double getDoubleFromNextLine();
        /** Read next line and return first 3 doubles in an array
         *  @param arr The array to fill with the doubles from the file.
         *  @return True if a valid line was found.
         */
        bool getDoubleArrayFromNextLine(std::array<double, 3> &arr);

    protected:
        /*! Find the next line containing data (i.e. non-empty, non-comment).
         *  @return True if a line is found, false otherwise.
         */
        bool readNextLineFromStream(bool skipHeader);

    private:
        std::string currentRow;		/*!< The whole row currently being processed */
        std::vector<std::string>    m_data;  /*!< The elements composing the current line */
        unsigned long currentLineNumber{};   /*!< current line number */
        std::istream* stream{};     /*!< The stream from which rows are retrieved */
};


