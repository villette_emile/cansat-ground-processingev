/*
 * CalculationUtils.h
 *
 */

#pragma once

#include <array>

/** @ingroup Utilities
 *  Print differences between elements of two arrays of floats
 *  @todo Turn this function into a template. */
void printDifferences(const std::array<float, 3> &a, const std::array<float,3> &b);

