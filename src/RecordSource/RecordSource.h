/*
 *  RecordSource.h
 */

/* Definition of the package.
 * Every class in the package should include a tag @ingroup RecordSource
 * in the class documentation block.
 */

 /** @defgroup RecordSource Record Source
 *  @brief The set of ground processing classes dedicated to feeding Record Processors, obtained from various sources.
 *
 *  The Record Source package contains all classes used in the Ground Segment feed records from
 *  serial line, from file, from TCP_Sockets, from random generation etc... to Processors.
 *
 *  _Dependencies_\n
 *  This package relies on:
 *  	- the Utils package
 *  	- the easylogging++ library
 *  	- Record Processing
 *
 *  @todo //.
 *
 *
 *  _History_\n
 *  The library was created by the 2018-2019 Cansat team (IsaTwo).
 */

#pragma once

#include "RecordProcessor/RecordProcessor.h"
#include "CancellationWatch.h"
#include "DebugUtils.h"

/** @ingroup RecordSource
 *  @brief Abstract base class for RecordSources, i.e. objects able to feed RecordProcessors with
 *  data.
 *
 *  @par Usage
 *  Subclass RecordSource:
 *  - overload at least method feedOneRecordToProcessor().
 *  - If any preparation that must be performed each time RecordSource::run() is called
 *  or that must occur _just_ before RecordSource starts feeding records
 *  to the processor), overload the default prepare() method (one time preparation should
 *  be in the constructor.
 *  - If any action is required after all records have been fed to the processor,
 *    overload method terminate().
 *
 *  RecordSource should be used as follow:  @code
 *  MyProcessorProcessorSubclass processor;
 *  MyRecordSourceSubclass       src(...., processor);
 *  src.run(RecordSource::withVisualFeedback);
 *  (or src.run(RecordSource::noVisualFeedback); @endcode
 *
 *  If required, several calls to run() are allowed (each one will result in calls to
 *  prepare(), feedOneRecordToProcessor() (as long as it returns true) and terminate();
 *
 */
class RecordSource {
public:
	typedef enum { 	allowUserInterrupt=1,
					doNoAllowUserInterrupt=0 } allowUserInterrupt_t;
	typedef enum { 	withVisualFeedback=1,
					noVisualFeedback=0 } visualFeedback_t;

   /** Constructor including a reference to the processor to feed.
    *  @param theProcessor The processor to feed with CSV_Records.
    *  @param visualFeedback If true, a character is output on cout every 100 records, to make
    *						  processing visible
    */
	RecordSource(Processor& theProcessor, visualFeedback_t visualFeedback);
	virtual ~RecordSource();

	/** Constructor without a reference to the processor to feed.  It is provided
	 *  for convenience (in some configurations, the processor is not known when the
	 *  source is created) but requires a call to #setProcessor() or #appendProcessor()
	 *  before method #run()
	 *  can be called.
	 *  @param visualFeedback If true, a character is output on cout every 100 records, to make
	 *						  processing visible
	 */
	RecordSource(visualFeedback_t visualFeedback);

	/** Set the processor that will be fed by this source.
	 *  This method should only be used when the source was not associated with a processor in the
	 *  constructor.
	 *  @warning calling this method while processor is already set results in a
	 *  		 runtime exception.
	 */
	void setProcessor(Processor& theProcessor) {
		if (processor) {
			throw runtime_error("*** RecordSource::setProcessor() called while processor already set");
		}
		processor=&theProcessor;
	}

	/** Append a processor chain of processors that will be fed by the source.
	 *  This method should only be used when the source was not associated with a processor in the
	 *  constructor.
	 *  NB: if the provided processor is the head of a chain, the whole chain will
	 *      be appended.
	 *  @warning calling this method while processor is already set results in a
	 *  		 runtime exception.
	 */
	void appendProcessor(Processor& theProcessor) {
		if (processor) processor->appendProcessor(theProcessor);
		else setProcessor(theProcessor);
	}

	/** Delete the whole chain of processors. Only use if all processors were dynamically
	 *  allocated.
	 */
	void deleteProcessorChain();

	/** Prepare the data source, feed every available record to the processor and
	 *  terminate.
	 *  @warning If the object was construced without providing a processor, be sure
	 *  		 to call #setProcessor() before running the source.
	 *  @param userInterrupt Define whether the user is allowed to interrupt the
	 *         processing before all records are processed.
	 *  @return The number of records transfered to the processor.
	 */
    virtual uint32_t run(allowUserInterrupt_t userInterrupt);

    virtual void printProcessorChainDescription(ostream& os) const;

protected:
    /** @name Methods to be overloaded by subclass
     *  @{
     */
    /** Overload this method to perform any preparation that cannot be performed
     *  in the constructor (optional). It is called as part of the call to run().
     *  @return true if preparation was successful and the record source is ready
     *          to feed the processor, false otherwise.
     */
	virtual bool prepare() {return true; };

	/** Feed the processor with one data record. This method must be implemented in the
	 *  subclass.
	 *  @param  processor The object responsible for processing every line of data received from the port.
	 *  @param	recordProcessed True if a record was found and processed. In this case, the result of the
	 *  		processing is in parameter success.
	 *  @param  success This parameter is updated to true by the subclass if the processing of
	 *  	            the record was successful, and to false otherwise. The value is irrelevant
	 *  	            if recordProcessed is false.
	 *  @return true if the method should be called again (because there is data left to process)
	 *          false if the last record has been processed or some error make it impossible
	 *          to process additional records.
	 */
	virtual bool feedOneRecordToProcessor(Processor &processor, bool& recordProcessed, bool &success)=0;

	/** Overload this method to perform any action required after the processing.
	 *  (optional) */
    virtual void terminate() {};
    /** @} */ // End of group of methods to overload

    /** Feed the processor with the data records. This method should does not return
     * until every available data has been passed to the processor.
     * @param allowUserInterrupt If True, the user is allowed to interrupt the
     *        source before all records are processed.
     * @return The number of records processed.
     */
   	virtual uint32_t feedProcessor(allowUserInterrupt_t allowUserInterrupt);

private:
    Processor *processor;	 /**< A pointer to the processor to feed */
	bool visualFeedback;	 /**< If true, a character is output on cout every 100 records, to make
								  processing visible */
	CancellationWatch *cancellationWatch;
							 /**< If user is allowed to interrupt, this will be the CancellationWatch
							  *   object */
};
