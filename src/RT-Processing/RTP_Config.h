/*
 * RTP_Config.h
 */

#pragma once

/** @ingroup Application
 *  @brief All configuration constants for the Ground processing software */
class RTP_Config {
public:
	// Command line default values (all strings!)
	static constexpr const char* const defaultTCP_Port="5000";
	static constexpr const char* const defaultSerialBaudRate="115200";

	// ----------------------------------------------------------------------------------------------------
	// Directories organisation
	// Files received from the CanSat, or simulation thereof.
	static constexpr const char* const dir_Records="data/A_Records";
	static constexpr const char* const dir_IMU_Calibration="data/C_Calibration/IMU";
	static constexpr const char* const dir_CameraCalibration="data/C_Calibration/camera";
	static constexpr const char* const dir_RecordsFromCanSat="data/A_Records/00_Operation";
	static constexpr const char* const dir_RecordsFromDummyStrings="data/A_Records/98_DummyStrings";
	static constexpr const char* const dir_RecordsFromDummyRecords="data/A_Records/99_DummyRecords";
	static constexpr const char* const dir_RecordsFromDummyGroundRecords="data/A_Records/97_DummyGroundRecords";
	static constexpr const char* const dir_CanSatSimulation="data/A_Records/01_GeneratedSimulations";
	static constexpr const char* const dir_TestIMU_Calibration="data/A_Records/10_TestIMU_Calibration";

	// Image files
	static constexpr const char* const dir_ImagesFromCanSat="data/B_Images/00_Operation";

	// Operational calibration files (i.e. calibration files used when no specific calibration file is available).
	static constexpr const char* const dir_OperationalCalibration="data/C_Calibration";

	// Operational RT-Processing configuration settings
	static constexpr const char* const dir_Configuration="data/D_Configuration";


	// ----------------------------------------------------------------------------------------------------
	// Other file naming settings
	static constexpr const char* const DefaultIMU_CalibrationFile="data/C_Calibration/IMU/default.imu.csv";
	static constexpr const char* const DefaultCameraCalibrationFile="data/C_Calibration/camera/default.camera.csv";
	static constexpr const char* const DefaultConfigurationFile="data/D_Configuration/default.config.csv";
};



