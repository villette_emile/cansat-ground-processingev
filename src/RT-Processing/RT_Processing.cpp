/*
 * RT_Processing.cpp
 *
 * Design note: it would have been more optimal not to store the cxxopt::Options
 * object (not used after parsing) and to store the cxxopt::ParseResult one.
 * Unfortunately there is no constructor for ParseResult that allows for
 * creating an object before the parsing actually occurs.  As a consequence, the
 * parseResult cannot possibly be a data member, nor be allocated.
 *
 */

#include <exception>
#include <iostream>
#include <RT_Processing.h>
#include <assert.h>
#include <CO2_Averaging.h>
#include <sstream>
#include "DebugUtils.h"
#include "FileMgr.h"
#include "RTP_Config.h"
#include "TestDataEmitter.h"
#include "SerialPortListener.h"
#include "CSV_RecordSource.h"
#include "DummyRecordSource.h"
#include "DummyGroundRecordSource.h"
#include "RecordProcessor/RP_Echo.h"
#include "RecordProcessor/RP_CSV_SocketSend.h"
#include "RecordProcessor/RP_CSV_FileWriter.h"
#include "RecordProcessor/RP_StringSocketSend.h"
#include "RecordProcessor/RP_Transparent.h"
#include "RecordProcessor/RP_RecordDelay.h"
#include "RP_Example.h"
#include "RP_EchoGUI_Data.h"
#include "IMU_Calibrator.h"
#include "GPS_CoordinateConversion.h"
#include "IMU_DataFusion.h"
#include "RP_EchoIMU.h"
#include "ImageGeolocation.h"
#include "PrimaryMission.h"

using namespace std;

static constexpr bool DBG_SELECT_CFG_FILE = false;
static constexpr bool DBG_SRC_FILE = false;
static constexpr bool DBG_APP_STRUCTURE = false;
static constexpr bool DBG_CHAIN = false;
static constexpr bool DBG = false; // This symbol only controls the DBG_FCT tracing.

RT_Processing::RT_Processing() :
		options("RT_Processing",
				"The real-time ground processing for IsaTwo (CSPU)"), parseResult(
				nullptr), recordSource(nullptr), fileBase("fileBase not set"),srcType(SourceType_t::serial) {
}

RT_Processing::~RT_Processing() {
	if (parseResult)
		delete parseResult;

	if (recordSource) {
		recordSource->deleteProcessorChain();
		delete recordSource;
		recordSource = nullptr;
	}
}

int RT_Processing::run(int argc, char* argv[]) {
	DBG_FCT
	int resultCode = 0;

	try {
		// Configure command-line parser, and parse line
		processCommandLine(argc, argv);

		if (parseResult->count("help")) {
			printHelp();
		} else {
			LOG_IF(DBG_APP_STRUCTURE, DEBUG) << "Initializing RT_Processing..."
					<< endl;
			installDataFileStructure();
			LOG_IF(DBG_APP_STRUCTURE, DEBUG) << "Launching RT_Processing..."
					<< endl;
			performProcessing();
			cout << endl << "RT_Processing: end of job" << endl << flush;
		}
	} // try
	catch (cxxopts::OptionParseException& e) {
		cout << e.what() << endl;
		cout << options.help( { "Main", "Other" }) << endl;
		resultCode = -1;
	} catch (cxxopts::OptionSpecException& e) {
		cout << "Error in command-line options specification:" << endl;
		cout << e.what() << endl;
		resultCode = -2;
	} catch (exception &e) {
		cout << "Exception received in RT_Processing::run()." << endl;
		cout << e.what() << endl;
		cout << "Use option --help for help about the command-line options"
				<< endl;
		resultCode = -99;
	}

	return resultCode;
}

void RT_Processing::printHelp() const {
	cout << options.help( { "Main", "Other", "Debug & Test" }) << endl;
}

/**	@warning The following options are used by Easylogging++ and should
 *			 not be used to avoid collision: -v, --v, --verbose, --logging-flags
 *			 --default-log-file
 *			 (Referene <a href="https://github.com/muflihun/easyloggingpp#using-in-line-configurations">here</a>)
 */
void RT_Processing::processCommandLine(int argc, char* argv[]) {
	DBG_FCT
	/* Strange syntax... options.add_options returns a reference to an
	 * option_adder object, and we're calling it operator(), repetitively.
	 * Each option is described by
	 *  - a short and long name, separated with a comma
	 *  - a description (used in the help
	 *  - if the option is not boolean, a type for its value.
	 *  (Options can be grouped, but this is currently not used).
	 *  See https://github.com/jarro2783/cxxopts for details
	 */
	options.add_options("Main")("h,help", "Print help")("c,processing-chain",
			"Select the processing chain to run. "
					"CHAIN-ID must be a valid chain identifier among: "
					"full (the complete operation processing chain), "
					"save-as-ground (convert records into Ground Records and save to file), "
					"demoAB (just echo the received strings twice on standard output),"
					"IMU_Calibration (perform magnetometer, gyroscopes, and accelerometers calibration),"
					"GPS_CoordinateConversion (convert GPS data into operation cartesian referential),"
					"AHRS_Fusion (derive can attitude angles from IMU data),"
					"CO2Avg (performs a moving average on the CO2 readings to remove meaningless noise.),"
					"TestAHRS (test the IMU-Fusion-GUI processing chain),"
					" OTHER CHAINS TO DOCUMENT HERE",
			cxxopts::value<std::string>()->default_value("full"),
			"full | save-as-ground | IMU_Calibration | GPS_CoordinateConversion | AHRS_Fusion | EarthToLocal_CoordinateConversion | CO2Avg | TestAHRS | demoAB")(
			"s,source-type",
			"Select the type of source to use to obtain the records to "
					"process (records are expected to be IsaTwoRecords in CSV format). "
					"SOURCE-TYPE must be a valid source type among: "
					"serial (listen to USB serial port),"
					"file (read data from file specified with option --source-file), "
					"dummy-strings (generate dummy numbered strings (for test only), "
					"dummy-records (generate dummy IsaTwoRecords (for test only) or "
					"dummy-ground-records (generate dummy IsaTwoGroundRecords (for test only)",
			cxxopts::value<std::string>()->default_value("serial"),
			"serial | file | dummy-strings | dummy-records | dummy-ground-records")(
			"t,send-to-socket",
			"Send the result of the processing to a TCP socket "
					"(define port number with option tcp-port). ",
			cxxopts::value<bool>())

			;
	options.add_options("Other")("f,source-file",
			"Path to the input file containing the records to process, relative to "
					"the records data folder (data/A_Records)."
					"If the file does not have an extension '.can.csv' is assumed. If it has extension"
					"'.xxx', '.xxx.csv' is assumed. "
					"Records are expected to be IsaTwoRecords or IsaTwoGroundRecords in CSV format. "
					"This option is ignored if source-type is not 'file'. It source-type"
					"is not present, it is assumed to be 'file' if this option is present.",
			cxxopts::value<std::string>(), "PATH-TO-FILE")("b,baud-rate",
			"Define baud rate to use on incoming serial line. "
					"This option is ignored if source-type is not ‘serial’",
			cxxopts::value<unsigned int>()->default_value(
					RTP_Config::defaultSerialBaudRate), "BAUD-RATE")(
			"p,tcp-port",
			"Select the TCP-IP port on which a server socket must be open "
					"so a client can connect and obtain real-time results. "
					"If the port is busy, the next 10 are tried and the first "
					"free one is used. Ignored if send-to-socket is false ",
			cxxopts::value<unsigned short>()->default_value(
					RTP_Config::defaultTCP_Port), "PORT")(
			"IMU-calibration-file",
			"Path to the file containing the IMU calibration parameters to use, "
					"relative to folder data/C_Calibration/IMU. If extension is not '.csv', "
					"extension '.csv' is assumed. If no extension is provided, "
					"extension '.imu.csv' is assumed. If this option is not provided, and "
					"calibration data is required for the processing, a default file is used,  "
					"according to the following rules: if source of records is 'serial',  "
					"file data/C_Calibration/IMU/default.imu.csv is used (if any). Else, "
					"the file path is obtained by replacing 'A_Records' with 'C_Calibration/IMU' "
					"in the fileBase, and appending '.imu.csv' to it. If the resulting file does "
					"not exist, the default file is used. If no valid file can be found, an "
					"exception is thrown.", cxxopts::value<string>(),
			"PATH-TO-FILE")(
			"camera-calibration-file",
			"Path to the file containing the camera calibration parameters to use, "
					"relative to folder data/C_Calibration/camera. If extension is not '.csv', "
					"extension '.csv' is assumed. If no extension is provided, "
					"extension '.camera.csv' is assumed. If this option is not provided, and "
					"calibration data is required for the processing, a default file is used,  "
					"according to the following rules: if source of records is 'serial',  "
					"file data/C_Calibration/camera/default.camera.csv is used (if any). Else, "
					"the file path is obtained by replacing 'A_Records' with 'C_Calibration/camera' "
					"in the fileBase, and appending '.camera.csv' to it. If the resulting file does "
					"not exist, the default file is used. If no valid file can be found, an "
					"exception is thrown.", cxxopts::value<string>(),
			"PATH-TO-FILE")(
			"configuration-file",
			"Path to the file containing the RT-Processing configuration parameters to use, "
					"relative to folder data/D_Configuration. If extension is not '.csv', "
					"extension '.csv' is assumed. If no extension is provided, "
					"extension '.settings.csv' is assumed. If this option is not provided, and "
					"calibration data is required for the processing, a default file is used,  "
					"according to the following rules: if source of records is 'serial',  "
					"file data/D_Configuration/default.imu.csv is used (if any). Else, "
					"the file path is obtained by replacing 'A_Records' with 'D_Configuration' "
					"in the fileBase, and appending '.settings.csv' to it. If the resulting file does "
					"not exist, the default file is used. If no valid file can be found, an "
					"exception is thrown.", cxxopts::value<string>(),
			"PATH-TO-FILE")
	("v,visual-feedback",
			"Provide some feedback on cout: a dot ('.') for each record "
					"transferred, a '!' for each record ignored because no client is connected and "
					"processing is not suspended. ", cxxopts::value<bool>());
	options.add_options("Debug & Test")("d,print-chain-descriptor",
			"Display a description of the selected processing chain "
					"on cout before running it (debugging only).",
			cxxopts::value<bool>())("V,validate-results",
			"Check results against values in input record. (debugging only).",
			cxxopts::value<bool>())("demo-selector",
			"Select which demo must be run (debugging only) "
					"echo-strings = expect strings and echo them on on cout, "
					"echo-records = expect IsaTwoRecords and echo them on cout, "
					"echo-ground-records = expect IsaTwoGroundRecords and echo them on cout, "
					"test-example = compute roll angle as half the timestamp, with validation "
					"against the value found in the input record,",
			cxxopts::value<std::string>()->default_value("echo-records"),
			"echo-strings | echo-records | echo-ground-records | echo-gui-data | test-example ");

	parseResult = new cxxopts::ParseResult(options.parse(argc, argv));
}

void RT_Processing::performProcessing() {
	DBG_FCT
	assert(parseResult != nullptr);
	string sourceFile = "";

	configureRecordSource();		// 1. Create the record source.
	bool stringResult = configureProcessingChain();
	// 2. Create processing chain and attach
	//    to record source.
	appendResultSink(stringResult);
	// 3. Append a processor to output the
	//    resulting records
	// Display information
	if (parseResult->count("print-chain-descriptor")) {
		recordSource->printProcessorChainDescription(cout);
	}
	// ... and run !
	recordSource->run(RecordSource::allowUserInterrupt);
}

RT_Processing::SourceType_t RT_Processing::defineSourceType() {
	DBG_FCT
	SourceType_t result;
	string sourceType;
	// If absent, option source-type defaults to serial, unless option source-file
	// is present.
	if (((*parseResult).count("source-type") == 0)
			&& ((*parseResult).count("source-file") != 0)) {
		result = SourceType_t::file;
	} else {
		// Do not test for count of sourceType option, it has a default value.
		sourceType = (*parseResult)["source-type"].as<string>();
		LOG_IF(DBG_APP_STRUCTURE, DEBUG) << "source-type found: '"
				<< sourceType << "'";
		if (sourceType == "file") {
			result = SourceType_t::file;
		} else if (sourceType == "serial") {
			result = SourceType_t::serial;
			fileBase = RTP_Config::dir_RecordsFromCanSat;
			FileMgr::addFinalSlash(fileBase);
			fileBase += FileMgr::getTimestampedFileName();
		} else if (sourceType == "dummy-strings") {
			result = SourceType_t::dummyStrings;
			fileBase = RTP_Config::dir_RecordsFromDummyStrings;
			FileMgr::addFinalSlash(fileBase);
			fileBase += FileMgr::getTimestampedFileName();
		} else if (sourceType == "dummy-records") {
			result = SourceType_t::dummyRecords;
			fileBase = RTP_Config::dir_RecordsFromDummyRecords;
			FileMgr::addFinalSlash(fileBase);
			fileBase += FileMgr::getTimestampedFileName();
		} else if (sourceType == "dummy-ground-records") {
			result = SourceType_t::dummyGroundRecords;
			fileBase = RTP_Config::dir_RecordsFromDummyGroundRecords;
			FileMgr::addFinalSlash(fileBase);
			fileBase += FileMgr::getTimestampedFileName();
		} else {
			throw runtime_error(
					string(
							"Unexpected source-type option '" + sourceType
									+ "'"));
		}
	} //
	return result;
}

std::string RT_Processing::defineSourceFileAndFileBase() {
	DBG_FCT
	if (!parseResult->count("source-file")) {
		throw runtime_error(
				"Missing '--source-file' option when defineSourceFileAndFileBase() is called");
	}
	string sourceFileOption = (*parseResult)["source-file"].as<string>();
	LOG_IF(DBG_SRC_FILE,DEBUG) << "Received option source-file='"
			<< sourceFileOption << "'";

	// Add implicit extension if missing.
	if (!FileMgr::hasExtension(sourceFileOption, "csv")) {
		string extension = FileMgr::getExtension(sourceFileOption);
		if (extension.empty()) {
			sourceFileOption += ".can.csv";
		} else
			sourceFileOption += ".csv";
	}

	string sourceFilePath(RTP_Config::dir_Records);
	FileMgr::addFinalSlash(sourceFilePath);

	// Isolate the part of the file name excluding the extension(s)
	string dir, fileName;
	FileMgr::relativeDirBaseSplit(sourceFileOption, dir, fileName);
	// base still possible contain extension(s): only keep until the dot, if any
	string fileNameNoExt = fileName.substr(0, fileName.find_first_of('.'));
	if (!dir.empty())
		FileMgr::addFinalSlash(dir);
	fileBase = sourceFilePath + dir + fileNameNoExt;
	LOG_IF(DBG_SRC_FILE,DEBUG) << "FileBase set to '" << fileBase << "'";

	sourceFilePath += sourceFileOption;
	LOG_IF(DBG_SRC_FILE,DEBUG) << "Using source file '" << sourceFilePath
			<< "'";

	if (!FileMgr::isRegularFile(sourceFilePath)) {
		throw runtime_error(
				string("*** Error: cannot find file '") + sourceFilePath
						+ "'. Aborted");
	}
	return sourceFilePath;
}

void RT_Processing::configureRecordSource() {
	DBG_FCT
	srcType = defineSourceType();
	unsigned int baudRate;
	string sourceFile;

	switch (srcType) {
	case SourceType_t::dummyStrings:
		recordSource = new TestDataEmitter(5, 1000);
		cout << "Receiving records from dummy string emitter" << endl;
		break;
	case SourceType_t::serial:
		baudRate = (*parseResult)["baud-rate"].as<unsigned int>();
		recordSource = new SerialPortListener(baudRate);
		cout << "Receiving records from serial line" << endl;
		break;
	case SourceType_t::file:
		sourceFile = defineSourceFileAndFileBase();
		recordSource = new CSV_RecordSource(sourceFile);
		cout << "Reading records from file '" << sourceFile << "'" << endl;
		break;
	case SourceType_t::dummyRecords:
		recordSource = new DummyRecordSource(RecordSource::withVisualFeedback,
				100);
		cout << "Generating dummy IsaTwoRecords." << endl;
		break;
	case SourceType_t::dummyGroundRecords:
		recordSource = new DummyGroundRecordSource(
				RecordSource::withVisualFeedback, 80);
		cout << "Generating dummy IsaTwoGroundRecords every 80 msec." << endl;
		break;
	default:
		stringstream msg;
		msg << "Unsupported source '" << (int) srcType << "'." << endl;
		runtime_error e(msg.str());
		throw e;
	} // switch
	LOG_IF(DBG, DEBUG) << "fileBase is currently '" << fileBase << "'";
}

bool RT_Processing::configureProcessingChain() {
	DBG_FCT
	// Do not test for count of processing-chain option, it has a default value.
	bool doValidateResults = (*parseResult)["validate-results"].as<bool>();
	string chain = (*parseResult)["processing-chain"].as<string>();
	LOG_IF(DBG_CHAIN, DEBUG) << "processing-chain found: '" << chain << "'";
	LOG_IF(DBG_CHAIN, DEBUG) << "Configuring processing chain: ";
	LOG_IF(DBG_CHAIN, DEBUG) << "  fileBase: " << fileBase;
	LOG_IF(DBG_CHAIN, DEBUG) << "  chain   : " << chain;

	// From this point, you can assume that:
	//   * srcType is set and reliable.
	//   * fileBase is set and reliable (append '.xxx.csv' to it to obtain a valid output file path),
	//   * The IMU calibration data set can be obtained by calling getIMU_CalibrationDataSet();

	bool resultIsString = false;
	stringstream msg;

	if (chain == "demoAB") {
		string selector = (*parseResult)["demo-selector"].as<string>();
		if (selector == "echo-strings") {
			// Demo chain, processing strings only.
			Processor* echo1 = new RP_StringEcho(cout);
			Processor* echo2 = new RP_StringEcho(cout);
			echo1->appendProcessor(*echo2);
			recordSource->appendProcessor(*echo1);
			resultIsString = true;
			cout << "Echoing strings twice on cout..." << endl;
		} else if (selector == "echo-records") {
			Processor* transparent = new RP_Transparent<IsaTwoRecord,
					IsaTwoGroundRecord>;
			Processor* echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			transparent->appendProcessor(*echo);
			recordSource->appendProcessor(*transparent);
			resultIsString = false;
			cout << "Echoing IsaTwoRecords twice on cout..." << endl;
		} else if (selector == "echo-ground-records") {
			Processor* transparent = new RP_Transparent<IsaTwoGroundRecord,
					IsaTwoGroundRecord>;
			Processor* echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			transparent->appendProcessor(*echo);
			recordSource->appendProcessor(*transparent);
			resultIsString = false;
			cout << "Echoing IsaTwoGroundRecords twice on cout..." << endl;
		} else if (selector == "echo-gui-data") {
			Processor* delay = new RP_RecordDelay<IsaTwoGroundRecord>;
			Processor* echo = new RP_EchoGUI_Data(cout);
			delay->appendProcessor(*echo);
			recordSource->appendProcessor(*delay);
			resultIsString = false;
			cout << "Echoing GUI-data on cout..." << endl;
		} else if (selector == "test-example") {
			RP_Example* p = new RP_Example();
			p->activateResultsValidation(true);
			Processor* echo1 = new RP_EchoGUI_Data(cout);
			recordSource->appendProcessor(*echo1);
			recordSource->appendProcessor(*p);
			resultIsString = false;
			cout << "Running Example processor..." << endl;
		} else {
			msg.clear();
			msg << "DemoAB: Unsupported demo selector '" << selector << "'."
					<< endl;
			throw runtime_error(msg.str());
		}
	} else {
		// The first processor converts IsaTwoRecords received from the can
		// into IsaTwoGroundRecords, whatever the subsequent processing.
		// Next processors always start from IsaTwoGroundrecords and produce other
		// IsaTwoGroundRecords.
		Processor* convertToGndRecords = new RP_Transparent<IsaTwoRecord,
				IsaTwoGroundRecord>();
		if (chain == "full") {
			cout << "Running the full processing chain..." << endl;
			IMU_CalibrationDataSet IMU_Calib = getIMU_CalibrationDataSet();
			CameraCalibrationDataSet CameraCalib = getCameraCalibrationDataSet();
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			IMU_Calibrator* imuCalibrator = new IMU_Calibrator(IMU_Calib);
			imuCalibrator->activateResultsValidation(doValidateResults);
			IMU_DataFusion* imuDataFusion = new IMU_DataFusion(RTP_Config);
			imuDataFusion->activateResultsValidation(doValidateResults);
			PrimaryMission* primaryMission = new PrimaryMission(RTP_Config);
			primaryMission->activateResultsValidation(doValidateResults);
			CO2_Averaging* co2Averaging = new CO2_Averaging(RTP_Config);
			co2Averaging->activateResultsValidation(doValidateResults);
			GPS_CoordinateConversion* gpsCoordinateConvertor = new GPS_CoordinateConversion(RTP_Config);
			gpsCoordinateConvertor->activateResultsValidation(doValidateResults);
			ImageGeolocation* imageGeolocator = new ImageGeolocation(RTP_Config, CameraCalib);
			imageGeolocator->activateResultsValidation(doValidateResults);
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			GPS_CoordinateConversion* coordinateConversion = new GPS_CoordinateConversion(RTP_Config);
			string filePath = fileBase + ".can.csv";
			Processor *store = new RP_CSV_FileWriter<IsaTwoRecord>(filePath);
			string filePath2 = fileBase + ".rtp.csv";
			Processor *store2 = new RP_CSV_FileWriter<IsaTwoGroundRecord>(filePath2);
			Processor *trans = new RP_Transparent<IsaTwoRecord, IsaTwoRecord>;

			convertToGndRecords->appendProcessor(*imuCalibrator);
			convertToGndRecords->appendProcessor(*imuDataFusion);
			convertToGndRecords->appendProcessor(*primaryMission);
			convertToGndRecords->appendProcessor(*co2Averaging);
			convertToGndRecords->appendProcessor(*coordinateConversion);
			convertToGndRecords->appendProcessor(*imageGeolocator);
			convertToGndRecords->appendProcessor(*echo);
			convertToGndRecords->appendProcessor(*store2);

			recordSource->appendProcessor(*trans);
			recordSource->appendProcessor(*store);
			recordSource->appendProcessor(*convertToGndRecords);
			resultIsString = false;

			// COMPOSE THE PROCESSING CHAIN HERE, append processors to convertToGroundRecords.

		} else if (chain == "full-saved") {
			cout << "Running the full processing chain..." << endl;
			IMU_CalibrationDataSet IMU_Calib = getIMU_CalibrationDataSet();
			CameraCalibrationDataSet CameraCalib = getCameraCalibrationDataSet();
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			IMU_Calibrator* imuCalibrator = new IMU_Calibrator(IMU_Calib);
			imuCalibrator->activateResultsValidation(doValidateResults);
			IMU_DataFusion* imuDataFusion = new IMU_DataFusion(RTP_Config);
			imuDataFusion->activateResultsValidation(doValidateResults);
			PrimaryMission* primaryMission = new PrimaryMission(RTP_Config);
			primaryMission->activateResultsValidation(doValidateResults);
			CO2_Averaging* co2Averaging = new CO2_Averaging(RTP_Config);
			co2Averaging->activateResultsValidation(doValidateResults);
			GPS_CoordinateConversion* gpsCoordinateConvertor = new GPS_CoordinateConversion(RTP_Config);
			gpsCoordinateConvertor->activateResultsValidation(doValidateResults);
			ImageGeolocation* imageGeolocator = new ImageGeolocation(RTP_Config, CameraCalib);
			imageGeolocator->activateResultsValidation(doValidateResults);
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			GPS_CoordinateConversion* coordinateConversion = new GPS_CoordinateConversion(RTP_Config);
			string filePath = fileBase + "_repros.rtp.csv";
			Processor *store = new RP_CSV_FileWriter<IsaTwoGroundRecord>(filePath);
			Processor *trans = new RP_Transparent<IsaTwoRecord, IsaTwoRecord>;

			convertToGndRecords->appendProcessor(*imuCalibrator);
			convertToGndRecords->appendProcessor(*imuDataFusion);
			convertToGndRecords->appendProcessor(*primaryMission);
			convertToGndRecords->appendProcessor(*co2Averaging);
			convertToGndRecords->appendProcessor(*coordinateConversion);
			convertToGndRecords->appendProcessor(*imageGeolocator);
			convertToGndRecords->appendProcessor(*echo);
			convertToGndRecords->appendProcessor(*store);

			recordSource->appendProcessor(*trans);
			recordSource->appendProcessor(*convertToGndRecords);
			resultIsString = false;

		} else if (chain == "full-saved-echo") {
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			Processor *delay = new RP_RecordDelay<IsaTwoGroundRecord>;
			delay->appendProcessor(*echo);
			recordSource->appendProcessor(*delay);
			resultIsString = false;
			cout << "Echoing the saved ground records from the full chained chain..." << endl;
		} else if (chain == "save-as-ground") {
			Processor* p = new RP_CSV_FileWriter<IsaTwoGroundRecord>(
					fileBase + ".gnd.csv");
			convertToGndRecords->appendProcessor(*p);
			recordSource->appendProcessor(*convertToGndRecords);
			resultIsString = false;
			cout << "Saving incoming records as Ground Records..." << endl;
		} else if (chain == "IMU_Calibration") {
			IMU_CalibrationDataSet IMU_Calib = getIMU_CalibrationDataSet();
			IMU_Calibrator* p = new IMU_Calibrator(IMU_Calib);
			p->activateResultsValidation(doValidateResults);
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			p->appendProcessor(*echo);
			recordSource->appendProcessor(*p);
			resultIsString = false;
			cout << "Running the IMU calibration chain..." << endl;
		} else if (chain == "GPS_CoordinateConversion") {
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			GPS_CoordinateConversion* p = new GPS_CoordinateConversion(RTP_Config);
			p->activateResultsValidation(doValidateResults);
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			p->appendProcessor(*echo);
			recordSource->appendProcessor(*p);
			resultIsString = false;
			cout << "Running the GPS coordinate conversion chain..." << endl;
		} else if (chain == "AHRS_Fusion") {
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			IMU_DataFusion* p = new IMU_DataFusion(RTP_Config);
			p->activateResultsValidation(doValidateResults);
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			Processor *guiecho = new RP_EchoGUI_Data(cout);
			Processor *guiecho2 = new RP_EchoGUI_Data(cout);
			p->appendProcessor(*echo);
			recordSource->appendProcessor(*guiecho);
			recordSource->appendProcessor(*p);
			recordSource->appendProcessor(*guiecho2);

			resultIsString = false;
			cout << "Running the AHRS Fusion chain..." << endl;
		} else if (chain == "CO2Avg") {
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			CO2_Averaging* p = new CO2_Averaging(RTP_Config);
			p->activateResultsValidation(doValidateResults);
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			p->appendProcessor(*echo);
			recordSource->appendProcessor(*p);

			resultIsString = false;
			cout << "Running the secondary mission chain..." << endl;
		} else if (chain == "TestAHRS") {
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			IMU_DataFusion* fusion = new IMU_DataFusion(RTP_Config);
			IMU_CalibrationDataSet IMU_Calib = getIMU_CalibrationDataSet();
			IMU_Calibrator* calib = new IMU_Calibrator(IMU_Calib);
			Processor *trans = new RP_Transparent<IsaTwoRecord, IsaTwoRecord>;
			Processor *trans2 = new RP_Transparent<IsaTwoRecord, IsaTwoGroundRecord>;
			//Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			Processor *echo = new RP_EchoGUI_Data(cout);
			string filePath = fileBase + ".can.csv";
			Processor *store = new RP_CSV_FileWriter<IsaTwoRecord>(filePath);
			string filePath2 = fileBase + ".rtp.csv";
			Processor *store2 = new RP_CSV_FileWriter<IsaTwoGroundRecord>(filePath2);
			fusion->appendProcessor(*echo);
			recordSource->appendProcessor(*trans);
			recordSource->appendProcessor(*store);
			recordSource->appendProcessor(*trans2);
			recordSource->appendProcessor(*calib);
			recordSource->appendProcessor(*fusion);
			recordSource->appendProcessor(*store2);

			resultIsString = false;
			cout << "Running the test of IMU-FUSION-GUI chain..." << endl;
		} else if (chain == "TestAHRS-saved") {
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			Processor *delay = new RP_RecordDelay<IsaTwoGroundRecord>;
			delay->appendProcessor(*echo);
			recordSource->appendProcessor(*delay);
			resultIsString = false;
			cout << "Running the test of savedFiles-GUI chain..." << endl;
		}  else if (chain == "Echo-IMU-data") {
			Processor *trans = new RP_Transparent<IsaTwoRecord, IsaTwoGroundRecord>;
			Processor *echo = new RP_EchoIMU(cout);
			IMU_CalibrationDataSet IMU_Calib = getIMU_CalibrationDataSet();
			IMU_Calibrator* Calibrator = new IMU_Calibrator(IMU_Calib);
			trans->appendProcessor(*Calibrator);
			recordSource->appendProcessor(*trans);
			recordSource->appendProcessor(*echo);
			resultIsString = false;
			cout << "Echoing IMU-data on cout..." << endl;
		} else if (chain == "ImageGeolocation") {
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			CameraCalibrationDataSet CameraCalib = getCameraCalibrationDataSet();
			ImageGeolocation* p = new ImageGeolocation(RTP_Config, CameraCalib);
			p->activateResultsValidation(doValidateResults);
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			p->appendProcessor(*echo);
			recordSource->appendProcessor(*p);
			resultIsString = false;
			cout << "Running the image geolocation chain..." << endl;
		} else if (chain == "PrimaryMission") {
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			PrimaryMission* p = new PrimaryMission(RTP_Config);
			p->activateResultsValidation(doValidateResults);
			Processor *echo = new RP_RecordEcho<IsaTwoGroundRecord>(cout);
			p->appendProcessor(*echo);
			recordSource->appendProcessor(*p);
			resultIsString = false;
			cout << "Running the primary mission chain..." << endl;
		} else {
			msg.clear();
			msg << "Unsupported processing chain '" << chain << "'." << endl;
			throw runtime_error(msg.str());
		}
	}
	return resultIsString;
}

string RT_Processing::getConfigFileName(const string& cmdLineOption,
		const string& defaultConfigFileName, const string& settingsFileDir,
		const string& subExtension, const string& extension) {
	DBG_FCT
	string configFile;

	if ((*parseResult).count(cmdLineOption)) {
		configFile = settingsFileDir;
		FileMgr::addFinalSlash(configFile);
		configFile += (*parseResult)[cmdLineOption].as<string>();
		LOG_IF(DBG_SELECT_CFG_FILE, DEBUG) << "Checking extension on '" << configFile
				<< "'";
		if (!FileMgr::hasExtension(configFile, extension)) {
			string existingExtension = FileMgr::getExtension(configFile);
			if (existingExtension.empty()) {
				configFile += ".";
				configFile += subExtension;
				configFile += ".";
				configFile += extension;
			} else {
				configFile += ".";
				configFile += extension;
			}
		}
	} else {
		if (srcType == SourceType_t::serial) {
			configFile = defaultConfigFileName;
		} else {
			configFile = "define from fileBase";
			// 1. Look in calibration files dir.
			string dirToReplace(RTP_Config::dir_Records);
			FileMgr::addFinalSlash(dirToReplace);
			string replacementDir(settingsFileDir);
			FileMgr::addFinalSlash(replacementDir);
			size_t dirToReplaceIdx = fileBase.find(dirToReplace);
			if (dirToReplaceIdx == string::npos) {
				stringstream msg;
				msg << "Looking for configuration file: Cannot find dir '"
						<< dirToReplace << "' in fileBase '" << fileBase << "'";
				throw runtime_error(msg.str());
			}
			LOG_IF(DBG_SELECT_CFG_FILE, DEBUG) << "Replacing '" << dirToReplace
					<< " ' by '" << replacementDir << "' in fileBase '"
					<< fileBase << "'";
			configFile = fileBase;
			configFile.replace(dirToReplaceIdx, dirToReplace.length(),
					replacementDir);
			configFile += ".";
			configFile += subExtension;
			configFile += ".";
			configFile += extension;
			LOG_IF(DBG_SELECT_CFG_FILE, DEBUG) << "Looking for configuration file '"
					<< configFile << "'";
			if (!FileMgr::isRegularFile(configFile)) {
				// 2. Look near source file.
				configFile = fileBase + "." + subExtension + "." + extension;
				LOG_IF(DBG_SELECT_CFG_FILE, DEBUG)
						<< "Looking for configuration file '" << configFile
						<< "'";
				if (!FileMgr::isRegularFile(configFile)) {
					configFile = defaultConfigFileName;
				}
			}
		}
	}
	// Check existence
	if (!FileMgr::isRegularFile(configFile)) {
		stringstream msg;
		msg << "Cannot find configuration file '" << configFile << "'";
		throw runtime_error(msg.str());
	}
	return configFile;
}

IMU_CalibrationDataSet RT_Processing::getIMU_CalibrationDataSet() {
	DBG_FCT
	string calibFile = getConfigFileName("IMU-calibration-file",
			RTP_Config::DefaultIMU_CalibrationFile,
			RTP_Config::dir_IMU_Calibration,
			"imu", "csv");
	cout << "  IMU Calibration file used   : " << calibFile << endl;
	IMU_CalibrationDataSet dataSet(calibFile);
	return dataSet;  // NB: This returns a copy.
}

CameraCalibrationDataSet RT_Processing::getCameraCalibrationDataSet() {
	DBG_FCT
	string calibFile = getConfigFileName("camera-calibration-file",
			RTP_Config::DefaultCameraCalibrationFile,
			RTP_Config::dir_CameraCalibration, "settings", "csv");
	cout << "  Camera calibration file used: " << calibFile << endl;
	CameraCalibrationDataSet dataSet(calibFile);
	return dataSet;  // NB: This returns a copy.
}

RTP_ConfigDataSet RT_Processing::getConfigurationDataSet() {
	DBG_FCT
	string configFile = getConfigFileName("configuration-file",
			RTP_Config::DefaultConfigurationFile,
			RTP_Config::dir_Configuration,
			"config", "csv");
	cout << "  RTP Configuration file used : " << configFile << endl;
	RTP_ConfigDataSet dataSet(configFile);
	cout << dataSet;
	return dataSet;  // NB: This returns a copy.
}

void RT_Processing::appendResultSink(bool resultIsString) {
	DBG_FCT

	Processor* sink;
	if ((*parseResult).count("send-to-socket")) {
		// Collect options
		unsigned short portNumber =
				(*parseResult)["tcp-port"].as<unsigned short>();

		// We always wait for a client to be connected, unless we are working
		// in real-time, i.e. if data is comming from the serial port.
		SocketStringSender::ClientDisconnectOption_t disconnectOption =
				SocketStringSender::suspendOnClientDisconnection;
		if ((*parseResult)["source-type"].as<string>() == "serial") {
			disconnectOption =
					SocketStringSender::proceedDuringClientDisconnection;
		}

		Processor::VisualFeedback_t fb = Processor::noVisualFeedback;
		if ((*parseResult)["visual-feedback"].as<bool>()) {
			fb = Processor::withVisualFeedback;
		}

		if (resultIsString) {
			sink = new RP_StringSocketSend(portNumber, disconnectOption, fb);
		} else {
			RTP_ConfigDataSet RTP_Config = getConfigurationDataSet();
			sink = new RP_CSV_SocketSend<IsaTwoGroundRecord>(
						portNumber,
						RTP_Config.transmissionPeriod,
						disconnectOption, fb);
		}
		recordSource->appendProcessor(*sink);
		cout << "Sending results to TCP-IP port " << portNumber << "..."
				<< endl;
	}
}

void RT_Processing::installDataFileStructure() {
	DBG_FCT
	FileMgr::createDirectory(RTP_Config::dir_RecordsFromCanSat, false, true);
	FileMgr::createDirectory(RTP_Config::dir_RecordsFromDummyStrings, false,
			true);
	FileMgr::createDirectory(RTP_Config::dir_RecordsFromDummyRecords, false,
			true);
	FileMgr::createDirectory(RTP_Config::dir_RecordsFromDummyGroundRecords,
			false, true);
	FileMgr::createDirectory(RTP_Config::dir_CanSatSimulation, false, true);
	FileMgr::createDirectory(RTP_Config::dir_TestIMU_Calibration, false, true);
	FileMgr::createDirectory(RTP_Config::dir_ImagesFromCanSat, false, true);
	FileMgr::createDirectory(RTP_Config::dir_OperationalCalibration, false,
			true);
	FileMgr::createDirectory(RTP_Config::dir_IMU_Calibration, false, true);
	FileMgr::createDirectory(RTP_Config::dir_CameraCalibration, false, true);
	FileMgr::createDirectory(RTP_Config::dir_Configuration, false, true);
}
