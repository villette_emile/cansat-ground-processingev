/*
 * ImageGeolocation.cpp
 *
 *  Created on: 8 avr. 2019
 *      Author: Steven
 */

#include <ImageGeolocation.h>

#define DBG

#ifdef DBG
//#define DBG_PIX , DBG_VEC , DBG_CC , DBG_DPM , DBG_ANGLES ,DBG_DPM ,DBG_CAMERA_CALIB
#endif

#ifndef DBG
#define DBG_PIX
//#define DBG_VEC
//#define DBG_CC
//#define DBG_DPM
#define DBG_ANGLES
#define DBG_CAMERA_CALIB
#endif


ImageGeolocation::ImageGeolocation(const RTP_ConfigDataSet theRTP_Config, const CameraCalibrationDataSet theCameraCalib)
	: RTP_Config(theRTP_Config), CameraCalib(theCameraCalib){
	E = {};
	RotationMatrix = {};
	//latDPM = (double)1/111238.3925;
	//longDPM = 1/(111320*cos(RTP_Config.localReferentialOrigin_Latitude*M_PI/180));
	latDPM = (double)1/111238.3925;
	longDPM = (double)1/70972.32717;

#ifdef DBG_CAMERA_CALIB
	cout<<endl<<"Camera calibration file:"<<endl
			<<"numpixelX: "<<CameraCalib.numPixelsX<<" ."<<endl
			<<"numpixelY: "<<CameraCalib.numPixelsY<<" ."<<endl
			<<"pixelSizeX: "<<CameraCalib.pixelSizeX<<" ."<<endl
			<<"pixelSizeY: "<<CameraCalib.pixelSizeY<<" ."<<endl
			<<"f: "<<CameraCalib.f<<" ."<<endl;
#endif

#ifdef DBG_DPM
	cout<<endl<<"K lat: "<<latDPM<<" & k long: "<<longDPM<<endl;
#endif

	//First step of geolocation
	pixelTL = {(-(double)(CameraCalib.numPixelsX)/2) * CameraCalib.pixelSizeX, (-(double)(CameraCalib.numPixelsY)/2) * CameraCalib.pixelSizeY, CameraCalib.f};
	pixelTR = {((double)(CameraCalib.numPixelsX)/2) * CameraCalib.pixelSizeX, (-(double)(CameraCalib.numPixelsY)/2) * CameraCalib.pixelSizeY, CameraCalib.f};
	pixelBL = {(-(double)(CameraCalib.numPixelsX)/2) * CameraCalib.pixelSizeX, ((double)(CameraCalib.numPixelsY)/2) * CameraCalib.pixelSizeY, CameraCalib.f};
	pixelBR = {((double)(CameraCalib.numPixelsX)/2) * CameraCalib.pixelSizeX, ((double)(CameraCalib.numPixelsY)/2) * CameraCalib.pixelSizeY, CameraCalib.f};

#ifdef DBG_PIX
	cout<<endl<<"Pixels coordinates (before rotation): "<<endl;
	cout<<pixelTL[0]<<"	"<<pixelTL[1]<<"	"<<pixelTL[2]<<endl;
	cout<<pixelTR[0]<<"	"<<pixelTR[1]<<"	"<<pixelTR[2]<<endl;
	cout<<pixelBL[0]<<"	"<<pixelBL[1]<<"	"<<pixelBL[2]<<endl;
	cout<<pixelBR[0]<<"	"<<pixelBR[1]<<"	"<<pixelBR[2]<<endl;
#endif

	tempPixel = pixelTL[0];			//Rotation of 90�
	pixelTL[0] = -pixelTL[1];
	pixelTL[1] = tempPixel;
	tempPixel = pixelTR[0];
	pixelTR[0] = -pixelTR[1];
	pixelTR[1] = tempPixel;
	tempPixel = pixelBL[0];
	pixelBL[0] = -pixelBL[1];
	pixelBL[1] = tempPixel;
	tempPixel = pixelBR[0];
	pixelBR[0] = -pixelBR[1];
	pixelBR[1] = tempPixel;
	/*for (unsigned long i = 0; i < 3; i++){
		for (unsigned long y = 0; y <3; y++){
			pixel[i] += RotationMatrix[y][i] * tempPixel[y];
		}
	}*/

#ifdef DBG_PIX
	cout<<endl<<"Pixels coordinates (before): "<<endl;
	cout<<pixelTL[0]<<"	"<<pixelTL[1]<<"	"<<pixelTL[2]<<endl;
	cout<<pixelTR[0]<<"	"<<pixelTR[1]<<"	"<<pixelTR[2]<<endl;
	cout<<pixelBL[0]<<"	"<<pixelBL[1]<<"	"<<pixelBL[2]<<endl;
	cout<<pixelBR[0]<<"	"<<pixelBR[1]<<"	"<<pixelBR[2]<<endl;
#endif
}

ImageGeolocation::~ImageGeolocation() {}

bool ImageGeolocation::doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut){
	recordOut=recordIn;

	if(recordIn.GPS_Measures){
		array<double, 3> PTL = pixelTL;
		array<double, 3> PTR = pixelTR;
		array<double, 3> PBL = pixelBL;
		array<double, 3> PBR = pixelBR;

#ifdef DBG_PIX
		cout<<endl<<"Pixels coordinates (after): "<<endl;
		cout<<PTL[0]<<"	"<<PTL[1]<<"	"<<PTL[2]<<endl;
		cout<<PTR[0]<<"	"<<PTR[1]<<"	"<<PTR[2]<<endl;
		cout<<PBL[0]<<"	"<<PBL[1]<<"	"<<PBL[2]<<endl;
		cout<<PBR[0]<<"	"<<PBR[1]<<"	"<<PBR[2]<<endl;
#endif

	//Initialation of the E matrix
#ifdef DBG_ANGLES
		cout<<"Roll: "<<recordIn.groundAHRS_Roll<<" Yaw: "<<recordIn.groundAHRS_Yaw<<" Pitch: "<<recordIn.groundAHRS_Pitch<<endl;
#endif
		E[0][0] = cos(recordIn.groundAHRS_Pitch) * cos(recordIn.groundAHRS_Yaw);
		E[0][1] = cos(recordIn.groundAHRS_Pitch) * sin(recordIn.groundAHRS_Yaw);
		E[0][2] = -sin(recordIn.groundAHRS_Pitch);
		E[1][0] = sin(recordIn.groundAHRS_Roll) * sin(recordIn.groundAHRS_Pitch) * cos(recordIn.groundAHRS_Yaw) - cos(recordIn.groundAHRS_Roll) * sin(recordIn.groundAHRS_Yaw);
		E[1][1] = sin(recordIn.groundAHRS_Roll) * sin(recordIn.groundAHRS_Pitch) * sin(recordIn.groundAHRS_Yaw) + cos(recordIn.groundAHRS_Roll) * cos(recordIn.groundAHRS_Yaw);
		E[1][2] = cos(recordIn.groundAHRS_Pitch) * sin(recordIn.groundAHRS_Roll);
		E[2][0] = cos(recordIn.groundAHRS_Roll) * sin(recordIn.groundAHRS_Pitch) * cos(recordIn.groundAHRS_Yaw) + sin(recordIn.groundAHRS_Roll) * sin(recordIn.groundAHRS_Yaw);
		E[2][1] = cos(recordIn.groundAHRS_Roll) * sin(recordIn.groundAHRS_Pitch) * sin(recordIn.groundAHRS_Yaw) - sin(recordIn.groundAHRS_Roll) * cos(recordIn.groundAHRS_Yaw);
		E[2][2] = cos(recordIn.groundAHRS_Pitch) * cos(recordIn.groundAHRS_Roll);

		//Geolocation
		pixelGeolocation(PTL, recordIn);
		pixelGeolocation(PTR, recordIn);
		pixelGeolocation(PBL, recordIn);
		pixelGeolocation(PBR, recordIn);

		//Storage
		bool ok = true;
		for(unsigned long i = 0; i < 3; i++){
			ok =ok && storeResult(PTL[0], recordOut.imgCornerLat[0] ,"imLat1");
			ok =ok && storeResult(PTR[0], recordOut.imgCornerLat[1] ,"imLat2");
			ok =ok && storeResult(PBL[0], recordOut.imgCornerLat[2] ,"imLat3");
			ok =ok && storeResult(PBR[0], recordOut.imgCornerLat[3] ,"imLat4");
			ok =ok && storeResult(PTL[1], recordOut.imgCornerLong[0] ,"imLong1");
			ok =ok && storeResult(PTR[1], recordOut.imgCornerLong[1] ,"imLong2");
			ok =ok && storeResult(PBL[1], recordOut.imgCornerLong[2] ,"imLong3");
			ok =ok && storeResult(PBR[1], recordOut.imgCornerLong[3] ,"imLong4");
		}
		return ok;
	}
	else{
		array<double, 2> PTL = {9999,9999};
		array<double, 2> PTR = {9999,9999};
		array<double, 2> PBL = {9999,9999};
		array<double, 2> PBR = {9999,9999};
		//Storage
		bool ok = true;
		for(unsigned long i = 0; i < 3; i++){
			ok =ok && storeResult(PTL[0], recordOut.imgCornerLat[0] ,"imLat1");
			ok =ok && storeResult(PTR[0], recordOut.imgCornerLat[1] ,"imLat2");
			ok =ok && storeResult(PBL[0], recordOut.imgCornerLat[2] ,"imLat3");
			ok =ok && storeResult(PBR[0], recordOut.imgCornerLat[3] ,"imLat4");
			ok =ok && storeResult(PTL[1], recordOut.imgCornerLong[0] ,"imLong1");
			ok =ok && storeResult(PTR[1], recordOut.imgCornerLong[1] ,"imLong2");
			ok =ok && storeResult(PBL[1], recordOut.imgCornerLong[2] ,"imLong3");
			ok =ok && storeResult(PBR[1], recordOut.imgCornerLong[3] ,"imLong4");
		}
		return ok;
	}
	
}

void ImageGeolocation::pixelGeolocation (array<double, 3> &pixel, const IsaTwoGroundRecord& recordIn){
	array<double, 3> tempPixels = {0, 0, 0};
	//NED referential
	tempPixels = pixel;
	pixel = {0, 0, 0};
	for (unsigned long i = 0; i < 3; i++){
		for (unsigned long y = 0; y <3; y++){
			pixel[i] += E[y][i] * tempPixels[y];
		}
	}

#ifdef DBG_VEC
	cout<<"vectors: "<<endl;
	cout<<pixel[0]<<"	"<<pixel[1]<<"	"<<pixel[2]<<endl;
#endif

	//Carthesian coordinates
	pixel[0] = recordIn.positionLocal[0] - pixel[0] * recordIn.positionLocal[2] / pixel[2];
	pixel[1] = recordIn.positionLocal[1] - pixel[1] * recordIn.positionLocal[2] / pixel[2];
	pixel[2] = 0;

#ifdef DBG_CC
	cout<<"Carthesian coordinates: "<<endl;
	cout<<pixel[0]<<"	"<<pixel[1]<<"	"<<pixel[2]<<endl;
#endif

	//Geographical coordinates
	pixel[0] = RTP_Config.localReferentialOrigin_Latitude + pixel[0] * latDPM;
	pixel[1] = RTP_Config.localReferentialOrigin_Longitude + pixel[1] * longDPM;
}
