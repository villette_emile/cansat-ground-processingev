/*
 * PrimaryMission.h
 *
 *  Created on: 23 mars 2019
 *      Author: Steven
 */

#pragma once

#include <RTP_ConfigDataSet.h>
#include <IsaTwoGroundRecord.h>
#include "RecordProcessor/RecordProcessor.h"

/** @ingroup RecordProcessing
 *  @brief A processor which calibrates the primary mission data (temperature and pressure).
 */
class PrimaryMission: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord> {
public:
	PrimaryMission(const RTP_ConfigDataSet theRTP_Config);
	virtual ~PrimaryMission();
	/** Perform a calibration and store it using the base class'
	 *  storeResult() method.
	 * @param recordIn  The record to process.
	 * @param recordOut The record resulting from the processing.
	 * @return True if the process success.
	 */
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut);
private:
	RTP_ConfigDataSet RTP_Config;
};
