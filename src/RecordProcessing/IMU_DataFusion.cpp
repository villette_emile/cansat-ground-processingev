/*
 * IMUDataFusion.cpp
 *
 *  Created on: 28 janv. 2019
 *      Author: Steven
 */

#include <IMU_DataFusion.h>

constexpr int PeriodToleranceInPercent = 1;  // Tolerance ratio for update period.

IMU_DataFusion::IMU_DataFusion (const RTP_ConfigDataSet& theRTP_Config)
{
	periodInMsec= (unsigned int) (1000.0 / theRTP_Config.IMU_SampleFrequency);
	lastTimestamp=0;
	fusion.begin((float)theRTP_Config.IMU_SampleFrequency);
	cout << "Madgwick Quaternion Update algorithm initialized at " << theRTP_Config.IMU_SampleFrequency << " Hz" << endl;
}

IMU_DataFusion::~IMU_DataFusion() {}

bool IMU_DataFusion::doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut) {
	recordOut=recordIn;

	// Check the timing of the records is consistent with the announced frequency
	if (lastTimestamp != 0) {
		int deltaMsec=  ((int) (recordIn.timestamp - lastTimestamp)) - (int) periodInMsec;
		int deltaPercent=(int) (100.0*( (float) deltaMsec/periodInMsec));
		if ((deltaPercent < -PeriodToleranceInPercent) || (deltaPercent > PeriodToleranceInPercent)){
			cout << "IMU Data Fusion: *** WARNING: NON-COMPLIANT TIMING ***" << endl;
			cout << "   Period = " << periodInMsec << " msec" << endl;
			cout << "   Successive timestamps: " << lastTimestamp << " then " << recordIn.timestamp  << " (diff=" << recordIn.timestamp - lastTimestamp << " msec)" << endl;
			cout << "   Discrepency = " << deltaMsec << " msec = " << deltaPercent << "% of period." << endl;
		}
	}
	lastTimestamp=recordIn.timestamp;

	fusion.update((float)recordIn.calibratedGyro[0], (float)recordIn.calibratedGyro[1], (float)recordIn.calibratedGyro[2],
			(float)recordIn.calibratedAccel[0], (float)recordIn.calibratedAccel[1], (float)recordIn.calibratedAccel[2],
			(float)recordIn.calibratedMag[0], (float)recordIn.calibratedMag[1], (float)recordIn.calibratedMag[2]);

	double resultRoll = (double)fusion.getRollRadians();
	double resultYaw = (double)fusion.getYawRadians();
	double resultPitch = (double)fusion.getPitchRadians();

	//Compliant result
#ifdef CHECK_PROCESSOR_OUTPUT_RANGE
	if(abs(resultRoll) > 2*M_PI){
		throw runtime_error("ERROR : AMPLITUDE ROLL TOO BIG !!!");
	}
	if(abs(resultYaw) > 2*M_PI){
		throw runtime_error("ERROR : AMPLITUDE YAW TOO BIG !!!");
	}
	if(abs(resultRoll) > 2*M_PI){
		throw runtime_error("ERROR : AMPLITUDE PITCH TOO BIG !!!");
	}
#endif
	if(isnan(resultRoll)){
		resultRoll = 9999;
	}
	if(isnan(resultYaw)){
		resultYaw = 9999;
	}
	if(isnan(resultPitch)){
		resultPitch = 9999;
	}

	//Storage
	bool ok = true;
	for(unsigned long i = 0; i < 3; i++){
		ok =ok && storeResult(resultRoll, recordOut.groundAHRS_Roll,"roll");
		ok =ok && storeResult(resultYaw, recordOut.groundAHRS_Yaw,"yaw");
		ok =ok && storeResult(resultPitch, recordOut.groundAHRS_Pitch,"pitch");
	}
	return ok;
}

