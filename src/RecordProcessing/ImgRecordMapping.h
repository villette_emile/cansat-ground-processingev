/*
 * ImgRecordMapping.h
 *
 */

#pragma once
#include <IsaTwoGroundRecord.h>
#include "RecordProcessor/RecordProcessor.h"
#
class ImgRecordMapping: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>{
public:
	/**
	 * @param theImgFolderIn The folder containing the image files. Must exist and may not
	 *  				  be empty (otherwise, a runtime_error is raised.
	 * @param theImgFolderOut The folder in which to copy the renamed files. May not exist
	 * 					and will be created (runtime_error if it exists).
	 * @param theImagerActivationTimestamp The value of the master clock at which the
	 * 		  imager was activated.
	 */
	ImgRecordMapping(const std::string& theImgFolderIn,
					 const std::string& theImgFolderOut,
					 const unsigned long theImagerActivationTimestamp);
	virtual ~ImgRecordMapping();
	/** Maps image files to the appropriate IsaTwoGroundRecord to use for its
	 *  processing.
	 * @param recordIn  The record to process.
	 * @param recordOut The record resulting from the processing (always unchanged).
	 * @return True if the processing succeeds.
	 */
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut);

private:
	static constexpr auto DBG=true;
	static constexpr auto DBG_PROCESS=false;
	static constexpr auto DBG_CONSTRUCTOR=true;
	std::string	imgFolderIn;	/**< path to the directory containing the image files provided by the can. */
	std::string imgFolderOut;	/**< path to the destination directory for the mapped image files. */
	std::vector<unsigned long> originalFileTimestamps; /**< The timestamps associated by the can with the image files. */
	unsigned long imagerActivationTimestamp; /**< Value of the master clock at which the imager was activated. */
	std::vector<unsigned long>::iterator nextFileToMap; /**< Iterator pointing to the next file to be mapped. */
	unsigned long previousTimestamp; /**< The timestamp of the previous record processed (0 = none). */
	unsigned long nextTimestampToMap; /**< The timestamp of the next image to map (converted in Master reference */
};

