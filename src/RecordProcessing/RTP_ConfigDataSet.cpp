/*
 * RTP_ConfigDataSet.cpp
 */

#include <RTP_ConfigDataSet.h>
#include <iostream>
#include "FileMgr.h"
#include "DebugUtils.h"
#include "CSV_Row.h"

static constexpr bool DBG=false;

using namespace std;

RTP_ConfigDataSet::RTP_ConfigDataSet()
:localReferentialOrigin_Latitude(0),
 localReferentialOrigin_Longitude(0),
 localReferentialOrigin_Altitude(0),
 temperatureOffset(0),
 seaLevelPressure(0),
 PressureOffest(0),
 IMU_SampleFrequency(0),
 CO2_NumSampleToAverage(0),
 transmissionPeriod(0)
{}

RTP_ConfigDataSet::RTP_ConfigDataSet(const std::string& dataFile) {
	if (!FileMgr::isRegularFile(dataFile)) {
		throw runtime_error(string("Inexistant Camera calibration file '")+dataFile+"'");
	}
	DBG_FCT
	try {
		ifstream in;
		ostringstream msg;
		in.open(dataFile);
		if (!in.good()) {
			msg.clear();
			msg << "Cannot open file '"<<dataFile <<"'.";
			throw runtime_error(msg.str());
		}
		LOG_IF(DBG, DEBUG) << "Reading RTP configuration file '" << dataFile <<"'....";
		CSV_Row row(in);
		row.next(true);

		size_t idx=0;
		localReferentialOrigin_Latitude=row.getDouble(idx++);
		localReferentialOrigin_Longitude=row.getDouble(idx++);
		localReferentialOrigin_Altitude=row.getDouble(idx++);
		temperatureOffset=row.getDouble(idx++);
		seaLevelPressure=row.getDouble(idx++);
		PressureOffest=row.getDouble(idx++);
		IMU_SampleFrequency=row.getDouble(idx++);
		CO2_NumSampleToAverage=row.getUInt(idx++);
		transmissionPeriod=row.getULong(idx++);

		LOG_IF(DBG, DEBUG) << "RTP Configuration file processed. ";
	}
	catch(exception &e) {
		cout << "Exception while parsing RTP configuration file: '" << dataFile << "'" << endl;
		cout << e.what() << endl;
		throw;
	}
}

std::ostream& operator<< (std::ostream& stream, const RTP_ConfigDataSet& set)
{
	stream << "RT-processing configuration parameters:" << endl;
	stream << "   Local refential origin: Lat=" << set.localReferentialOrigin_Latitude << "°, Long="
		   << set.localReferentialOrigin_Longitude << "°, alt."
		   << set.localReferentialOrigin_Altitude << "m" << endl;
	stream << "   Temperature offset    : " << set.temperatureOffset << "°" << endl;
	stream << "   Pressure offset       : " << set.PressureOffest << " hPa" << endl;
	stream << "   Sea-level pressure    : " << set.seaLevelPressure << " hPa" << endl;
	stream << "   IMU sample frequency  : " << set.IMU_SampleFrequency << " Hz" << endl;
	stream << "   CO2 samples to average: " << set.CO2_NumSampleToAverage << endl;
	stream << "	  Transmission period	: " << set.transmissionPeriod << " ms" << endl;
	return stream;
}
