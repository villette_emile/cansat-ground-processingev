/*
 * IMUDataFusion.h
 *
 *  Created on: 28 janv. 2019
 *      Author: Steven
 */

#ifndef RECORDPROCESSING_IMU_DATAFUSION_H_
#define RECORDPROCESSING_IMU_DATAFUSION_H_

#include <IsaTwoGroundRecord.h>
#include "RecordProcessor/RecordProcessor.h"
#include <MadgwickAHRS.h>
#include "RTP_ConfigDataSet.h"

/** @ingroup RecordProcessing
 *  @brief A processor which performs a fusion of the IMU data to obtain the orientation
 *  of the can (Euler angles: roll-yaw-pitch) using the Madgwick library.
 */
class IMU_DataFusion: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>{
public:
	IMU_DataFusion(const RTP_ConfigDataSet& theRTP_Config);
	virtual ~IMU_DataFusion();
	/** Perform a fusion of the IMU data and store it using the base class'
			 *  storeResult() method.
			 * @param recordIn  The record to process.
			 * @param recordOut The record resulting from the processing.
			 * @return True if the process success.
			 */
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut);
private:
	Madgwick fusion;
	unsigned int periodInMsec;
	unsigned long lastTimestamp;
};

#endif /* RECORDPROCESSING_IMU_DATAFUSION_H_ */
