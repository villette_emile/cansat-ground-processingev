/*
 * GPS_CoordinateConversion.h
 *
 *  Created on: 18 janv. 2019
 *      Authors: Steven and Lorenz
 */

#ifndef RECORDPROCESSING_GPS_COORDINATECONVERSION_H_
#define RECORDPROCESSING_GPS_COORDINATECONVERSION_H_

#include <IsaTwoGroundRecord.h>
#include "RecordProcessor/RecordProcessor.h"
#include <RTP_ConfigDataSet.h>

/** @ingroup RecordProcessing
 *  @brief A processor which converses the GPS data as longitude-latitude-altitude
 *  to cartesian coordinate in the Operating site referential.
 */
class GPS_CoordinateConversion: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>{
public:
	GPS_CoordinateConversion(const RTP_ConfigDataSet theRTP_Config);
	virtual ~GPS_CoordinateConversion();
	/** Perform a coordinate conversion and store it using the base class'
			 *  storeResult() method.
			 * @param recordIn  The record to process.
			 * @param recordOut The record resulting from the processing.
			 * @return True if the process success.
			 */
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut);

	void ECEFConvertion(array<double, 3> &theECEFCoordinates);

private:
	RTP_ConfigDataSet RTP_Config;
	double LCCMatrix[3][3];
	array<double, 3> localReferentialOrigine_Coordinate;	/**< The coordinates of the operating location*/
	array<double, 3> ECEFCoordinates;	/**< The coordinates converted to ECEF*/
	array<double, 3> NEDCoordinates;	/**< The coordinates converted to NED*/
};

#endif /* RECORDPROCESSING_GPS_COORDINATECONVERSION_H_ */
