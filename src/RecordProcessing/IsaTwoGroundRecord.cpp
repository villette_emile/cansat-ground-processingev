/*
 * IsaTwoGroundRecord.cpp
 *
 */

#include <IsaTwoGroundRecord.h>
#include "DebugUtils.h"
#include "IsaTwoInterface.h"
constexpr auto DBG=false;
constexpr auto DBG_PARSING=false;

IsaTwoGroundRecord::IsaTwoGroundRecord(): IsaTwoRecord(39, (unsigned int) IsaTwoRecordType::GroundDataRecord) {
}

IsaTwoGroundRecord& IsaTwoGroundRecord::operator=(const IsaTwoRecord& other){
	DBG_FCT
	if (this == &other) return *this;
	IsaTwoRecord::operator=(other);
	recordType=(uint16_t) IsaTwoRecordType::GroundDataRecord;
	// Leave data not common with IsaTwoRecord unchanged.
	return *this;
}

IsaTwoGroundRecord::~IsaTwoGroundRecord() {
}

void IsaTwoGroundRecord::clear() {
	DBG_FCT
	IsaTwoRecord::clear();
	std::fill(calibratedAccel.begin(), calibratedAccel.end(), 0.0);
	std::fill(calibratedGyro.begin(), calibratedGyro.end(), 0.0);
	std::fill(calibratedMag.begin(), calibratedMag.end(), 0.0);

	groundAHRS_Roll=groundAHRS_Yaw=groundAHRS_Pitch=0.0f;
	std::fill(groundAHRS_Accel.begin(), groundAHRS_Accel.end(), 0.0);

	std::fill(positionEarthCentric.begin(), positionEarthCentric.end(), 0.0);
	std::fill(positionLocal.begin(), positionLocal.end(), 0.0);
	std::fill(KF_Position.begin(), KF_Position.end(), 0.0);
	std::fill(KF_Velocity.begin(), KF_Velocity.end(), 0.0);

	temperatureCorrected=altitude_Corrected=CO2_VoltageCleaned=CO2_Concentration=0.0;
	std::fill(imgCornerLat.begin(), imgCornerLat.end(), 0.0);
	std::fill(imgCornerLong.begin(), imgCornerLong.end(), 0.0);
}

void IsaTwoGroundRecord::printCSV_Header(ostream &os) const {
  IsaTwoRecord::printCSV_Header(os);
  os << ", "
     << "calibratedAccelX, calibratedAccelY, calibratedAccelZ,"
	 << "calibratedGyroX, calibratedGyroY, calibratedGyroZ,"
	 << "calibratedMagX, calibratedMagY, calibratedMagZ,"
	 << "groundAHRS_AccelX, groundAHRS_AccelY, groundAHRS_AccelZ,"
	 << "groundAHRS_Roll, groundAHRS_Yaw, groundAHRS_Picth,"
	 << "posEarthCentricX, posEarthCentricY, posEarthCentricZ, posLocalX, posLocalY, posLocalZ,"
  	 << "KF_PosX, KF_PosY, KF_PosZ, KF_VelocityX, KF_VelocityY, KF_VelocityZ,"
	 << "tempCorrected, altGPS_Corrected, CO2_VoltClean, CO2_ppm,"
  	 << "imgTL_Lat, imgTR_Lat, imgBL_Lat, imgBR_Lat, imgTL_long, imgTR_Long, imgBL_Long, imgBR_Long";
}

void IsaTwoGroundRecord::printCSV(ostream &os) const {
	IsaTwoRecord::printCSV(os);
	os << CSV_Record::separator;
	printCSV(os, calibratedAccel, false, true);
	printCSV(os, calibratedGyro, false, true);
	printCSV(os, calibratedMag, false, true);
	printCSV(os, groundAHRS_Accel, false, true);
	printCSV(os, groundAHRS_Roll, true);
	printCSV(os, groundAHRS_Yaw, true);
	printCSV(os, groundAHRS_Pitch, true);

	printCSV(os, positionEarthCentric, false, true);
	printCSV(os, positionLocal, false, true);
	printCSV(os, KF_Position, false, true);
	printCSV(os, KF_Velocity, false, true);

	printCSV(os, temperatureCorrected, true);
	printCSV(os, altitude_Corrected, true);
	printCSV(os, CO2_VoltageCleaned, true);
	printCSV(os, CO2_Concentration, true);

	printCSV(os, imgCornerLat, false, true);
	printCSV(os, imgCornerLong, false, false); // No separator after last value.
}

unsigned int IsaTwoGroundRecord::parseRow(const CSV_Row &row) {
	DBG_FCT
	unsigned int idx = IsaTwoRecord::parseRow(row);
	try {
		readFromRow(row, idx, calibratedAccel);
		LOG_IF(DBG_PARSING,DEBUG) << "idx=" << idx << ",calibratedAccelX=" << calibratedAccel[0];
		LOG_IF(DBG_PARSING,DEBUG) << "idx=" << idx << ",calibratedAccelY=" << calibratedAccel[1];
		LOG_IF(DBG_PARSING,DEBUG) << "idx=" << idx << ",calibratedAccelZ=" << calibratedAccel[2];
		readFromRow(row, idx, calibratedGyro);
		LOG_IF(DBG_PARSING,DEBUG) << "idx=" << idx << ",calibratedGyroX=" << calibratedGyro[0];
		readFromRow(row, idx, calibratedMag);
		LOG_IF(DBG_PARSING,DEBUG) << "idx=" << idx << ",calibratedMagX=" << calibratedMag[0];
		readFromRow(row, idx, groundAHRS_Accel);
		readFromRow(row, idx, groundAHRS_Roll);
		LOG_IF(DBG_PARSING,DEBUG) << "idx=" << idx << ",roll=" << groundAHRS_Roll;
		readFromRow(row, idx, groundAHRS_Yaw);
		LOG_IF(DBG_PARSING,DEBUG) << "idx=" << idx << ",yaw=" << groundAHRS_Yaw;
		readFromRow(row, idx, groundAHRS_Pitch);
		LOG_IF(DBG_PARSING,DEBUG) << "idx=" << idx << ",pitch=" << groundAHRS_Pitch;
		readFromRow(row, idx, positionEarthCentric);
		readFromRow(row, idx, positionLocal);
		readFromRow(row, idx, KF_Position);
		readFromRow(row, idx, KF_Velocity);
		readFromRow(row, idx, temperatureCorrected);
		readFromRow(row, idx, altitude_Corrected);
		readFromRow(row, idx, CO2_VoltageCleaned);
		readFromRow(row, idx, CO2_Concentration);
		readFromRow(row, idx, imgCornerLat);
		readFromRow(row, idx, imgCornerLong);
		// No need to skip rest of line. This is taken care of by CSV_Row
		return idx;
	}
	catch(const exception &e) {
		cout << "Exception parsing IsaTwoRecord file, after reading value '" << row[idx] << "' at index "<<idx << endl;
		// Do not print exception description: it will be done
		// by the caller.
		throw;
	}
}
