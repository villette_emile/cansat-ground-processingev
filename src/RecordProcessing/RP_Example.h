/*
 * RP_Example.h
 */

#pragma once
#include <IsaTwoGroundRecord.h>
#include "RecordProcessor/RecordProcessor.h"

/**  @ingroup RecordProcessing
 * @brief A record processor which performs a meaningless computation
 * (set groundAHRS_Roll to half the timestamp) to demonstrate the use of
 * the validation features. */
class RP_Example : public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord> {
public:
	RP_Example()
		: RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>(Processor::noVisualFeedback) {};

	virtual ~RP_Example() {};

	/** Perform a dummy calculation and store it using the base class'
	 *  storeresult() method.
	 * @param recordIn  The record to process.
	 * @param recordOut The record resulting from the processing.
	 * @return Always true (cannot possibly fail).
	 */
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut) {
		recordOut=recordIn;
		double result= recordIn.timestamp/2.0;
		bool ok=storeResult(result, recordOut.groundAHRS_Roll,"roll");
		return ok;
	}
};


