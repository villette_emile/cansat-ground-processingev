/*
 * IsaTwoGroundRecord.h
 *
 */

#pragma once

#include <IsaTwoRecord.h>

/** @ingroup RecordProcessing
 *  @brief The record containing the data received from the can augmented with all data computed by
 *  the RT-Processing.
 */
class IsaTwoGroundRecord: public IsaTwoRecord {
public:
	IsaTwoGroundRecord();
	/** Operator= overloading to be able to initialize the IsaTwoRecord part of the object
	 * @param other The IsaTwoRecord to copy data from. */
	IsaTwoGroundRecord& operator=(const IsaTwoRecord& other);
	IsaTwoGroundRecord(const IsaTwoGroundRecord&)=delete;
	virtual ~IsaTwoGroundRecord();

	// IMU calibrated data
	array<double, 3> calibratedAccel{};/**< Accelerometer readings in m/s^2 */
	array<double, 3> calibratedGyro{}; /**< Gyroscope readings in rad/s */
	array<double, 3> calibratedMag{};  /**< Calibrated magnetometer readings, in µT */

	// AHRS outputs
	array<double,3>  groundAHRS_Accel{};	/**< Acceleration as computed by the AHRS */
	double groundAHRS_Roll{}, groundAHRS_Yaw{}, groundAHRS_Pitch{};   /**< Euler angles as computed by the AHRS */

	// Public data members carried by the record, in addition to base class data
	array<double, 3>  positionEarthCentric{}; /**<  in m,   in earth centric cartesian referential */
	array<double, 3>  positionLocal{};  /**<  in m, in local cartesian referential */

	// Output of Kalman filter
	array<double, 3> KF_Position{};   /**< Can position, in m, as computed by the Kalman filter   */
	array<double, 3> KF_Velocity{};   /**< Can velocity, in m/s, as computed by the Kalman filter */

	// Primary mission processed data
	double temperatureCorrected{};    /**< The temperature in degrees. Data from the thermistor,
										   offset corrected. */
	double altitude_Corrected{};	  /**< The altitude in meters, relative to the ground i.e. data
	 	 	 	 	 	 	 	 	 	   from the BMP, corrected to set altitude 0 to launch site). */


	// Secondary mission processed data
	double CO2_VoltageCleaned{};	  /**< The voltage read from the CO2 sensor, after averaging. */
	double CO2_Concentration{};		  /**< Concentration of CO2 in ppm. */
	array<double,4> imgCornerLat{};   /**< The longitude of the 4 image corners in the local referential
										   top-left, top-right, bottom-left, bottom-right */
	array<double,4> imgCornerLong{};  /**< The latitude of the 4 image corners in the local referential
										   top-left, top-right, bottom-left, bottom-right */


	virtual void clear();
	virtual void printCSV_Header(ostream &os) const;
	virtual void printCSV(ostream &os) const;
protected:
	using CSV_Record::printCSV;
	using IsaTwoRecord::printCSV;
	virtual unsigned int parseRow(const CSV_Row &row);

};

