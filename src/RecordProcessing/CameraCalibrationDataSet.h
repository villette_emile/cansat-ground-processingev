/*
 * CameraCalibrationDataSet.h
 */

#pragma once
#include <array>
#include <string>

/** @ingroup RecordProcessing
 *  @brief A class containing all calibration data for the Camera Management Unit. */
class CameraCalibrationDataSet {
public:
	/** Create a data set initialized with 0 values */
	CameraCalibrationDataSet();
	/** Create a data set from calibration file
	 *  @param dataFile The path to the file to load.
	 */
	CameraCalibrationDataSet(const std::string& dataFile);

	uint32_t numPixelsX;  /**< The number of pixels in an image, along the X-axis */
	uint32_t numPixelsY; /**< The number of pixels in an image, along the Y-axis */
	double   angle1TBD;  /**< TO BE COMPLETED */
	double   angle2TBD;  /**< TO BE COMPLETED */
	double   angle3TBD;  /**< TO BE COMPLETED */
	double	pixelSizeX;	 /**<The size of a pixel in m in the camera�s X direction.*/
	double	pixelSizeY;	 /**<The size of a pixel in m in the camera�s Y direction.*/
	double 	 viewingAngleX;	/** < The angle between the viewing directions of extreme
								  pixels (leftmost and rightmost) pixels in the X direction,
								  in radians (always positive). */
	double 	 viewingAngleY;/** < The angle between the viewing directions of extreme
								  pixels (leftmost and rightmost) pixels in the Y direction,
								  in radians (always positive). */
	double f;	/**<The focal length in m: the distance over which initially parallel rays converge*/

	//  TMP: Add data here
};

