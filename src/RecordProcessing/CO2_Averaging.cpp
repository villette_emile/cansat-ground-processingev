/*
 * CO2_Averaging.cpp
 *
 *  Created on: 22 f�vr. 2019
 *      Author: Steven
 */

#include <CO2_Averaging.h>

CO2_Averaging::CO2_Averaging(RTP_ConfigDataSet theRTP_Config)
	:RTP_Config(theRTP_Config){
	SlAvg = RTP_Config.CO2_NumSampleToAverage;
}

CO2_Averaging::~CO2_Averaging() {}

bool CO2_Averaging::doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut){
	recordOut=recordIn;

	//CO2 sliding average
	double resultCO2Avg;
	resultCO2Avg = SlAvg.getAverage(recordIn.CO2_Voltage);

	//Storage
	bool ok = true;
	ok =ok && storeResult(resultCO2Avg, recordOut.CO2_VoltageCleaned,"CO2");
	return ok;
}

