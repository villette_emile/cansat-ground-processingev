/*
 * IsaTwoRecord.cpp
 *
 */

#include <IsaTwoRecord.h>
#include <cmath>
#include <iomanip>
#include "CSV_Row.h"
#include "CalculationUtils.h"
#include "DebugUtils.h"
#include "IsaTwoInterface.h"

static constexpr bool DBG=false;

#ifdef INCLUDE_AHRS_DATA
constexpr auto numAHRS_Elements=6;
#else
constexpr auto numAHRS_Elements=0;
#endif
#ifdef INCLUDE_GPS_VELOCITY
constexpr auto numGpsVelocityElements=2;
#else
constexpr auto numGpsVelocityElements=0;
#endif
constexpr auto numDataElements=20+numAHRS_Elements+numGpsVelocityElements;

IsaTwoRecord::IsaTwoRecord() : IsaTwoRecord(0, (unsigned int) IsaTwoRecordType::DataRecord) {};

IsaTwoRecord::IsaTwoRecord(unsigned int numExtraElements, unsigned int theRecordType) :
		CSV_Record(numDataElements+numExtraElements),
		recordType(theRecordType) {
	DBG_FCT
	initReferenceTime();
	clear();
}

void IsaTwoRecord::clear() {
	DBG_FCT
	timestamp=0L;
	std::fill(accelRaw.begin(), accelRaw.end(), 0.0);
	std::fill(gyroRaw.begin(), gyroRaw.end(), 0.0);
	std::fill(magRaw.begin(), magRaw.end(), 0.0);

	// GPS data
	GPS_Measures=false;
	GPS_LatitudeDegrees = GPS_LongitudeDegrees = 0.0;
#ifdef INCLUDE_GPS_VELOCITY
	GPS_VelocityKnots = GPS_VelocityAngleDegrees = 0.0;
	GPS_Altitude = 0.0;
#endif

#ifdef INCLUDE_AHRS_DATA
	// AHRS
	std::fill(AHRS_Accel.begin(), AHRS_Accel.end(), 0.0);
	roll=yaw=pitch=0.0;
#endif
	// Primary mission
	temperatureBMP=pressure=altitude=temperatureThermistor= 0.0;
	// Secundary mission
	CO2_Voltage=0.0;
}

IsaTwoRecord::~IsaTwoRecord() {}

void IsaTwoRecord::printCSV_Header(ostream &os) const {
  os << "type, timestamp(ms),accel.raw X, accel.raw Y, accel.raw Z,"
	 << "gyro raw X, gyro raw Y, gyro raw Z, "
	 << "mag. (µT) X, mag. (µT) Y, mag. (µT) Z,";
 // GPS
  os << "GPS present (0/1), GPS Lat degrees, GPS Long degrees, GPS alt, ";
#ifdef INCLUDE_GPS_VELOCITY
  os << "GPS Velocity knots, GPS Velocity degrees, ";
#endif
#ifdef INCLUDE_AHRS_DATA
  os << "AHRS accel m/s^2 X, AHRS accel m/s^2 Y, AHRS accel m/s^2 Z, "
     << "Roll, Yaw, Pitch, ";
#endif
  // Primary mission
  os << "TempBMP, Pressure, Altitude, TempThermistor,";
  // Secundary mission
  os << "CO2_Voltage";
}


void IsaTwoRecord::printCSV(ostream &os) const {
	os << recordType << separator << timestamp << separator;
	// A. IMU data
	printCSV(os, accelRaw, true);
	printCSV(os, gyroRaw, true);
	printCSV(os, magRaw, false, true);

	// B. GPS Data
	os << GPS_Measures << separator;
	printCSV(os, GPS_LatitudeDegrees, true);
	printCSV(os, GPS_LongitudeDegrees, true);
	printCSV(os, GPS_Altitude, true);
#ifdef INCLUDE_GPS_VELOCITY
	printCSV(os, GPS_VelocityKnots, true);
	printCSV(os, GPS_VelocityAngleDegrees, true);
#endif

#ifdef INCLUDE_AHRS_DATA
	// C. AHRS data
	printCSV(os, AHRS_Accel, false, true);
	printCSV(os, roll, true);
	printCSV(os, yaw, true);
	printCSV(os, pitch, true);
#endif

	// D. Primary mission
	printCSV(os, temperatureBMP, true);
	printCSV(os, pressure, true);
	printCSV(os, altitude, true);
	printCSV(os, temperatureThermistor, true);

	// D. Secondary mission
	printCSV(os, CO2_Voltage, false);

	// No separator after last value.
}

unsigned int IsaTwoRecord::parseRow(const CSV_Row &row) {
	DBG_FCT
	unsigned int idx=0;
	unsigned long readRecordType;
	try {
		readFromRow(row, idx, readRecordType);
		if (recordType != (unsigned int) readRecordType) {
			idx--; // revert to avoid erroneous diagnostic: problem is about idx 0;
			stringstream msg;
			msg << "Unexpected recordType=" <<readRecordType << " while expecting recordType=" << recordType;
			throw runtime_error(msg.str());
		}
		readFromRow(row, idx, timestamp);
		// A. IMU data
		readFromRow(row, idx, accelRaw);
		readFromRow(row, idx, gyroRaw);
		readFromRow(row, idx, magRaw);

		// B. GPS data
		readFromRow(row,idx,  GPS_Measures);
		readFromRow(row,idx,  GPS_LatitudeDegrees);
		readFromRow(row,idx,  GPS_LongitudeDegrees);
		readFromRow(row,idx,  GPS_Altitude);

#ifdef INCLUDE_GPS_VELOCITY
		readFromRow(row,idx,  GPS_VelocityKnots);
		readFromRow(row,idx,  GPS_VelocityAngleDegrees);
#endif

#ifdef INCLUDE_AHRS_DATA
		// C. AHRS data
		readFromRow(row,idx,  AHRS_Accel);
		readFromRow(row,idx,  roll);
		readFromRow(row,idx,  yaw);
		readFromRow(row,idx,  pitch);
#endif

		// D. Primary mission
		readFromRow(row,idx,  temperatureBMP);
		readFromRow(row,idx,  pressure);
		readFromRow(row,idx,  altitude);
		readFromRow(row,idx,  temperatureThermistor);

		//E. Secondary mission
		readFromRow(row,idx,  CO2_Voltage);

		return idx;
	}
	catch(const exception &e) {
		cout << "Exception parsing IsaTwoRecord file, after reading value '" << row[idx] << "' at index "<<idx << endl;
		// Do not print exception description: it will be done
		// by the caller.
		throw;
	}
}

void IsaTwoRecord::initReferenceTime() {
	refTimePoint=high_resolution_clock::now();
}

void IsaTwoRecord::updateTimeStamp() {
	high_resolution_clock::time_point tNow=high_resolution_clock::now();
	duration<double, std::milli> timeSpan = tNow - refTimePoint;
	timestamp=(unsigned long) timeSpan.count();
}

ostream& operator<<(ostream& os, const IsaTwoRecord & record) {
	record.printCSV(os);
	return os;
}

istream& operator>>(istream& is, IsaTwoRecord & record) {
	record.readFromCSV(is);
	return is;
}
