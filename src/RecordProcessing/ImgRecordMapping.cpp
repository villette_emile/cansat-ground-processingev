/*
 * ImgRecordMapping.cpp
 *
 */

#include <ImgRecordMapping.h>
#include <cstdlib>
#include "FileMgr.h"
#include "CinManip.h"

static constexpr unsigned long MaxAllowedTimeDifference=100; //msec. The maximum time difference allowed between an image and the closest ground record.

ImgRecordMapping::ImgRecordMapping(	 const std::string& theImgFolderIn,
									 const std::string& theImgFolderOut,
									 const unsigned long theImagerActivationTimestamp)
		: 	imgFolderIn(theImgFolderIn),
			imgFolderOut(theImgFolderOut),
			imagerActivationTimestamp(theImagerActivationTimestamp),
			previousTimestamp(0L)
{
	cout << "Mapping image files from '" << imgFolderIn << "' into '" << imgFolderOut << "'..." << endl;
	cout << "Master clock at imager activation: " << imagerActivationTimestamp << endl;
	if (!FileMgr::directoryExists(imgFolderIn)) {
		throw runtime_error(std::string("Error: directory '") + imgFolderIn + "' does not exist. Aborted.");
	}
	if (!FileMgr::createDirectory(imgFolderOut, false, false)) {
		throw runtime_error(std::string("Error: could not create directory '") + imgFolderOut + "'. Aborted.");
	}

	std::vector<std::string> imgFileNames;
	bool result= FileMgr::listDirectory(imgFileNames,imgFolderIn,"jpg");
	if (!result) {
		throw runtime_error(std::string("Cannot list directory '")  + imgFolderIn + "'. Aborted.");
	}
	if (imgFileNames.empty()) {
		throw runtime_error(std::string("No image file in directory '")  + imgFolderIn + "'. Aborted.");
	}

	// extract timestamps from file names
    for (auto fileName : imgFileNames ) {
    	unsigned long ts= (unsigned long) atol(fileName.substr(0, 8).c_str());
    	if (ts!=0) {
    		originalFileTimestamps.push_back(ts);
    	}
    	else {
    		LOG_IF(DBG, DEBUG) << "Could not convert file name '" << fileName << "' (ignored).";
    	}
    }
	if (originalFileTimestamps.empty()) {
		throw runtime_error(std::string("No mappable image file in directory '")  + imgFolderIn + "'. Aborted.");
	}
    std::sort(originalFileTimestamps.begin(), originalFileTimestamps.end());
    nextFileToMap = originalFileTimestamps.begin();
    nextTimestampToMap= *nextFileToMap + imagerActivationTimestamp;
    cout << "Found " << originalFileTimestamps.size() << " images to map." << endl;
    if (DBG_CONSTRUCTOR)
    {
    	cout << "originalFileTimestamps (" << originalFileTimestamps.size() << "):" << endl;
    	for (auto ts : originalFileTimestamps ) {
    		cout << "  " << ts << endl;
    	}
    }
}

ImgRecordMapping::~ImgRecordMapping(){}

bool ImgRecordMapping::doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut) {
	LOG_IF(DBG_PROCESS, DEBUG) << "--------";
	LOG_IF(DBG_PROCESS, DEBUG) << "nextTimestampToMap=" << nextTimestampToMap;
   	LOG_IF(DBG_PROCESS, DEBUG) << "previousTimestamp =" << previousTimestamp;
   	LOG_IF(DBG_PROCESS, DEBUG) << "record timestamp  =" << recordIn.timestamp;

	bool ok = true;
	unsigned long newTimestamp;
	recordOut=recordIn; // This processor is transparent for the records.
	if (nextFileToMap >= originalFileTimestamps.end()) {
	  	LOG_IF(DBG_PROCESS, DEBUG) << "No file left to map";
		// Everything is mapped, just be transparent.
		return ok;
	}
    while ( (nextFileToMap < originalFileTimestamps.end())
    		&& (recordIn.timestamp >= nextTimestampToMap)) {
    	LOG_IF(DBG_PROCESS, DEBUG) << "While: nextTimestampToMap=" << nextTimestampToMap;
    	// The next image to map is between the previous record and the current one.
    	// Let's map it, if the difference is not too large
    	unsigned long timeDifference=recordIn.timestamp - nextTimestampToMap;
    	if (previousTimestamp==0) {
    		newTimestamp=recordIn.timestamp;
    	} else if (timeDifference < ((recordIn.timestamp - previousTimestamp)/2)) {
    		// image is closer to current record
    		newTimestamp=recordIn.timestamp;
    	} else {
    		// image is closer to previous record
    		newTimestamp=previousTimestamp;
    		timeDifference=nextTimestampToMap-previousTimestamp;
    	}

    	if (timeDifference <= MaxAllowedTimeDifference) {
    		// Copy image file as 'newTimestamp.jpg'
    		ostringstream from, to;
    		from << imgFolderIn << "/" << setfill('0') << setw(8) << *nextFileToMap <<".jpg";
    		to << imgFolderOut << "/" << setfill('0') << setw(8) << newTimestamp <<".jpg";
    		cout  << "  Mapping '" << from.str() << "' " << endl;
    		cout <<  "       to '" << to.str() << "', " << endl;
    		cout <<  "   deltaT=" <<  timeDifference << " msec"  << endl;
    		ok= FileMgr::copyFile(from.str(),to.str());
    		if (!ok) {
    			cout << "*** Error copying file!***" << endl;
    		}
    	}
    	else {
    		cout << " *** Skipped image at " << *nextFileToMap
    			 <<". Closest record is at " << timeDifference << " msec." << endl;
    	}
    	// move to next file
    	nextFileToMap++;
    	if (nextFileToMap < originalFileTimestamps.end()) {
    		nextTimestampToMap=*nextFileToMap + imagerActivationTimestamp;
    	} else nextTimestampToMap=0;
    }
    previousTimestamp=recordIn.timestamp;

	return ok;
}
