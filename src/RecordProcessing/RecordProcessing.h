/*
 * Record Processing.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the package should include a tag @ingroup RecordProcessing
 * in the class documentation block.
 */

 /** @defgroup RecordProcessing Record Processing
 *  @brief The set of ground processing classes dedicated to Processing IsaTwoGroundRecords.
 *  
 *  The Record Processing package contains all classes used in the Ground Segment to process records.
 *  
 *  _Dependencies_\n
 *  This package relies on:
 *  	- the Utils package
 *  	- the easylogging++ library
 *
 *  @todo //.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2018-2019 Cansat team (IsaTwo).
 */
