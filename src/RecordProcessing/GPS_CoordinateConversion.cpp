/*
 * GPS_CoordinateConversion.cpp
 *
 *  Created on: 18 janv. 2019
 *      Authors: Steven and Lorenz
 */

#include <GPS_CoordinateConversion.h>
#include <math.h>


GPS_CoordinateConversion::GPS_CoordinateConversion(const RTP_ConfigDataSet theRTP_Config)
		: RTP_Config(theRTP_Config){
	//Matrix ECEF to NED
	LCCMatrix[0][0] = -sin(RTP_Config.localReferentialOrigin_Latitude*M_PI/180)*cos(RTP_Config.localReferentialOrigin_Longitude*M_PI/180);
	LCCMatrix[0][1] = -sin(RTP_Config.localReferentialOrigin_Latitude*M_PI/180)*sin(RTP_Config.localReferentialOrigin_Longitude*M_PI/180);
	LCCMatrix[0][2] = cos(RTP_Config.localReferentialOrigin_Latitude*M_PI/180);
	LCCMatrix[1][0] = -sin(RTP_Config.localReferentialOrigin_Longitude*M_PI/180);
	LCCMatrix[1][1] = cos(RTP_Config.localReferentialOrigin_Longitude*M_PI/180);
	LCCMatrix[1][2] = 0;
	LCCMatrix[2][0] = -cos(RTP_Config.localReferentialOrigin_Latitude*M_PI/180)*cos(RTP_Config.localReferentialOrigin_Longitude*M_PI/180);
	LCCMatrix[2][1] = -cos(RTP_Config.localReferentialOrigin_Latitude*M_PI/180)*sin(RTP_Config.localReferentialOrigin_Longitude*M_PI/180);
	LCCMatrix[2][2] = -sin(RTP_Config.localReferentialOrigin_Latitude*M_PI/180);

	ECEFCoordinates = {99999,99999,99999};

	//Coordinates of the operating location convert to ECEF
	localReferentialOrigine_Coordinate[0] = RTP_Config.localReferentialOrigin_Latitude;
	localReferentialOrigine_Coordinate[1] = RTP_Config.localReferentialOrigin_Longitude;
	localReferentialOrigine_Coordinate[2] = RTP_Config.localReferentialOrigin_Altitude;
	ECEFConvertion(localReferentialOrigine_Coordinate);

}

GPS_CoordinateConversion::~GPS_CoordinateConversion(){}

bool GPS_CoordinateConversion::doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut) {
			recordOut=recordIn;

			bool ok = true;

			if (recordIn.GPS_Measures){		//If GPS is fix and send data
				//Convertion to ECEF

				ECEFCoordinates[0] = recordIn.GPS_LatitudeDegrees;
				ECEFCoordinates[1] = recordIn.GPS_LongitudeDegrees;
				ECEFCoordinates[2] = recordIn.GPS_Altitude;
				ECEFConvertion(ECEFCoordinates);
				//Storage of the converted data
				for(unsigned long i = 0; i < 3; i++){
					ok =ok && storeResult(ECEFCoordinates[i], recordOut.positionEarthCentric[i],"ecc");
				}

				//Convertion to NED
				NEDCoordinates = {0, 0, 0};
				for (unsigned long i = 0; i < 3; i++){
					for (int j = 0; j < 3; j++){
						NEDCoordinates[i] += LCCMatrix[i][j] * (ECEFCoordinates[j]-localReferentialOrigine_Coordinate[j]);
					}
				}

			}

			else{		//If GPS is not fix and doesn't send data
				//Storage of the previous converted data
				for(unsigned long i = 0; i < 3; i++){
					ok =ok && storeResult(ECEFCoordinates[i], recordOut.positionEarthCentric[i],"pecc");
				}
			}

			//Storage of the final results
			for(unsigned long i = 0; i < 3; i++){
				ok =ok && storeResult(NEDCoordinates[i], recordOut.positionLocal[i],"lcc");
			}
			return ok;
}

void GPS_CoordinateConversion::ECEFConvertion(array<double, 3> &theECEFCoordinates){
	double latitudeInDegrees = theECEFCoordinates[0];	/**< The latitude in decimal degrees, ([-90;90], + = N, - = S) */
	double longitudeInDegrees = theECEFCoordinates[1];  /**< The longitude in decimal degrees ([-180;180], + =E, - =W) */
	double altitudeFromGPS = theECEFCoordinates[2];				/**< Altitude of antenna, in meters above mean sea level (geoid) */

	// Conversion of the data in radiant
	double latitudeGeodetic = (latitudeInDegrees/180)*M_PI;
	double longitudeGeodetic = (longitudeInDegrees/180)*M_PI;

	//Constants
	constexpr double equatorialRadius = 6378137; // Earth equatorial radius
	constexpr double  polarRadius = 6356752.31424518; //Earth polar radius
	constexpr double constantForCalculations = 1 - (polarRadius*polarRadius)/(equatorialRadius*equatorialRadius);//A constant for the N formula

	// Calculations of the earth radius at a given place thanks to geodetic latitudeGeodetic
	double squareRoot = sqrt(1-constantForCalculations*sin(latitudeGeodetic)*sin(latitudeGeodetic));
	double N = (equatorialRadius)/squareRoot;

	// Calculations of the x,y,z coordinates. Taking the
	double x = (N+altitudeFromGPS)*cos(latitudeGeodetic)*cos(longitudeGeodetic);
	double y = (N+altitudeFromGPS)*cos(latitudeGeodetic)*sin(longitudeGeodetic);
	double z = ((polarRadius*polarRadius/(equatorialRadius*equatorialRadius))*N+altitudeFromGPS)*sin(latitudeGeodetic);

	// Store data in the table received
	theECEFCoordinates[0]=x;
	theECEFCoordinates[1]=y;
	theECEFCoordinates[2]=z;
}
