/*
 * RP_CSV_SocketSend.h
 *
 */

#pragma once
#include "SocketStringSender.h"
#include "RecordProcessor/RecordProcessor.h"
#include "DebugUtils.h"

/** @ingroup RecordProcessor
 *  @brief A processor which manages a TCP connexion and sends CSV_Record or strings)into
 *  a TCP socket.
 *
 *  This class relies on the SocketStringSender gateway to run a socket server, accept a single
 *  external connection and sends everything to the socket as long as the client is connected.
 *  If no connection is open, or the client disconnects, it either:
 *   - blocks until another connection request is received, blocking any activity in this thread
 *     (if option=suspendOnClientDisconnection)
 *   - just ignores the incoming record (just copying them to is input) until a connexion is
 *     reestablished.
 *
 *     @remark: This class also also accepts strings to be sent to the socket, without any
 *     		    useless parsing overhead.
 */
template<class RECORD>
class RP_CSV_SocketSend : public RecordProcessor<RECORD, RECORD>, public SocketStringSender {
public:
	using  clock  = std::chrono::high_resolution_clock;
	using time_point_type   = std::chrono::time_point< std::chrono::high_resolution_clock> ;
	using SocketStringSender::ClientDisconnectOption_t;
	using RecordProcessor<RECORD, RECORD>::doProcess;
	// The above line brings all doProcess methods in this name space. This is
	// required to avoid name-hiding.

	/** Constructor
	 *  @param thePortNbr The number of the port on which to accept TCP connections.
	 *  @param theTransmissionPeriod The minimum delay between two successive transmissions
	 *  	    					 on the socket. Extraneous records are not transmitted.
	 *  @param option If suspendOnClientDeconnexion, the processing is suspended
	 *  			  when the client is not connected and resumed afterward.
	 *  			  This is the appropriate mode when processing offline
	 *  			  data (it would not make sense not to wait for a client to ingest them).
	 *  			  If proceedDuringClientDisconnection, incoming records are just passed to the
	 *  			  next processor (if any) when no client is connected.
	 *  			  This is the appropriate mode when processing a real-time feed: it would
	 *  			  not make sense to miss data because the client disconnected.
	 *  @param fb	 If withVisualFeedback, the processor prints some feedback on cout:
	 *  						a '.' for each record transferred,
	 *  						a '!' for each record ignored because no client is connected and
	 *  						      suspendOnClientDeconnexion is false.
	 *
	 */
	RP_CSV_SocketSend(	unsigned short thePortNbr,
						unsigned long theTransmissionPeriod,
						ClientDisconnectOption_t option=suspendOnClientDisconnection,
						Processor::VisualFeedback_t fb=Processor::noVisualFeedback)
			: RecordProcessor<RECORD,RECORD>(fb),
			  SocketStringSender(thePortNbr, option),
			  transmissionPeriod(theTransmissionPeriod),
			  lastEmission(clock::now())
			  {
				DBG_FCT
			  }

protected:

	   /** Prepare the socket sender. It is called just before the first call to
	    *  of the doProcess() methods.
	    *  @return True if preparation was successful and the processor is ready
	    *          to accept records, false otherwise.
	    */
	   	virtual bool doPrepare() { DBG_FCT ; return prepare(); };

	   	/** Process the provided record: transmit it to the socket if a connexion is open.
	   	 *  If no connexion is open, this method blocks if option=suspendOnClientDisconnection
	   	 *  or just does not transmit the record if option=proceedDuringClientDisconnection
	   	 *
	   	 *  @param recordIn The record to process.
	   	 *  @param recordOut The data resulting from the processing (always identical to recordIn).
	   	 *  @return True if processing was successful, false otherwise.
	   	 */
	   	virtual bool doProcess(const RECORD& recordIn , RECORD& recordOut) {
	   		DBG_FCT
			bool result=false;
	   		recordOut=recordIn;
	   		std::ostringstream str;
	   		recordIn.printCSV(str);
	   		if ((clock::now() - lastEmission) > chrono::milliseconds(transmissionPeriod))
	   		{
	   			lastEmission=clock::now();
	   			result= send(str.str());

	   		}
	   		else result= true;
	   		return result;
	   	};

	   	/** Process the provided record, received as a String, generating result as string as well.
	   	 * This method is overridden to avoid parsing the string into a CSV_Record: this is not
	   	 * required.
	   	 * @param recordIn  The record to process, in string format. It is assumed NOT to be terminated by
	   	 *                  a '\\n'.
	   	 * @param recordOut The data resulting from the processing, in string format, without a final '\\n'.
	   	 * 					Always set to recordIn
	   	 * @return true if processing was successful, false otherwise.
	   	 */
	   	virtual bool doProcess(const std::string& recordIn, std::string& recordOut) {
	   		recordOut=recordIn;
	   		return send(recordIn);
	   	}

	   	/** Release socket resources.
	   	 */
	   	virtual void doTerminate() { terminate();};
private:
	static constexpr auto DBG=false;
	unsigned long transmissionPeriod; /** Minimum delay between 2 successive transmissions, in msec */
	std::chrono::time_point<std::chrono::high_resolution_clock> lastEmission; /** timestamp of the last transmission on the socket */
};




