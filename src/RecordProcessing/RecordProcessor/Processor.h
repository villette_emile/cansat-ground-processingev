/*
 * Processor.h
 *
 */

#pragma once
#include <string>
#include <iostream>
#include "CSV_Record.h"
#include "DebugUtils.h"


/** @ingroup RecordProcessor
 *  @brief A common abstract base class for all processors.
 *
 *  Processors are object performing some processing the data they received. Input and output of processor is
 *  either strings (for some processors), either CSV_Records (for other processors).
 *
 *  Processors can be chained in any order, configuring the chain using the appendProcessor() method. Each processor
 *  in the chain will perform its job and provide its results (which could be identical to to its input) to the
 *  next processor in the chain.
 *
 *  The processing of the first string causes the #doPrepare() call to be propagated along the processor
 *  chain, for all processors to initialize whatever resource they need.
 *
 *  A call to #terminateProcessing() results in a #doTerminate() call propagated along the processor
 *  chain, for all processors to release their resources.  This termination phase should be initiated
 *  as soon as the processing is over, but is triggered in the destructor as well, for safety.
 *
 *  Results can be obtained either from a particular processor (getLocal...() methods) or for the
 *  complete processing chain (getFinal...() methods).  Please note that results are always available
 *  in string format (all CSV_Records can be converted to string format), but not always as records
 *  (since not all strings are CSV records representations).
 *
 *  As an example, a chain of 6 processors could be used to
 *  -# perform calculation A,
 *  -# echo result of calculation A on cout
 *  -# perform calculation B,
 *  -# send result to socket,
 *  -# save result to file.
 *  -# echo result of calculation B on cout.
 *
 *  The Processor class implements the processor chaining logic, the initialization and termination
 *  logic, and define the #processString() and getResultxxxx methods available in any processor.
 *
 */
class Processor {
public:
	/** Enum to configure the level of feadback requested on the standard output */
	typedef enum { 	withVisualFeedback=1,
					noVisualFeedback=0 } VisualFeedback_t;
	Processor(VisualFeedback_t fb=withVisualFeedback);
	/** Destructor. Takes care of the termination of the complete processor chain. */
	virtual ~Processor();

	/** Process the provided string..
	 * @param  aString The string to process.
	 * 		   It is assumed NOT to be terminated by a '\n'.
	 * @return true if processing was successful, false otherwise.
	 */
    virtual bool processString(const std::string& aString);

    /** Obtain the result of this processor's processing on the last processed data, in string format.
     *  @return The processing result.It would be empty if this method is called before any
     *          data has been processed.
     */
    virtual std::string getLocalResult()=0;

    /** Obtain the result the complete processing chain on the last processed data, in string format.
      *  @return The processing result.It would be empty if this method is called before any
      *          data has been processed.
      */
    virtual std::string getFinalResult() {
    	DBG_FCT
    	return lastProcessor->getLocalResult();
    }

    /** Obtain the result of this processor's processing on the last processed data.
      *  @return The processing result.It would be null if the method is called on a processor which
      *          only processes strings and would return a blank record if this method is called
      *          before any data has been processed. Since the base class has no way to define
      *          which subclass of CSV_Record the processor will actually produce, this pointer
      *          should be dynamic_casted by the user to the appropriate subclass.
      *          (Or the result record should be accessed through methods specific to the subclass.
      */
    virtual const CSV_Record* getLocalResultRecord(){return nullptr;};

    /** Obtain the result of the whole processing chain on the last processed data, in string format.
     *  @return The processing result.It would be null if the last processor in the chain
     *          only processes strings and would return a blank record if this method is called
     *          before any data has been processed. Since the base class has no way to define
     *          which subclass of CSV_Record the last processor in the chain will actually produce,
     *          this pointer should be dynamic_casted by the user to the appropriate subclass.
     *          (Or the result record should be accessed from the last processor, through methods
     *          specific to the subclass.
     */
    virtual const CSV_Record* getFinalResultRecord() {
    	return lastProcessor->getLocalResultRecord();
    }

	/** Allow the whole chain of processors to perform any action required as soon as possible after
	 *  the last record is processed, that should preferably not wait until the destructor is called
	 *  A well behaved user should call it as soon as processing is over (RecordSource does).
	 *  This method call the #doTerminate() method of every processor, hence
	 *  supports multiple calls.
	 */
    virtual void terminateProcessing();

    /** Add a processor at the end of the processing chain. Data will be sent through
     *  the whole chain of processors, in the order they were appended.
     *
     *  Note that Processor does not take over the ownership of the appended processor, and will
     *  not delete it. It is the caller's responsibility to
     *  -# ensure the appended processor remains valid while the processor it is appended too is valid
     *  -# ensure the appended processor  is correctly disposed of when other processors of the
     *     chain are destroyed. This should be done either by declaring all processors on the stack,
     *     or by allocating all processors from the heap and calling #deleteAppendedProcessors()
     *     when appropriate.
     *
     *  This method is intended to be called on the first processor of a chain. Calling it
     *  on an intermediate processor will cause the first processors to be out of sync.
     *  It is allowed to append a chain of processor to another one: the sequence
     *  @code a.appendProcessor(b); a.appendProcessor(c); a.appendProcessor(d) @endcode
     *  results in a->b->c->d, and a further call to
     *  @code k.append(a) @endcode will result in k->a->b->c->d.
     *  Calls like: @code m.appendProcessor(b) @endcode or @code b.appendProcessor(p) @endcode
     *  will have unpredictible results.
     *
     * @param processor The processor to appended.
     */
    virtual void appendProcessor(Processor& processor);

    /** Delete the next processor and the whole chain of processors from it.
     *  processor is not deleted: this responsibility remains at the caller..
     *  This methods is intended to be used on the head of a chain of processors, otherwise, it
     *  will result in a dangling pointer in the previous processor in the chain.
     *  WARNING: This method should only be used if all processors in the chain have been
     *           allocated with the new operator.
     */
    virtual void deleteAppendedProcessors();

    virtual void printProcessorDescription(std::ostream & os) {
    	os << typeid(*this).name();
    }

    /** Print a description of the complete chain of processors
     *  @param os The stream on which to print the descritpion
     */
    void printProcessorChainDescription(std::ostream & os);
protected:
    //----------------------------------------------------------------------------------------------------
    /** @name 1. Methods intended for overriding
     *  @{
     */
    /** Overload this method to perform any preparation that cannot be performed
     *  in the constructor (optional). It is called just before the first call to
     *  of the doProcess() methods.
     *  @return true if preparation was successful and the processor is ready
     *          to accept records, false otherwise.
     */
    virtual bool doPrepare()=0;

    /** Process the provided record, received as a String.
     * @param  recordString The record to process, in string format.
     * 		   It is assumed NOT to be terminated by a '\\n'.
     * @return true if processing was successful, false otherwise.
     */
    virtual bool doProcess(const std::string& recordString)=0;

    /** Overload this method to perform any action required as soon as possible after the last
     *  record is processed and should preferably not wait until the destructor is called (optional)
     *  This method will be called at the latest in the destructor, but a well behaved user could
     *  have it called earlier (RecordSource does). This method MUST support multiple calls.
     *  Default version is empty.
     */
    virtual void doTerminate() {};
    /** @} */

    //----------------------------------------------------------------------------------------------------
    /** @name 2. Methods that should normally not require overriding
     *  In the standard usage of this class, the following methods should not require
     *  overriding.
     *  @{
     */

    /** Propagate the call to #doPrepare() along the chain (if not done before).
     *  @return True of all processors succesfully prepared.
     */
    virtual bool Prepare();
    /** Check whether this processor is prepared */
    virtual bool isPrepared() const { return prepared;};

    /** Get the applicable feedback level on standard input, for this processor */
    virtual bool visualFeedback() const { return useVisualFeedback;};

    /** Forward the results of the processing to the next processor, if any.
     *  @return true if everything ok, false otherwise.
     */
    virtual bool forwardResultToNextProcessor();

    /** Obtenain a pointer to the next processor in the chain */
    virtual Processor* getNextProcessor() { return nextProcessor;};
    /** @} */ // End of group of methods that do usually not required overriding

private:
    Processor *nextProcessor{}; 	/**< The processor in charge of processing the results of the
	    								 processing by the current processor. */
    Processor *lastProcessor{}; 	/**< The last processor in the chain (store to avoid
	    								 going through the whole chain every time */
    bool useVisualFeedback;			/**< If true, feedback on cout is allowed during error-less
	    								 processing, else feedback on cout in only allowed in error
	    								 cases. */
    bool prepared;					/**< True if a successful call to doPrepare() happened */
    static constexpr auto DBG=false;

};



