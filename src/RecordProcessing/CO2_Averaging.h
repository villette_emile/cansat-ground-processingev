/*
 * CO2_Averaging.h
 *
 *  Created on: 22 f�vr. 2019
 *      Author: Steven
 */

#pragma once
#include <IsaTwoGroundRecord.h>
#include <MovingAverage.h>
#include "RecordProcessor/RecordProcessor.h"
#include "RTP_ConfigDataSet.h"

/** @ingroup RecordProcessing
 *  @brief A processor which performs a moving average on the CO2 readings to remove meaningless noise.
 */
class CO2_Averaging: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>{
public:
	CO2_Averaging(RTP_ConfigDataSet theRTP_Config);
	virtual ~CO2_Averaging();
	/** Perform a calibration of the data from secondary mission, converts them to I.S. units and store it using the base class'
			 *  storeResult() method.
			 * @param recordIn  The record to process.
			 * @param recordOut The record resulting from the processing.
			 * @return True if the process success.
			 */
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut);

private:
	RTP_ConfigDataSet RTP_Config;
	MovingAverage SlAvg;
};
