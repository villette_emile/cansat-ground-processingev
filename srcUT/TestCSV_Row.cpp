#include "TestCSV_Row.h"
#include "cute.h"
#include "CSV_Row.h"
#include <fstream>
#include <iostream>

using namespace std;

const char* const testFileNoFinalCR="TestFiles/CSV_Row/commentLines_NoFinalCR.csv";
const char* const testFileFinalComment="TestFiles/CSV_Row/commentLines_NoFinalCR_FinalComment.csv";
const char* const testFileFinalEmptyLine="TestFiles/CSV_Row/commentLines_NoFinalCR_FinalEmptyLine.csv";
const char* const testFileFinalCR="TestFiles/CSV_Row/commentLines_FinalCR.csv";

const int numTestFiles2records=4;
const char* const testFiles2records[]= {	testFileNoFinalCR,
											testFileFinalComment,
											testFileFinalEmptyLine,
											testFileFinalCR
};

void testCSV_RowParsing() {
	ifstream fin(testFileNoFinalCR);
	CSV_Row row(fin);
	CSV_Row::skipOneRow(fin); // skip header
	row.next();
	ASSERT_EQUAL(row[0],"aString");
	ASSERT_EQUAL(row.getFloat(1),1.23f);
	ASSERT_EQUAL(row.getDouble(2),4.56);
	ASSERT_EQUAL(row.getInt(3),7);
	ASSERT_EQUAL(row.getUInt(4),8);
	ASSERT_EQUAL(row.getLong(5),9);
	ASSERT_EQUAL(row.getULong(6),10);
	ASSERT_EQUAL(row.getBool(7),false);
	row.next();
	ASSERT_EQUAL(row[0],"another string");
	ASSERT_EQUAL(row.getFloat(1),1.23f);
	ASSERT_EQUAL(row.getDouble(2),4.56);
	ASSERT_EQUAL(row.getInt(3),7);
	ASSERT_EQUAL(row.getUInt(4),8);
	ASSERT_EQUAL(row.getLong(5),9);
	ASSERT_EQUAL(row.getULong(6),10);
	ASSERT_EQUAL(row.getBool(7),true);
	fin.close();
}

void testEOF_Condition()
{
	int counter;
	for (int i = 0 ; i < numTestFiles2records; i++) {
		cout << endl << endl << "Reading file '" << testFiles2records[i] << "'" << endl;
		ifstream in(testFiles2records[i]);
		CSV_Row row(in);
		row.skipOneRow(in);
		counter=0;
		while (row.next() && (counter <10)) {
			counter++;
			cout << counter << ": read: " << row.getCompleteRow() << endl << flush;
		}
		ASSERT_EQUAL(2, counter);
	}
	cout << endl << "Done" << endl;
}

cute::suite make_suite_TestCSV_Row() {
	cute::suite s { };
	s.push_back(CUTE(testCSV_RowParsing));
	s.push_back(CUTE(testEOF_Condition));
	return s;
}
