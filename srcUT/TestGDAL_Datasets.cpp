#include "TestGDAL_Datasets.h"
#include "cute.h"
#include <iostream>
#include <iomanip>
#include <cassert>
#include <algorithm>
#include "ImgProcessing/GIS_ContourDataset.h"
#include "ImgProcessing/GIS_RasterDataset.h"
#include "ImgProcessing/GIS_TrajectoryDataset.h"
#include "ImgProcessing/JPEG_Image.h"
#include "FileMgr.h"

using namespace std;

const char* tmpDir="TestFiles/tmpFiles";
const char* contourDS_Name="testVDataSet";
const char* rasterDS_Name="testRDataSet";
const char* trajectoryDS_Name="testTrajDataSet";
const char* testJPG="TestFiles/GIS_Datasets/test.JPG";

void insertContours(GIS_ContourDataset* ds, int numContours, bool reset=false, bool includeViewingLines=false) {
	assert(ds);
	double canLong, canLat;
	static double startLat;
	static double startLong;
	static unsigned long count=0;
	if (reset || (count==0)) {
		startLat= -10.0;
		startLong=-130.0;
		count=0;
	}
	const double stepLat=0.8;
	const double stepLong=4.1;
	const double scale=1.8;
	const double startAltitude=123.45;
	for (int i = 0; i< numContours; i++) {
		double lt=startLat+count*stepLat;
		double lg=startLong+count*stepLong;
		if (lg> 150) {
			startLong-=300;
		}
		if (lt > 60) {
			lt -=100;
		}
		if (includeViewingLines) {
			canLat = lt +2;
			canLong=lg + 1.5;
		} else {
			canLong=canLat=1000; // = no data
		}
		ds->addContour(	lt, lg,
						lt+3*scale, lg+1*scale,
						lt+2*scale, lg+2*scale,
						lt+1*scale, lg+2*scale,
						count,
						startAltitude+count,
						canLat, canLong);
		count++;
	}
}

void vectorDatasetCreationTests() {
	try {
		string dsPath(tmpDir);
		dsPath+='/';
		dsPath+=contourDS_Name;

		GIS_ContourDataset* ds;
		cout << "Creating dataset with default format..." << endl;
		cout << "  (" << dsPath << ")" << endl;
		GIS_ContourDataset::deleteFiles(dsPath, true);
		ASSERT(!FileMgr::directoryExists(dsPath));
		ASSERT(!FileMgr::fileExists(dsPath));

		cout << "TEST 1. while inexistent, no replace" << endl;
		ds=new GIS_ContourDataset(dsPath,false /*replace*/);
		ASSERT(ds != nullptr);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
		GIS_ContourDataset::deleteFiles(dsPath, true);

		cout << "TEST 2. while inexistent, with replace " << endl;
		ds=new GIS_ContourDataset(dsPath,true /*replace*/);
		ASSERT(ds != nullptr);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
        // do not remove: used by next test.

		cout << "TEST 3. while existent, replace" << endl;
		ds=new GIS_ContourDataset(dsPath,true /*replace*/);
		ASSERT(ds != nullptr);
		// insert some data: we need it for the next test
		insertContours(ds, 1);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
		// Do not delete, needed for next test

		cout << "TEST 4. while existent, no replace " << endl;
		ds=new GIS_ContourDataset(dsPath,false /*replace*/);
		ASSERT(ds != nullptr);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
		GIS_ContourDataset::deleteFiles(dsPath, true);

		cout << "TEST 5. invalid format " << endl;
		ASSERT_THROWS(ds=new GIS_ContourDataset(dsPath,false /*replace*/, 0, "inexistent format"), runtime_error);
		cout << "TEST 6. Format does not support 'create'" << endl;
		ASSERT_THROWS(ds=new GIS_ContourDataset(dsPath,false /*replace*/, 0, "AeronavFAA"), runtime_error);

		cout << "TEST 7. Format does not support 'vector'" << endl;
		ASSERT_THROWS(ds=new GIS_ContourDataset(dsPath,false /*replace*/, 0, "ADRG"), runtime_error);
	}
	catch(exception &e) {
		cout << "Exception received: " << e.what() << endl;
		ASSERT(false);
	}

}

void insertContoursInSingleDataset()  {
	string dsPath(tmpDir);
	dsPath+='/';
	dsPath+=contourDS_Name;
	dsPath+="SAVED";
	GIS_ContourDataset* ds;
	cout << "Creating data, with unlimited number of layers";
			cout << "  (" << dsPath << ")" << endl;
	GIS_ContourDataset::deleteFiles(dsPath, true);
	ASSERT(!FileMgr::directoryExists(dsPath));
	ASSERT(!FileMgr::fileExists(dsPath));
	ds=new GIS_ContourDataset(dsPath,false /*replace*/);
	ASSERT(ds != nullptr);

	// 10 contours without viewing lines, 10 with viewing lines.
	insertContours(ds, 5, false, false);
	insertContours(ds, 5, false, true);

	delete ds;
	ds=nullptr;
	ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));

	// Reread file, check number of contours
	ds=new GIS_ContourDataset(dsPath,false /*replace*/);
	ASSERT(ds != nullptr);
	ASSERT(ds->checkIntegrity());
	ASSERT(ds->getContourCount()==10);

	delete ds;
	ds = nullptr;
	// Do not delete dataset: this one we use for our tests.
}

void checkFilesDeleted(const string &fileName, unsigned short numToCheck) {
	// Check all other dataset are deleted as well.
	for (int i = 2; i <= numToCheck; i++) {
		stringstream fn;
		fn << fileName;
		fn << setw(2) << setfill('0') << i;
		ASSERT(!FileMgr::directoryExists(fn.str()));
		ASSERT(!FileMgr::fileExists(fn.str()));
	}
}

void insertContoursInMultipleDataset()  {
	string dsPath(tmpDir);
	dsPath+='/';
	dsPath+=contourDS_Name;

	GIS_ContourDataset* ds;
	cout << "Creating data, with max. 10 layers per dataset";
			cout << "  (" << dsPath << ")" << endl;
	GIS_ContourDataset::deleteFiles(dsPath, true);
	checkFilesDeleted(dsPath, 10);
	ds=new GIS_ContourDataset(dsPath,false /*replace*/, 10);
	ASSERT(ds != nullptr);

    insertContours(ds, 95, true /*reset*/);
	delete ds;
	ds=nullptr;
	ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));

	// Reread file, check number of contours
	ds=new GIS_ContourDataset(dsPath,false /*replace*/);
	ASSERT(ds != nullptr);
	ASSERT(ds->checkIntegrity());
	ASSERT(ds->getContourCount()==10);

	delete ds;
	ds = nullptr;
	ASSERT(GIS_ContourDataset::deleteFiles(dsPath, true));
#ifdef _WIN32
	// For unknown reason, on Windows, deletion of the first dataset succeeds but does not remove the files.
	// Removing it manually does not fix the problem: the problem is that the files are locked by another process.
	// Why on windows and not on MacOS ???
	/*
		string cmd="rmdir /s /q ";
		string winPath(dsPath);
		std::replace(winPath.begin(), winPath.end(), '/', '\\');
		cmd += winPath;
		cout << "Windows only patch: deleting manually..." << endl;
		cout << cmd << endl;
		system(cmd.c_str());
	*/
#else
	ASSERT(!FileMgr::directoryExists(dsPath));
	ASSERT(!FileMgr::fileExists(dsPath));
#endif
	// Check all other dataset are deleted as well.
	checkFilesDeleted(dsPath, 10);
}

void openJPEG_Image() {
	JPEG_Image img;
	ASSERT_THROWS(img.getBitmap(), runtime_error);
	ASSERT_THROWS(img.getSizeX(), runtime_error);
	ASSERT_THROWS(img.getSizeY(), runtime_error);
	ASSERT_THROWS(img.getNumChannels(), runtime_error);
	ASSERT_THROWS(img.loadImage("inexistentFile"), runtime_error);
	img.loadImage(testJPG);
	cout << "Loaded image. Size="<< img.getSizeX() << "x" << img.getSizeY();
	cout << " numChannels=" << img.getNumChannels() << endl;

	img.addTestRGB_Blocks();
	// Dump pixels (temporary section).
	const int nPixelsToDump=0; // img.getSizeX()*img.getSizeY();
	const int numLine=0;
	unsigned char *bitmap = img.getBitmap();
	if (nPixelsToDump) cout << "First " << nPixelsToDump << " pixels from line " << numLine << " in bitmap:" << endl;
	for (unsigned int i = 0 ; i <nPixelsToDump; i++ ){
		 int R, G, B;

		unsigned char *pix=bitmap+(int) (img.getSizeY()*numLine+ i*img.getNumChannels());
		R= (int) *pix++;
		G= (int) *pix++;
		B= (int) *pix;
		const double theshold=20;
		// cout << fabs(R-G) << ", " <<  fabs(B-G) <<", " << fabs(R-B) << endl;
		if ((fabs(R-G) > theshold) || (fabs(R-B)> theshold) || (fabs(G-B) > theshold)) {
			cout << " " << i << " (at " << (unsigned long) pix << ")  R:" << R << " G:" << G  << "  B:" << B << endl;
		}
	}
}

void rasterDatasetCreation() {
	try {
		string dsPath(tmpDir);
		dsPath+='/';
		dsPath+=rasterDS_Name;
		dsPath+="SAVED";

		// clean up just in case.
		if (FileMgr::fileExists(dsPath) && (FileMgr::isRegularFile(dsPath))) {
			FileMgr::deleteFile(dsPath);
		}
		GIS_ContourDataset::deleteFiles(dsPath, true);
		ASSERT(!FileMgr::directoryExists(dsPath));
		ASSERT(!FileMgr::fileExists(dsPath));

		GIS_RasterDataset* ds;
		cout << "Creating dataset with default format from JPEG file..." << endl;
		cout << "  (" << dsPath << ")" << endl;

		cout << "TEST 1. while inexistent, no replace" << endl;
		//Formats tested: default (GTiff), ENVI, RST
		ds=new GIS_RasterDataset(dsPath, testJPG,false /*replace*/);
		ASSERT(ds != nullptr);
		ds->setCanSatMetaData(12.3456789, -23.45678912, 34.5678912);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
		// This one we do not delete.

		dsPath=tmpDir;
		dsPath+='/';
		dsPath+=rasterDS_Name;
		// clean up just in case.
		if (FileMgr::fileExists(dsPath) && (FileMgr::isRegularFile(dsPath))) {
			FileMgr::deleteFile(dsPath);
		}
		if (FileMgr::directoryExists(dsPath)) {
				GIS_RasterDataset::deleteFiles(dsPath, true);
		}
		cout << "TEST 2. while inexistent, with replace " << endl;
		ds=new GIS_RasterDataset(dsPath, testJPG,true /*replace*/);
		ASSERT(ds != nullptr);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
        // do not remove: used by next test.

		cout << "TEST 3. while existent, replace" << endl;
		ds=new GIS_RasterDataset(dsPath, testJPG,true /*replace*/);
		ASSERT(ds != nullptr);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
		// do not delete file: needed for next test.

		cout << "TEST 4. while existent, do not replace " << endl;
		ds=new GIS_RasterDataset(dsPath, testJPG, false /*replace*/);
		ASSERT(ds != nullptr);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
		GIS_RasterDataset::deleteFiles(dsPath, true);

		cout << "TEST 5. invalid format " << endl;
		ASSERT_THROWS(ds=new GIS_RasterDataset(dsPath, testJPG,false /*replace*/, 0,0,0.01,0.01, "inexistent format"), runtime_error);
		cout << "TEST 6. Format does not support 'create'" << endl;
		ASSERT_THROWS(ds=new GIS_RasterDataset(dsPath, testJPG,false /*replace*/, 0,0,0.01,0.01, "AAIGrid"), runtime_error);

		cout << "TEST 7. Format does not support 'raster'" << endl;
		ASSERT_THROWS(ds=new GIS_RasterDataset(dsPath, testJPG,false /*replace*/, 0,0,0.01,0.01, "ESRI Shapefile"), runtime_error);

		cout << "TEST 8. Create from bitmap" << endl;
		JPEG_Image img(testJPG);
		ds=new GIS_RasterDataset(dsPath, img.getBitmap(), img.getSizeX(), img.getSizeY(), img.getNumChannels(),false);
		ASSERT(ds != nullptr);
		delete ds;
		ds=nullptr;
		ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
		GIS_RasterDataset::deleteFiles(dsPath, true);
	}
	catch(exception &e) {
		cout << "Exception received: " << e.what() << endl;
		ASSERT(false);
	}

}

void createTrajectoryDataset() {
	string dsPath(tmpDir);
	dsPath+='/';
	dsPath+=trajectoryDS_Name;
	dsPath+="SAVED";

	// clean up just in case.
	if (FileMgr::fileExists(dsPath) && (FileMgr::isRegularFile(dsPath))) {
		FileMgr::deleteFile(dsPath);
	}
	GIS_TrajectoryDataset::deleteFiles(dsPath);
	ASSERT(!FileMgr::directoryExists(dsPath));
	ASSERT(!FileMgr::fileExists(dsPath));

	GIS_TrajectoryDataset* ds;
	cout << "Creating dataset with default format..." << endl;
	cout << "  (" << dsPath << ")" << endl;

	cout << "TEST 1. while inexistent, no replace" << endl;
	ds=new GIS_TrajectoryDataset(dsPath,false /*replace*/);
	ASSERT(ds != nullptr);
	ds->addCanPosition(-20,-10);
	ds->addCanPosition(-15,-5);
	ds->addCanPosition(-10,10);
	ds->addCanPosition(-5,12);
	ds->addCanPosition(0,-3);
	ASSERT(ds->checkIntegrity(true));
	delete ds;
	ds=nullptr;
	ASSERT(FileMgr::directoryExists(dsPath) || (FileMgr::fileExists(dsPath)));
	// This one we do not delete.

	// Add other creation modes here.
}

cute::suite make_suite_TestGDAL_Datasets() {

	cute::suite s { };
	s.push_back(CUTE(vectorDatasetCreationTests));
	s.push_back(CUTE(insertContoursInSingleDataset));
	s.push_back(CUTE(insertContoursInMultipleDataset));
	s.push_back(CUTE(openJPEG_Image));
	s.push_back(CUTE(rasterDatasetCreation));
	s.push_back(CUTE(createTrajectoryDataset));


	//s.push_back(CUTE(createRasterDataset));
	return s;
}
