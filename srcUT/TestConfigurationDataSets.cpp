#include <string>
#include <iostream>
#include <iomanip>
#include "TestConfigurationDataSets.h"
#include "cute.h"

#include "IMU_CalibrationDataSet.h"
#include "CameraCalibrationDataSet.h"
#include "RTP_ConfigDataSet.h"
#include "FileMgr.h"

using namespace std;

string imuParsingTestDataFile="Testfiles/configurationFiles/testParsing.imu.csv";
string imuParsingTestDataFile_Erroneous1="Testfiles/configurationFiles/testParsingErroneous1.imu.csv";
string imuParsingTestDataFile_Erroneous2="Testfiles/configurationFiles/testParsingErroneous2.imu.csv";
string imuParsingTestDataFile_Incomplete="Testfiles/configurationFiles/testParsingIncomplete.imu.csv";
string cameraParsingTestDataFile="Testfiles/configurationFiles/testParsing.camera.csv";
string cameraParsingTestDataFile_Erroneous1="Testfiles/configurationFiles/testParsingErroneous1.camera.csv";
string cameraParsingTestDataFile_Erroneous2="Testfiles/configurationFiles/testParsingErroneous2.camera.csv";
string cameraParsingTestDataFile_Incomplete="Testfiles/configurationFiles/testParsingIncomplete.camera.csv";
string configParsingTestDataFile="Testfiles/configurationFiles/testParsing.config.csv";
string configParsingTestDataFile_Erroneous1="Testfiles/configurationFiles/testParsingErroneous1.config.csv";
string configParsingTestDataFile_Erroneous2="Testfiles/configurationFiles/testParsingErroneous2.config.csv";
string configParsingTestDataFile_Incomplete="Testfiles/configurationFiles/testParsingIncomplete.config.csv";

void test_IMU_DataSetParsing() {
	IMU_CalibrationDataSet imuData(imuParsingTestDataFile);

	ASSERT_EQUAL_DELTA(imuData.accelOffset[0], -1.000001, 0.000001);
	ASSERT_EQUAL_DELTA(imuData.accelOffset[1], 2.000002, 0.000001);
	ASSERT_EQUAL_DELTA(imuData.accelOffset[2], -3.000003, 0.000001);
	ASSERT_EQUAL_DELTA(imuData.accelResolution[0], 4.000004, 0.000001);
	ASSERT_EQUAL_DELTA(imuData.accelResolution[1], -5.000005, 0.000001);
	ASSERT_EQUAL_DELTA(imuData.accelResolution[2], 6.000006, 0.000001);

	ASSERT_EQUAL_DELTA(imuData.magOffset[0], -7.000007, 0.000001);
	ASSERT_EQUAL_DELTA(imuData.magOffset[1], 8.000008, 0.000001);
	ASSERT_EQUAL_DELTA(imuData.magOffset[2], -9.000009, 0.000001);
	for (auto i =0UL ; i < 3; i++ ) {
		for (auto j=0UL; j< 3; j++) {
			cout << "transformation matrix [" << i << "," << j << "]=" << setprecision(10)
				 << imuData.magTransformationMatrix[i][j]<< endl;
			double expected=10.000001*(i+1) + 1.0000001*(j+1);
			if (j==1) expected=-expected;
			cout << "   expected: " << setprecision(20) << expected << endl;
			ASSERT_EQUAL_DELTA(imuData.magTransformationMatrix[i][j],expected, 0.0000001);
		}
	}

	ASSERT_EQUAL_DELTA(imuData.gyroOffset[0], 34.0000034, 0.0000001);
	ASSERT_EQUAL_DELTA(imuData.gyroOffset[1], -35.0000035, 0.0000001);
	ASSERT_EQUAL_DELTA(imuData.gyroOffset[2], 36.0000036, 0.0000001);
	ASSERT_EQUAL_DELTA(imuData.gyroResolution[0], -37.0000037, 0.0000001);
	ASSERT_EQUAL_DELTA(imuData.gyroResolution[1], 38.0000038, 0.0000001);
	ASSERT_EQUAL_DELTA(imuData.gyroResolution[2], -39.0000039, 0.0000001);


	cout << "Testing detection of format errors: " << endl;
	ASSERT(FileMgr::isRegularFile(imuParsingTestDataFile_Erroneous1));
	ASSERT_THROWS(IMU_CalibrationDataSet data2(imuParsingTestDataFile_Erroneous1), exception);
	ASSERT(FileMgr::isRegularFile(imuParsingTestDataFile_Erroneous2));
	ASSERT_THROWS(IMU_CalibrationDataSet data3(imuParsingTestDataFile_Erroneous2), exception);
	ASSERT(FileMgr::isRegularFile(imuParsingTestDataFile_Incomplete));
	ASSERT_THROWS(IMU_CalibrationDataSet data4(imuParsingTestDataFile_Incomplete), exception);
}

void test_CameraDataSetParsing() {
	CameraCalibrationDataSet data(cameraParsingTestDataFile);

	ASSERT_EQUAL(data.numPixelsX, 600);
	ASSERT_EQUAL(data.numPixelsY, 300);
	ASSERT_EQUAL_DELTA(data.pixelSizeX, 0.000001, 0.000001);
	ASSERT_EQUAL_DELTA(data.pixelSizeY, 0.000002, 0.000001);
	ASSERT_EQUAL_DELTA(data.angle1TBD, 1.1, 0.000001);
	ASSERT_EQUAL_DELTA(data.angle2TBD, -2.2, 0.000001);
	ASSERT_EQUAL_DELTA(data.angle3TBD, 3.3, 0.000001);
	ASSERT_EQUAL_DELTA(data.viewingAngleX, 41.41 , 0.000001);
	ASSERT_EQUAL_DELTA(data.viewingAngleY, 52.52, 0.000001);
	ASSERT_EQUAL_DELTA(data.f, 0.06, 0.000001);

	cout << "Testing detection of format errors: " << endl;
	ASSERT(FileMgr::isRegularFile(cameraParsingTestDataFile_Erroneous1));
	ASSERT_THROWS(CameraCalibrationDataSet data2(cameraParsingTestDataFile_Erroneous1), exception);
	ASSERT(FileMgr::isRegularFile(cameraParsingTestDataFile_Erroneous2));
	ASSERT_THROWS(CameraCalibrationDataSet data3(cameraParsingTestDataFile_Erroneous2), exception);
	ASSERT(FileMgr::isRegularFile(cameraParsingTestDataFile_Incomplete));
	ASSERT_THROWS(CameraCalibrationDataSet data4(cameraParsingTestDataFile_Incomplete), exception);
}

void test_ConfigurationDataSetParsing() {
	RTP_ConfigDataSet data(configParsingTestDataFile);

	ASSERT_EQUAL_DELTA(data.localReferentialOrigin_Latitude, 12.12, 0.000001);
	ASSERT_EQUAL_DELTA(data.localReferentialOrigin_Longitude, -5.5, 0.000001);
	ASSERT_EQUAL_DELTA(data.localReferentialOrigin_Altitude, 124.83, 0.000001);
	ASSERT_EQUAL_DELTA(data.temperatureOffset, -4.2 , 0.000001);
	ASSERT_EQUAL_DELTA(data.seaLevelPressure, 968.43, 0.000001);
	ASSERT_EQUAL_DELTA(data.PressureOffest, 3.6, 0.000001);
	ASSERT_EQUAL_DELTA(data.IMU_SampleFrequency, 10, 0.000001);
	ASSERT_EQUAL(data.CO2_NumSampleToAverage, 3);
	ASSERT_EQUAL(data.transmissionPeriod, 300);

	cout << "Testing detection of format errors: " << endl;
	ASSERT(FileMgr::isRegularFile(configParsingTestDataFile_Erroneous1));
	ASSERT_THROWS(RTP_ConfigDataSet imuData2(configParsingTestDataFile_Erroneous1), exception);
	ASSERT(FileMgr::isRegularFile(configParsingTestDataFile_Erroneous2));
	ASSERT_THROWS(RTP_ConfigDataSet imuData3(configParsingTestDataFile_Erroneous2), exception);
	ASSERT(FileMgr::isRegularFile(configParsingTestDataFile_Incomplete));
	ASSERT_THROWS(RTP_ConfigDataSet imuData4(configParsingTestDataFile_Incomplete), exception);
}

cute::suite make_suite_TestconfigurationDataSets() {
	cute::suite s { };
	s.push_back(CUTE(test_IMU_DataSetParsing));
	s.push_back(CUTE(test_CameraDataSetParsing));
	s.push_back(CUTE(test_ConfigurationDataSetParsing));
	return s;
}
