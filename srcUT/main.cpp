
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wstring-conversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#  include "cute.h"
#  include "ide_listener.h"
#  include "xml_listener.h"
#  include "cute_runner.h"
#pragma GCC diagnostic pop

#include "DebugUtils.h"

// Add one include here for each test suite.
#include "TestFileMgr.h"
#include "TestRecordProcessor.h"
#include "TestCancellationWatch.h"
#include "TestDataRecord.h"
#include "TestCSV_Row.h"
#include "TestConfigurationDataSets.h"
#include "TestMovingAverage.h"
#include "TestGDAL_Datasets.h"
#include "TestImgRecordMapping.h"

INITIALIZE_EASYLOGGINGPP

bool runSuite(int argc, char const *argv[]) {
	// Set up logging
	START_EASYLOGGINGPP(argc, argv);
	configureLogging();

	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = true;

	// Add 2 lines hereunder for each Test suite.
	cute::suite TestFileMgr = make_suite_TestFileMgr();
	success &= runner(TestFileMgr, "FileMgr_Test");

	cute::suite TestCSV_Row = make_suite_TestCSV_Row();
	success &= runner(TestCSV_Row, "CSV_Row_Test");
	cute::suite TestDataRecord = make_suite_dataRecord_Test();
	success &= runner(TestDataRecord, "IsaTwoRecord_Test");

	cute::suite TestProcessor = make_suite_TestRecordProcessor();
	success &= runner(TestProcessor, "TestProcessor");
	cute::suite TestWatch = make_suite_InteractiveTests();
	success &= runner(TestWatch, "Interactive Tests");

	cute::suite TestIMU = make_suite_TestconfigurationDataSets();
	success &= runner(TestIMU, "Configuration Data Sets");

	cute::suite TestMlAvg = make_suite_TestMovingAverage();
	success &= runner(TestMlAvg, "Test of moving average object");

	cute::suite TestGDAL = make_suite_TestGDAL_Datasets();
	success &= runner(TestGDAL, "GIS Datasets");

	cute::suite TestImgMapping = make_suite_TestImgRecordMapping();
	success &= runner(TestImgMapping, "Img-Record mapping");

	return success;
}

int main(int argc, char const *argv[]) {
    return runSuite(argc, argv) ? EXIT_SUCCESS : EXIT_FAILURE;
}
