#include "IsaTwoRecord.h"
#include "IsaTwoGroundRecord.h"
#include "IsaTwoInterface.h"
#include "cute.h"

#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <sstream>
#include "CSV_Row.h"
#include "FileMgr.h"

#define DEBUG
#include "DebugUtils.h"
constexpr auto DBG=false;

#include "TestDataRecord.h"

using namespace std;

const string tmpFilesDir="TestFiles/tmpFiles";
const string tmpFileName=tmpFilesDir+"/data.csv";

void AssertIsaTwoRecordsEqual(const IsaTwoRecord& rec2,
		const IsaTwoRecord& rec1) {
	DBG_FCT
	ASSERT_EQUAL(rec2.getRecordType(), rec1.getRecordType());

	ASSERT_EQUAL(rec2.timestamp, rec1.timestamp);
	ASSERT_EQUAL(rec2.accelRaw[0], rec1.accelRaw[0]);
	ASSERT_EQUAL(rec2.accelRaw[1], rec1.accelRaw[1]);
	ASSERT_EQUAL(rec2.accelRaw[2], rec1.accelRaw[2]);
	ASSERT_EQUAL(rec2.gyroRaw[0], rec1.gyroRaw[0]);
	ASSERT_EQUAL(rec2.gyroRaw[1], rec1.gyroRaw[1]);
	ASSERT_EQUAL(rec2.gyroRaw[2], rec1.gyroRaw[2]);
	ASSERT_EQUAL(rec2.magRaw[0], rec1.magRaw[0]);
	ASSERT_EQUAL(rec2.magRaw[1], rec1.magRaw[1]);
	ASSERT_EQUAL(rec2.magRaw[2], rec1.magRaw[2]);
	ASSERT_EQUAL(rec2.GPS_Measures, rec1.GPS_Measures);
	ASSERT_EQUAL(rec2.GPS_LatitudeDegrees, rec1.GPS_LatitudeDegrees);
	ASSERT_EQUAL(rec2.GPS_LongitudeDegrees, rec1.GPS_LongitudeDegrees);
	ASSERT_EQUAL(rec2.GPS_Altitude, rec2.GPS_Altitude);
#ifdef INCLUDE_GPS_VELOCITY
	ASSERT_EQUAL(rec2.GPS_VelocityKnots, rec1.GPS_VelocityKnots);
	ASSERT_EQUAL(rec2.GPS_VelocityAngleDegrees, rec1.GPS_VelocityAngleDegrees);
#endif
#ifdef INCLUDE_AHRS_DATA
	ASSERT_EQUAL(rec2.AHRS_Accel[0], rec1.AHRS_Accel[0]);
	ASSERT_EQUAL(rec2.AHRS_Accel[1], rec1.AHRS_Accel[1]);
	ASSERT_EQUAL(rec2.AHRS_Accel[2], rec1.AHRS_Accel[2]);
	ASSERT_EQUAL(rec2.roll, rec1.roll);
	ASSERT_EQUAL(rec2.yaw, rec1.yaw);
	ASSERT_EQUAL(rec2.pitch, rec1.pitch);
#endif
	// D. Primary
	ASSERT_EQUAL(rec2.temperatureBMP, rec2.temperatureBMP);
	ASSERT_EQUAL(rec2.pressure, rec1.pressure);
	ASSERT_EQUAL(rec2.altitude, rec1.altitude);
	ASSERT_EQUAL(rec2.temperatureThermistor, rec1.temperatureThermistor);
	//E. Secundary
	ASSERT_EQUAL(rec2.CO2_Voltage, rec1.CO2_Voltage);
}

void AssertIsaTwoGroundRecordsEqual(const IsaTwoGroundRecord& rec1,
		const IsaTwoGroundRecord& rec2) {
	DBG_FCT
	AssertIsaTwoRecordsEqual(rec2, rec1);

	ASSERT_EQUAL(rec2.calibratedAccel[0], rec1.calibratedAccel[0]);
	ASSERT_EQUAL(rec2.calibratedAccel[1], rec1.calibratedAccel[1]);
	ASSERT_EQUAL(rec2.calibratedAccel[2], rec1.calibratedAccel[2]);
	ASSERT_EQUAL(rec2.calibratedGyro[0], rec1.calibratedGyro[0]);
	ASSERT_EQUAL(rec2.calibratedGyro[1], rec1.calibratedGyro[1]);
	ASSERT_EQUAL(rec2.calibratedGyro[2], rec1.calibratedGyro[2]);
	ASSERT_EQUAL(rec2.calibratedMag[0], rec1.calibratedMag[0]);
	ASSERT_EQUAL(rec2.calibratedMag[1], rec1.calibratedMag[1]);
	ASSERT_EQUAL(rec2.calibratedMag[2], rec1.calibratedMag[2]);

	ASSERT_EQUAL(rec2.groundAHRS_Roll, rec1.groundAHRS_Roll);
	ASSERT_EQUAL(rec2.groundAHRS_Yaw, rec1.groundAHRS_Yaw);
	ASSERT_EQUAL(rec2.groundAHRS_Pitch, rec1.groundAHRS_Pitch);

	ASSERT_EQUAL(rec2.positionEarthCentric[0], rec1.positionEarthCentric[0]);
	ASSERT_EQUAL(rec2.positionEarthCentric[1], rec1.positionEarthCentric[1]);
	ASSERT_EQUAL(rec2.positionEarthCentric[2], rec1.positionEarthCentric[2]);
	ASSERT_EQUAL(rec2.positionLocal[0], rec1.positionLocal[0]);
	ASSERT_EQUAL(rec2.positionLocal[1], rec1.positionLocal[1]);
	ASSERT_EQUAL(rec2.positionLocal[2], rec1.positionLocal[2]);
	ASSERT_EQUAL(rec2.KF_Position[0], rec1.KF_Position[0]);
	ASSERT_EQUAL(rec2.KF_Position[1], rec1.KF_Position[1]);
	ASSERT_EQUAL(rec2.KF_Position[2], rec1.KF_Position[2]);
	ASSERT_EQUAL(rec2.KF_Velocity[0], rec1.KF_Velocity[0]);
	ASSERT_EQUAL(rec2.KF_Velocity[1], rec1.KF_Velocity[1]);
	ASSERT_EQUAL(rec2.KF_Velocity[2], rec1.KF_Velocity[2]);

	ASSERT_EQUAL(rec2.temperatureCorrected, rec1.temperatureCorrected);
	ASSERT_EQUAL(rec2.altitude_Corrected, rec1.altitude_Corrected);
	ASSERT_EQUAL(rec2.CO2_VoltageCleaned, rec1.CO2_VoltageCleaned);
	ASSERT_EQUAL(rec2.CO2_Concentration, rec1.CO2_Concentration);

	for (size_t i =0; i<4; i++) {
		ASSERT_EQUAL(rec2.imgCornerLat[i], rec1.imgCornerLat[i]);
		ASSERT_EQUAL(rec2.imgCornerLong[i], rec1.imgCornerLong[i]);
	}
}

/* Init array of doubles with test number (startDouble is incremented by 1.1 after each field */
void initArray(std::array<double, 3> &arr, double &startDouble) {
	for (unsigned long i = 0; i<3; i++) {
		arr[i]=startDouble;
		startDouble+=1.01;
	}
}

/* Init array of doubles with test number (startDouble is incremented by 1.1 after each field */
void initArray(std::array<double, 4> &arr, double &startDouble) {
	for (unsigned long i = 0; i<4; i++) {
		arr[i]=startDouble;
		startDouble+=1.01;
	}
}

/* Initialize integer fields with successive integers from startInt, and double fields from
 * startDouble, with increment 0.1
 */
void initIsaTwoRecordExceptTimestamp(IsaTwoRecord &rec, uint16_t &startInt, double &startDouble) {
	// rec.timestamp not set.
	rec.accelRaw[0] = (int16_t) startInt++;
	rec.accelRaw[1] = (int16_t) (-startInt++);
	rec.accelRaw[2] = (int16_t) startInt++;
	rec.gyroRaw[0] = (int16_t) startInt++;
	rec.gyroRaw[1] = (int16_t) (-startInt++);
	rec.gyroRaw[2] = (int16_t) startInt++;
	initArray(rec.magRaw,startDouble);
	rec.GPS_Measures = false;
	rec.GPS_LatitudeDegrees = startDouble;	startDouble += 1.01;
	rec.GPS_LongitudeDegrees = startDouble;	startDouble += 1.01;
	rec.GPS_Altitude = startDouble;			startDouble += 1.01;
#ifdef INCLUDE_GPS_VELOCITY
	rec.GPS_VelocityKnots = startDouble;		startDouble += 1.01;
	rec.GPS_VelocityAngleDegrees = startDouble;	startDouble += 1.01;
#endif
#ifdef INCLUDE_AHRS_DATA
	initArray(rec.AHRS_Accel,startDouble);
	rec.roll = startDouble;		startDouble += 1.01;
	rec.yaw = startDouble;		startDouble += 1.01;
	rec.pitch = startDouble;		startDouble += 1.01;
#endif
	rec.temperatureBMP = startInt++;
	rec.pressure = startDouble;	startDouble += 1.01;
	rec.altitude = startDouble;	startDouble += 1.01;
	rec.temperatureThermistor=startDouble;	startDouble += 1.01;
	rec.CO2_Voltage = startDouble;		startDouble += 1.01;
}

void testCanRecords() {
    FileMgr::createDirectory(tmpFilesDir, true);
	cout << "Testing IsaTwoGroundRecord output and assignment operator..." << endl;
	IsaTwoRecord rec, rec2;
	// Check record type:
	ASSERT_EQUAL(rec.getRecordType(), (uint16_t) IsaTwoRecordType::DataRecord);

	uint16_t i=3;
	double val=3.5;
	initIsaTwoRecordExceptTimestamp(rec, i, val);
	ASSERT_EQUAL(0, rec.timestamp); // timestamp should be 0 by default.
	ASSERT_EQUAL(rec.getRecordType(),(uint16_t) IsaTwoRecordType::DataRecord); // record type unchanged.

    // Write to file
    FileMgr::deleteFile(tmpFileName, true); // silent if does not exist.
    ofstream fout(tmpFileName);
    ASSERT(fout.good());

    rec.printCSV_Header(fout);
    fout << endl << rec ;
    cout << "Wrote: " << rec << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    rec.updateTimeStamp();
    ASSERT_GREATER_EQUAL(rec.timestamp, (unsigned int) 100);
    fout << endl<< rec;
    cout << "Wrote: " << rec << endl;
    rec.GPS_Measures=true;
    fout << endl << rec ;
    cout << "Wrote: " << rec << endl ;
    fout.close();

    cout << endl << "Reading from file again..." << endl;
    ifstream fin(tmpFileName);
    ASSERT(fin.good());
    CSV_Row::skipOneRow(fin);
    fin >> rec2;
    cout << "read 1: " << rec2;
    // Reset flag & timestamp because they were changed in the previous test:
    rec.GPS_Measures=false;
    rec.timestamp=0L;
	AssertIsaTwoRecordsEqual(rec2, rec);
    fin >> rec2;
    cout << "read 2: " << rec2 << endl << flush;
    ASSERT_EQUAL(false, rec2.GPS_Measures);
    ASSERT_GREATER_EQUAL(rec2.timestamp, rec.timestamp);
    fin >> rec2;
    cout << "read 3: " << rec2 << endl << flush;
    ASSERT_EQUAL(true, rec2.GPS_Measures);
    fin.close();

    cout << "Testing operator=..." << endl << flush;
    rec2.clear();
    rec2=rec;
    AssertIsaTwoRecordsEqual(rec2, rec);
    // Modify some values in non atomic types to check they are not shared
    int16_t save= rec.accelRaw[0];
    rec.accelRaw[0]=3;
    rec.accelRaw[1]=10;
    rec.accelRaw[2]=50;
    ASSERT_EQUAL(rec2.accelRaw[0],save);
    ASSERT_NOT_EQUAL_TO(rec2.accelRaw[1],  (int16_t) 10);
    ASSERT_NOT_EQUAL_TO(rec2.accelRaw[2],  (int16_t) 50);

    cout << "Testing clear method..." << endl;
    // rec has no null values left.
    rec.clear();
    IsaTwoRecord cleanRecord;
    AssertIsaTwoRecordsEqual(rec, cleanRecord);
}

void initIsaTwoGroundRecordExceptTimestamp(IsaTwoGroundRecord& rec, uint16_t &startInt, double &startDouble) {
	initIsaTwoRecordExceptTimestamp(rec, startInt, startDouble);
	initArray(rec.calibratedAccel,startDouble);
	initArray(rec.calibratedGyro ,startDouble);
	initArray(rec.calibratedMag  ,startDouble);
	rec.groundAHRS_Roll = startDouble;	startDouble += 1.01;
	rec.groundAHRS_Yaw = startDouble;	startDouble += 1.01;
	rec.groundAHRS_Pitch = startDouble;	startDouble += 1.01;
	initArray(rec.positionEarthCentric,startDouble);
	initArray(rec.positionLocal,startDouble);
	initArray(rec.KF_Position,startDouble);
	initArray(rec.KF_Velocity,startDouble);
	rec.temperatureCorrected = startDouble;	startDouble += 1.01;
	rec.altitude_Corrected = startDouble;	startDouble += 1.01;
	rec.CO2_VoltageCleaned = startDouble;	startDouble += 1.01;
	rec.CO2_Concentration = startDouble;	startDouble += 1.01;
	initArray(rec.imgCornerLat, startDouble);
	initArray(rec.imgCornerLong, startDouble);
}

void testGroundRecords() {
    FileMgr::createDirectory(tmpFilesDir, true);
	cout << "Testing IsaTwoGroundRecord output and assignment operator..." << endl;
	IsaTwoGroundRecord rec, rec2;
	// Check record type:
	ASSERT_EQUAL(rec.getRecordType(), (uint16_t) IsaTwoRecordType::GroundDataRecord);

	uint16_t i=3;
	double val=3.5;
	initIsaTwoGroundRecordExceptTimestamp(rec, i, val);
	cout << "Roll set to " << rec.groundAHRS_Roll << endl;
	ASSERT_EQUAL(0, rec.timestamp); // timestamp should be 0 by default.
	ASSERT_EQUAL(rec.getRecordType(),(uint16_t) IsaTwoRecordType::GroundDataRecord); // record type unchanged.

    // Write to file
    FileMgr::deleteFile(tmpFileName, true); // silent if does not exist.
    ofstream fout(tmpFileName);
    ASSERT(fout.good());

    rec.printCSV_Header(fout);
    fout << endl << rec ;
    cout << "Wrote: " << rec << endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    rec.updateTimeStamp();
    ASSERT_GREATER_EQUAL(rec.timestamp, (unsigned int) 100);
    fout << endl<< rec;
    cout << "Wrote: " << rec << endl;
    rec.GPS_Measures=true;
    fout << endl << rec ;
    cout << "Wrote: " << rec << endl ;
    fout.close();

    cout << endl << "Reading from file again..." << endl;
    ifstream fin(tmpFileName);
    ASSERT(fin.good());
    CSV_Row::skipOneRow(fin);
    fin >> rec2;
    cout << "read 1: " << rec2;
    // Reset flag & timestamp because they were changed in the previous test:
	cout << endl << "Roll retrieved: " << rec.groundAHRS_Roll << endl;
    rec.GPS_Measures=false;
    rec.timestamp=0L;
	AssertIsaTwoGroundRecordsEqual(rec2, rec);
    fin >> rec2;
    cout << "read 2: " << rec2 << endl << flush;
    ASSERT_EQUAL(false, rec2.GPS_Measures);
    ASSERT_GREATER_EQUAL(rec2.timestamp, rec.timestamp);
    fin >> rec2;
    cout << "read 3: " << rec2 << endl << flush;
    ASSERT_EQUAL(true, rec2.GPS_Measures);
    fin.close();

    cout << "Testing operator=..." << endl << flush;
    rec2.clear();
    rec2=rec;
    AssertIsaTwoGroundRecordsEqual(rec2, rec);
    // Modify some values in non atomic types to check they are not shared
    int16_t save= rec.accelRaw[0];
    rec.accelRaw[0]=3;
    rec.accelRaw[1]=10;
    rec.accelRaw[2]=50;
    ASSERT_EQUAL(rec2.accelRaw[0],save);
    ASSERT_NOT_EQUAL_TO(rec2.accelRaw[1],  (int16_t) 10);
    ASSERT_NOT_EQUAL_TO(rec2.accelRaw[2],  (int16_t) 50);
}

void testGroundRecord_File() {
	const int numRecords=50;
	cout << "Testing complete Ground Record file..." << endl;

	IsaTwoGroundRecord writtenRec, readRec;
	// Initialise rec;
	uint16_t startInt=1;
	double startDouble=1.01;
	initIsaTwoGroundRecordExceptTimestamp(writtenRec, startInt, startDouble);
	ASSERT(writtenRec.timestamp == 0); // timestamp should be 0 by default.
	writtenRec.updateTimeStamp();

    // Write to file
    ofstream fout(tmpFileName);
    writtenRec.printCSV_Header(fout);
    for (int i = 0 ; i < numRecords ; i++) fout << endl << writtenRec;
    cout << "Wrote " <<numRecords <<" records." << writtenRec ;
    fout.close();

    cout << endl << "Reading from file again..." << endl;
    ifstream fin(tmpFileName);
    ASSERT(fin.good());
    CSV_Row::skipOneRow(fin); // skip first line (header)
    int numReadRecords = 0;
    while (!fin.eof()) {
    	 fin >> readRec;
    	 numReadRecords++;
		 cout << "read record ";
    }
    ASSERT_EQUAL(numReadRecords, numRecords);
    fin.close();
    AssertIsaTwoGroundRecordsEqual(readRec, writtenRec);
}

void TestAssignmentOperators(){
	cout << "Testing assignment operator overloading" << endl;
	IsaTwoRecord rec1, rec2;
	uint16_t i=3;
	double val=3.5;
	cout << "1. Plain assignment" << endl;
	initIsaTwoRecordExceptTimestamp(rec1, i, val);
	initIsaTwoRecordExceptTimestamp(rec2, i, val);
	rec1.timestamp= 100;
	rec2.timestamp= 200;
	rec1=rec2;
	AssertIsaTwoRecordsEqual(rec1, rec2);
	cout << " 2.  Ditto through references" << endl;
	initIsaTwoRecordExceptTimestamp(rec1, i, val);
	IsaTwoRecord& refRec1=rec1;
	IsaTwoRecord& refRec2=rec2;
	refRec1=refRec2;
	AssertIsaTwoRecordsEqual(rec1, rec2);
	cout << "3. Ditto through references of base type" << endl;
	initIsaTwoRecordExceptTimestamp(rec1, i, val);
	CSV_Record& refCSV_Rec1=rec1;
	CSV_Record& refCSV_Rec2=rec2;
	/* Assignment with references does not work,
	 * see https://stackoverflow.com/questions/669818/virtual-assignment-operator-c#669894
	 * for explanation. */
	// refCSV_Rec1=refCSV_Rec2; Just calls CSV_Record::operator=()
    CSV_Record * prec1=&refCSV_Rec1;
    CSV_Record * prec2=&refCSV_Rec2;
    cout << "type of prec1: " << typeid(prec1).name() << endl;
    cout << "type of prec2: " << typeid(prec2).name() << endl;
    cout << "type of *prec1: " << typeid(*prec1).name() << endl;
    cout << "type of *prec2: " << typeid(*prec2).name() << endl;

    //*prec1 = *prec2;  // Does not work, call CSV_Record::operator=
    			      // This is because signature of operators differ.
    // This works, but is ugly
    *(dynamic_cast<IsaTwoRecord*>(prec1)) = *(dynamic_cast<IsaTwoRecord*>(prec2));
	AssertIsaTwoRecordsEqual(rec1, rec2);

	cout << "4. Ditto recasting base class pointers" << endl;
	initIsaTwoRecordExceptTimestamp(rec1, i, val);
	IsaTwoRecord& refFromCSV_1=dynamic_cast<IsaTwoRecord&>(refCSV_Rec1);
	IsaTwoRecord& refFromCSV_2=dynamic_cast<IsaTwoRecord&>(refCSV_Rec2);
	/* For the same reason as above, this does not work:
	 * refFromCSV_1=refFromCSV_2;
	 * It just calls CSV_Record::operator=
	 */
	// Next line does, but is still ugly.
    //*(dynamic_cast<IsaTwoRecord*>(&refFromCSV_1)) = *(dynamic_cast<IsaTwoRecord*>(&refFromCSV_2));
    dynamic_cast<IsaTwoRecord&>(refFromCSV_1) = dynamic_cast<IsaTwoRecord&>(refFromCSV_2);
	AssertIsaTwoRecordsEqual(rec1, rec2);
}

void testRecordTypeValidation()
{
	string fileName=tmpFilesDir+"/wrongRecords.can.csv";

	IsaTwoRecord i2rec;
	ASSERT_EQUAL(i2rec.getRecordType(), (unsigned int) IsaTwoRecordType::DataRecord);

	// Create a file with empty IsaTwoRecords with wrong record type
	// Manually tamper with record type in CSV string.
	FileMgr::deleteFile(fileName, true);
	stringstream s;
	i2rec.printCSV(s);
	string csv=s.str();
	csv[0]++;   // increase first character.
	// Write to file
	ofstream fout(fileName);
	i2rec.printCSV_Header(fout);

	for (int i = 0 ; i < 5 ; i++) fout << endl << csv;
	cout << "Wrote 5 records with faulty record type." << endl ;
	fout.close();

	cout << endl << "Reading from file again..." << endl;
	ifstream fin(fileName);
	ASSERT(fin.good());
	CSV_Row::skipOneRow(fin); // skip first line (header)
	IsaTwoRecord readRec;
	ASSERT_THROWS(fin >> readRec, exception);
	cout << "Wrong record type properly detected. " << endl;
	fin.close();
	FileMgr::deleteFile(fileName, true);

	IsaTwoGroundRecord i2groundRec;
	ASSERT_EQUAL(i2groundRec.getRecordType(), (unsigned int) IsaTwoRecordType::GroundDataRecord);

	// Create a file with empty IsaTwoGroundRecords with wrong record type
	// Manually tamper with record type in CSV string.
	fileName=tmpFilesDir+"/wrongGroundRecords.gnd.csv";
	FileMgr::deleteFile(fileName, true);
	s.clear();
	i2groundRec.printCSV(s);
	csv=s.str();
	csv[0]++;   // increase first character.
	// Write to file
	fout.open(fileName);
	i2groundRec.printCSV_Header(fout);

	for (int i = 0 ; i < 5 ; i++) fout << endl << csv;
	cout << "Wrote 5 records with faulty record type." << endl ;
	fout.close();

	cout << endl << "Reading from file again..." << endl;
	fin.open(fileName);
	ASSERT(fin.good());
	CSV_Row::skipOneRow(fin); // skip first line (header)
	IsaTwoGroundRecord readGroundRec;
	ASSERT_THROWS(fin >> readGroundRec, exception);
	cout << "Wrong record type properly detected. " << endl;
	fin.close();
	FileMgr::deleteFile(fileName, true);
}

void TestAssignment_IsaTwo_to_IsaTwoGround() {
	cout << "Testing assignment of IsaTwoRecord to IsaTwoGroundRecord..." << endl;
	IsaTwoGroundRecord gndRecord, gndRecordCopy;
	IsaTwoRecord canRecord;
	gndRecord.clear();
	uint16_t startInt=10;
	double startDouble=100.0;
	initIsaTwoRecordExceptTimestamp(canRecord, startInt, startDouble);
	canRecord.updateTimeStamp();
	initIsaTwoGroundRecordExceptTimestamp(gndRecord, startInt, startDouble);
	gndRecord.timestamp=123456ul;
	gndRecordCopy=gndRecord;

	cout << "Check assignement of GroundRecord to GroundRecord..." << endl;
		AssertIsaTwoGroundRecordsEqual(gndRecord, gndRecordCopy);
	#ifdef USELESS_TO_REMOVE
		ASSERT_EQUAL(gndRecord.calibratedAccel[0], gndRecordCopy.calibratedAccel[0]);
		ASSERT_EQUAL(gndRecord.calibratedAccel[1], gndRecordCopy.calibratedAccel[1]);
		ASSERT_EQUAL(gndRecord.calibratedAccel[2], gndRecordCopy.calibratedAccel[2]);
		ASSERT_EQUAL(gndRecord.calibratedGyro[0], gndRecordCopy.calibratedGyro[0]);
		ASSERT_EQUAL(gndRecord.calibratedGyro[1], gndRecordCopy.calibratedGyro[1]);
		ASSERT_EQUAL(gndRecord.calibratedGyro[2], gndRecordCopy.calibratedGyro[2]);
		ASSERT_EQUAL(gndRecord.calibratedMag[0], gndRecordCopy.calibratedMag[0]);
		ASSERT_EQUAL(gndRecord.calibratedMag[1], gndRecordCopy.calibratedMag[1]);
		ASSERT_EQUAL(gndRecord.calibratedMag[2], gndRecordCopy.calibratedMag[2]);

		ASSERT_EQUAL(gndRecord.groundAHRS_Roll, gndRecordCopy.groundAHRS_Roll);
		ASSERT_EQUAL(gndRecord.groundAHRS_Yaw, gndRecordCopy.groundAHRS_Yaw);
		ASSERT_EQUAL(gndRecord.groundAHRS_Pitch, gndRecordCopy.groundAHRS_Pitch);

		ASSERT_EQUAL(gndRecord.positionEarthCentric[0], gndRecordCopy.positionEarthCentric[0]);
		ASSERT_EQUAL(gndRecord.positionEarthCentric[1], gndRecordCopy.positionEarthCentric[1]);
		ASSERT_EQUAL(gndRecord.positionEarthCentric[2], gndRecordCopy.positionEarthCentric[2]);
		ASSERT_EQUAL(gndRecord.positionLocal[0], gndRecordCopy.positionLocal[0]);
		ASSERT_EQUAL(gndRecord.positionLocal[1], gndRecordCopy.positionLocal[1]);
		ASSERT_EQUAL(gndRecord.positionLocal[2], gndRecordCopy.positionLocal[2]);
		ASSERT_EQUAL(gndRecord.KF_Position[0], gndRecordCopy.KF_Position[0]);
		ASSERT_EQUAL(gndRecord.KF_Position[1], gndRecordCopy.KF_Position[1]);
		ASSERT_EQUAL(gndRecord.KF_Position[2], gndRecordCopy.KF_Position[2]);
		ASSERT_EQUAL(gndRecord.KF_Velocity[0], gndRecordCopy.KF_Velocity[0]);
		ASSERT_EQUAL(gndRecord.KF_Velocity[1], gndRecordCopy.KF_Velocity[1]);
		ASSERT_EQUAL(gndRecord.KF_Velocity[2], gndRecordCopy.KF_Velocity[2]);
	#endif

	cout << "Checking canRecord to GroundRecord assignment" << endl;
	gndRecord=canRecord;
	ASSERT_EQUAL(gndRecord.getRecordType(), (uint16_t) IsaTwoRecordType::GroundDataRecord);
	ASSERT_EQUAL(gndRecord.timestamp, canRecord.timestamp);
	ASSERT_EQUAL(gndRecord.accelRaw[0], canRecord.accelRaw[0]);
	ASSERT_EQUAL(gndRecord.accelRaw[1], canRecord.accelRaw[1]);
	ASSERT_EQUAL(gndRecord.accelRaw[2], canRecord.accelRaw[2]);
	ASSERT_EQUAL(gndRecord.gyroRaw[0], canRecord.gyroRaw[0]);
	ASSERT_EQUAL(gndRecord.gyroRaw[1], canRecord.gyroRaw[1]);
	ASSERT_EQUAL(gndRecord.gyroRaw[2], canRecord.gyroRaw[2]);
	ASSERT_EQUAL(gndRecord.magRaw[0], canRecord.magRaw[0]);
	ASSERT_EQUAL(gndRecord.magRaw[1], canRecord.magRaw[1]);
	ASSERT_EQUAL(gndRecord.magRaw[2], canRecord.magRaw[2]);
	ASSERT_EQUAL(gndRecord.GPS_Measures, canRecord.GPS_Measures);
	ASSERT_EQUAL(gndRecord.GPS_LatitudeDegrees, canRecord.GPS_LatitudeDegrees);
	ASSERT_EQUAL(gndRecord.GPS_LongitudeDegrees, canRecord.GPS_LongitudeDegrees);
	ASSERT_EQUAL(gndRecord.GPS_Altitude, canRecord.GPS_Altitude);
#ifdef INCLUDE_GPS_VELOCITY
	ASSERT_EQUAL(gndRecord.GPS_VelocityKnots, canRecord.GPS_VelocityKnots);
	ASSERT_EQUAL(gndRecord.GPS_VelocityAngleDegrees, canRecord.GPS_VelocityAngleDegrees);
#endif
#ifdef INCLUDE_AHRS_DATA
	ASSERT_EQUAL(gndRecord.AHRS_Accel[0], canRecord.AHRS_Accel[0]);
	ASSERT_EQUAL(gndRecord.AHRS_Accel[1], canRecord.AHRS_Accel[1]);
	ASSERT_EQUAL(gndRecord.AHRS_Accel[2], canRecord.AHRS_Accel[2]);
	ASSERT_EQUAL(gndRecord.roll, canRecord.roll);
	ASSERT_EQUAL(gndRecord.yaw, canRecord.yaw);
	ASSERT_EQUAL(gndRecord.pitch, canRecord.pitch);
#endif
	// D. Primary
	ASSERT_EQUAL(gndRecord.temperatureBMP, canRecord.temperatureBMP);
	ASSERT_EQUAL(gndRecord.pressure, canRecord.pressure);
	ASSERT_EQUAL(gndRecord.altitude, canRecord.altitude);
	ASSERT_EQUAL(gndRecord.temperatureThermistor, canRecord.temperatureThermistor);
	//E. Secundary
	ASSERT_EQUAL(gndRecord.CO2_Voltage, canRecord.CO2_Voltage);
	// Ground Record fields are not affected by the assignment.


}

cute::suite make_suite_dataRecord_Test() {
	cute::suite s { };
	s.push_back(CUTE(testCanRecords));
	s.push_back(CUTE(testGroundRecords));
	s.push_back(CUTE(testGroundRecord_File));
	s.push_back(CUTE(TestAssignmentOperators));
	s.push_back(CUTE(testRecordTypeValidation));
	s.push_back(CUTE(TestAssignment_IsaTwo_to_IsaTwoGround));
	return s;
}
