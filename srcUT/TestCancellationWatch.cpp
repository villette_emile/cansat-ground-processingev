#include "TestCancellationWatch.h"

#include "CancellationWatch.h"
#include "CinManip.h"
#include "cute.h"
#include "CommonTestSettings.h"
#include <thread>
#include <iostream>

using namespace std;

void testCinManip() {
#ifdef INCLUDE_INTERACTIVE_TESTS
	std::cout<<"Enter a string+return: ";
		std::cin.get();
		std::cout<<"Clearing cin with manipulator.\n";
		std::cin>> ignoreline();
		std::cout<<"All done.\n";
		std::cin>> pause();

		std::cout<<"Enter a string+return: ";
		std::cin.get();
		std::cout<<"Clearing cin.\n";
		auto count=ignore_line(std::cin);
		std::cout<<"All done, ignored " << count << " characters. \n";
		std::cin>> pause();

		std::cout<<"Enter just a return: ";
		std::cin.get();
		std::cout<<"Clearing cin.\n";
		std::cin>> ignoreline();
		std::cout<<"All done \n";
		std::cin>> pause();
#else
	cout << "Skipped." << endl;
#endif
}

void testCancellationDuringProcessing() {
#ifdef INCLUDE_INTERACTIVE_TESTS
	cout << "Starting watch now and wait until cancelled: " << endl;
	cout << "  1. press just return, and do NOT cancel, ... " << endl;
	cout << "  2. enter characters & press just return, and do NOT cancel, ... " << endl;
	cout << "  3. press just return, and do cancel, ... " << endl << flush;
	{
	CancellationWatch cw;
	while (!cw.cancelled()) {
		this_thread::sleep_for(std::chrono::milliseconds(500));
	}
	cout << "Cancellation received" << endl;
	}

	// waiting for
	this_thread::sleep_for(std::chrono::milliseconds(500));

	cout << "Let's restart...";
	cout << "  Press just return, and  cancel, ... " << endl;
	{
		CancellationWatch cw;
		while (!cw.cancelled()) {
			this_thread::sleep_for(std::chrono::milliseconds(500));
		}
		cout << "Cancellation received" << endl;
	}

	// waiting for
	this_thread::sleep_for(std::chrono::milliseconds(500));
#else
	cout << "Skipped." << endl;
#endif
}

void testCleanupIfNoCancellation() {
	cout << "Starting watch now, terminating processing without a cancellation..." << endl;
	{
		CancellationWatch cw;
	this_thread::sleep_for(std::chrono::milliseconds(1000));
	cout << "Processing over: check the watch thread terminates... " << endl;
	}

	ASSERT(CancellationWatch::cinDisturbed());
#ifdef INCLUDE_INTERACTIVE_TESTS
	cout << "Let's see whether we can use cin." << endl;
	cout << "The first read could be disturbed by the CancellationWatch... "<< endl;
	cout <<" Enter a first string: " << endl;
	string str;
	cin >> str;
	cout << "Got string : '" << str << "'" << endl;
	cout << "The second read should be ok...." << endl;
	cout <<" Enter a second string: " << endl;
	cin >> str;
	cout << "Got string : '" << str << "'" << endl;
#else
	cout << "Partly skipped." << endl;
#endif
}

void testNoCancellationWithRestart() {
	cout << "Starting watch now, terminating processing without a cancellation twice..." << endl;
	{
		CancellationWatch cw;
		this_thread::sleep_for(std::chrono::milliseconds(1000));
		cout << "Processing over: waiting and starting again " << endl;

		this_thread::sleep_for(std::chrono::milliseconds(1000));

		this_thread::sleep_for(std::chrono::milliseconds(1000));
		cout << "Processing over: waiting and starting again " << endl;
	}
#ifdef INCLUDE_INTERACTIVE_TESTS
		cout << "Let's see whether we can use cin." << endl;
		cout << "The first read could be disturbed by the CancellationWatch... "<< endl;
		cout <<" Enter a first string: " << endl;
		string str;
		cin >> str;
		cout << "Got string : '" << str << "'" << endl;
		cout << "The second read should be ok...." << endl;
		cout <<" Enter a second string: " << endl;
		cin >> str;
		cout << "Got string : '" << str << "'" << endl;
#else
	cout << "Partly skipped." << endl;
#endif
}

void testEmbeddedCancellation_UserCancelledInside() {
#ifdef INCLUDE_INTERACTIVE_TESTS
	CancellationWatch cw1;
	cout << "Created first cancellation watch (should be a message)" << endl;
	{
		CancellationWatch cw2;
		cout << "Created second CancellationWatch (should not be a message!)" << endl;
		{
			CancellationWatch cw3;
			cout << "Created third CancellationWatch (should not be a message!)" << endl;
			cout << "  Press return, and cancel, ... " << endl;
			while (!cw3.cancelled()) {
				this_thread::sleep_for(std::chrono::milliseconds(500));
			}
			cout << "Cancellation received" << endl;
		}
		cout << "back to second watch, checking cancellation is received too... " << flush;
		ASSERT(cw2.cancelled());
		ASSERT(!cw2.cinDisturbed());
		cout << "OK" << endl << flush;
	}
	cout << "back to first watch, checking cancellation is received too... " << flush;
	ASSERT(cw1.cancelled());
	ASSERT(!cw1.cinDisturbed());
	cout << "OK!" << endl;
#else
	cout << "Skipped." << endl;
#endif
}

void testEmbeddedCancellation_NoCancellation(){
	CancellationWatch cw1;
	cout << "Created first cancellation watch (should be a message)" << endl;
	{
		CancellationWatch cw2;
		cout << "Created second CancellationWatch (should not be a message!)" << endl;
		{
			CancellationWatch cw3;
			cout << "Created third CancellationWatch (should not be a message!)" << endl;
			this_thread::sleep_for(std::chrono::milliseconds(1000));
			cout << "3 Processing over" << endl;
		}
		cout << "back to second watch... " << endl;
		this_thread::sleep_for(std::chrono::milliseconds(1000));
		cout << "2 Processing over" << endl;

		ASSERT(!cw2.cancelled());
		ASSERT(cw2.cinDisturbed());
		cout << "OK" << endl;
	}
	cout << "back to first watch... " << endl;
	this_thread::sleep_for(std::chrono::milliseconds(1000));
	cout << "1 Processing over" << endl;
	ASSERT(!cw1.cancelled());
	ASSERT(cw1.cinDisturbed());
	cout << "OK!" << endl;
}



cute::suite make_suite_InteractiveTests() {
	cute::suite s { };
	s.push_back(CUTE(testCinManip));
	s.push_back(CUTE(testCancellationDuringProcessing));
	s.push_back(CUTE(testCleanupIfNoCancellation));
	s.push_back(CUTE(testNoCancellationWithRestart));
	s.push_back(CUTE(testEmbeddedCancellation_UserCancelledInside));
	s.push_back(CUTE(testEmbeddedCancellation_NoCancellation));
	return s;
}
